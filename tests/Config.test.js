/*
 * @flow
 */
/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import { TurnusLintConfig } from "../src/Config";
import { clearIdbMockData } from "idb-keyval";

beforeEach(() => clearIdbMockData());

test("defaults", () => {
    const config = new TurnusLintConfig();
    expect(config.getNumericOption("minDaysBetweenAD")).toBe(3);
    expect(config.getNumericOption("maxConsecutiveNights")).toBe(3);
    expect(config.getNumericOption("maxConsecutiveNightsNightWorker")).toBe(5);
    expect(config.getNumericOption("maxConsecutiveAShifts")).toBe(3);
    expect(config.getStringOption("displayMode")).toBe("report");
    expect(config.getBooleanOption("enableMaxConsecutiveASHifts")).toBe(false);
    expect(
        config.getBooleanOption("enableInferredYearlyWorkWeekendcount"),
    ).toBe(true);
});
describe("*NumericOption", () => {
    test("minDaysBetweenAD", () => {
        const config = new TurnusLintConfig();
        expect(config.getNumericOption("minDaysBetweenAD")).toBe(3);

        config.setNumericOption("minDaysBetweenAD", 5);
        expect(config.getNumericOption("minDaysBetweenAD")).toBe(5);
        config.setNumericOption("minDaysBetweenAD", 7);
        expect(config.getNumericOption("minDaysBetweenAD")).toBe(7);
        config.setNumericOption("minDaysBetweenAD", 10);
        expect(config.getNumericOption("minDaysBetweenAD")).toBe(7);
        config.setNumericOption("minDaysBetweenAD", 1);
        expect(config.getNumericOption("minDaysBetweenAD")).toBe(1);
        config.setNumericOption("minDaysBetweenAD", 0);
        expect(config.getNumericOption("minDaysBetweenAD")).toBe(0);
        config.setNumericOption("minDaysBetweenAD", -1);
        expect(config.getNumericOption("minDaysBetweenAD")).toBe(0);
    });

    test("maxConsecutiveNights", () => {
        const config = new TurnusLintConfig();
        config.setNumericOption("maxConsecutiveNights", 5);
        expect(config.getNumericOption("maxConsecutiveNights")).toBe(5);
        config.setNumericOption("maxConsecutiveNights", 7);
        expect(config.getNumericOption("maxConsecutiveNights")).toBe(5);
        config.setNumericOption("maxConsecutiveNights", 10);
        expect(config.getNumericOption("maxConsecutiveNights")).toBe(5);
        config.setNumericOption("maxConsecutiveNights", 1);
        expect(config.getNumericOption("maxConsecutiveNights")).toBe(1);
        config.setNumericOption("maxConsecutiveNights", 0);
        expect(config.getNumericOption("maxConsecutiveNights")).toBe(1);
        config.setNumericOption("maxConsecutiveNights", -1);
        expect(config.getNumericOption("maxConsecutiveNights")).toBe(1);
    });
    test("maxConsecutiveNightsNightWorker", () => {
        const config = new TurnusLintConfig();
        config.setNumericOption("maxConsecutiveNightsNightWorker", 5);
        expect(config.getNumericOption("maxConsecutiveNightsNightWorker")).toBe(
            5,
        );
        config.setNumericOption("maxConsecutiveNightsNightWorker", 7);
        expect(config.getNumericOption("maxConsecutiveNightsNightWorker")).toBe(
            6,
        );
        config.setNumericOption("maxConsecutiveNightsNightWorker", 10);
        expect(config.getNumericOption("maxConsecutiveNightsNightWorker")).toBe(
            6,
        );
        config.setNumericOption("maxConsecutiveNightsNightWorker", 1);
        expect(config.getNumericOption("maxConsecutiveNightsNightWorker")).toBe(
            1,
        );
        config.setNumericOption("maxConsecutiveNightsNightWorker", 0);
        expect(config.getNumericOption("maxConsecutiveNightsNightWorker")).toBe(
            1,
        );
        config.setNumericOption("maxConsecutiveNightsNightWorker", -1);
        expect(config.getNumericOption("maxConsecutiveNightsNightWorker")).toBe(
            1,
        );
    });
    test("maxConsecutiveAShifts", () => {
        const config = new TurnusLintConfig();
        config.setNumericOption("maxConsecutiveAShifts", 5);
        expect(config.getNumericOption("maxConsecutiveAShifts")).toBe(5);
        config.setNumericOption("maxConsecutiveAShifts", 7);
        expect(config.getNumericOption("maxConsecutiveAShifts")).toBe(5);
        config.setNumericOption("maxConsecutiveAShifts", 10);
        expect(config.getNumericOption("maxConsecutiveAShifts")).toBe(5);
        config.setNumericOption("maxConsecutiveAShifts", 1);
        expect(config.getNumericOption("maxConsecutiveAShifts")).toBe(1);
        config.setNumericOption("maxConsecutiveAShifts", 0);
        expect(config.getNumericOption("maxConsecutiveAShifts")).toBe(1);
        config.setNumericOption("maxConsecutiveAShifts", -1);
        expect(config.getNumericOption("maxConsecutiveAShifts")).toBe(1);
    });
});
test("*BooleanOption", () => {
    const config = new TurnusLintConfig();
    expect(config.getBooleanOption("enableMaxConsecutiveASHifts")).toBe(false);
    expect(
        config.getBooleanOption("enableInferredYearlyWorkWeekendcount"),
    ).toBe(true);

    config.setBooleanOption("enableMaxConsecutiveASHifts", true);
    expect(config.getBooleanOption("enableMaxConsecutiveASHifts")).toBe(true);
    config.setBooleanOption("enableMaxConsecutiveASHifts", false);
    expect(config.getBooleanOption("enableMaxConsecutiveASHifts")).toBe(false);

    config.setBooleanOption("enableInferredYearlyWorkWeekendcount", false);
    expect(
        config.getBooleanOption("enableInferredYearlyWorkWeekendcount"),
    ).toBe(false);
    config.setBooleanOption("enableInferredYearlyWorkWeekendcount", true);
    expect(
        config.getBooleanOption("enableInferredYearlyWorkWeekendcount"),
    ).toBe(true);
});

test("*StringOption", () => {
    const config = new TurnusLintConfig();
    expect(config.getStringOption("displayMode")).toBe("report");
    config.setStringOption("displayMode", "table");
    expect(config.getStringOption("displayMode")).toBe("table");
    config.setStringOption("displayMode", "report");
    expect(config.getStringOption("displayMode")).toBe("report");
});

describe("callbacks", () => {
    test("numeric", () => {
        const config = new TurnusLintConfig();
        const listener = jest.fn();

        config.listen(listener);

        expect(config.getNumericOption("minDaysBetweenAD")).toBe(3);
        config.setNumericOption("minDaysBetweenAD", 5);

        expect(listener).toHaveBeenCalledWith(
            "OPTION_CHANGED",
            "minDaysBetweenAD",
            5,
            3,
        );
        listener.mockReset();

        config.setNumericOption("minDaysBetweenAD", 5);
        expect(listener).not.toHaveBeenCalled();

        config.setNumericOption("minDaysBetweenAD", 3);
        expect(listener).toHaveBeenCalledWith(
            "OPTION_CHANGED",
            "minDaysBetweenAD",
            3,
            5,
        );

        listener.mockReset();

        config.setNumericOption("minDaysBetweenAD", 99);
        expect(listener).toHaveBeenCalledWith(
            "OPTION_CHANGED",
            "minDaysBetweenAD",
            7,
            3,
        );
    });
    test("boolean", () => {
        const config = new TurnusLintConfig();
        const listener = jest.fn();

        config.listen(listener);

        expect(
            config.getBooleanOption("enableInferredYearlyWorkWeekendcount"),
        ).toBe(true);
        config.setBooleanOption("enableInferredYearlyWorkWeekendcount", false);

        expect(listener).toHaveBeenCalledWith(
            "OPTION_CHANGED",
            "enableInferredYearlyWorkWeekendcount",
            false,
            true,
        );
        listener.mockReset();
        config.setBooleanOption("enableInferredYearlyWorkWeekendcount", false);
        expect(listener).not.toHaveBeenCalled();
        config.setBooleanOption("enableInferredYearlyWorkWeekendcount", true);

        expect(listener).toHaveBeenCalledWith(
            "OPTION_CHANGED",
            "enableInferredYearlyWorkWeekendcount",
            true,
            false,
        );
    });
    test("string", () => {
        const config = new TurnusLintConfig();
        const listener = jest.fn();

        config.listen(listener);

        expect(config.getStringOption("displayMode")).toBe("report");

        config.setStringOption("displayMode", "table");
        expect(listener).toHaveBeenCalledWith(
            "OPTION_CHANGED",
            "displayMode",
            "table",
            "report",
        );

        listener.mockReset();

        config.setStringOption("displayMode", "table");
        expect(listener).not.toHaveBeenCalled();
    });
});

/*
 * @flow
 */
/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { getPersonCheckerWithData } from "./testHelpers";
import { getAndAnalyzePersonCheckerWithData } from "./testHelpers.js";

describe("PersonChecker", () => {
    describe("A/D", () => {
        test("fine", () => {
            const { checker } = getAndAnalyzePersonCheckerWithData(
                "A,D,A,A,A",
                (checker) => checker.checkAD(),
            );
            expect(checker.analysis.statistic("QUICK_RETURNS").value).toBe(1);
            expect(
                checker.analysis.statistic("QUICK_RETURNS"),
            ).toMatchSnapshot();
            expect(
                checker.analysis.statistic("QUICK_RETURNS_IN_WEEKEND").value,
            ).toBe(0);
            expect(
                checker.analysis.statistic("QUICK_RETURNS_IN_WEEKEND"),
            ).toMatchSnapshot();
            expect(checker.analysis.warnings().length).toBe(0);
        });
        test("stats", () => {
            const { checker } = getAndAnalyzePersonCheckerWithData(
                "A,D,A,D,A,D",
                (checker) => checker.checkAD(),
            );
            expect(checker.analysis.statistic("QUICK_RETURNS").value).toBe(3);
            expect(
                checker.analysis.statistic("QUICK_RETURNS_IN_WEEKEND").value,
            ).toBe(1);
        });
        test("too close", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "A,D,A,D",
                (checker) => checker.checkAD(),
            );
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "CLOSE_A/D",
                message: expect.stringMatching(
                    "tette A/D-vakter, måndag/tysdag og onsdag/torsdag",
                ),
            });
        });
    });
    describe("ShiftTypesInOneWeek", () => {
        test("fine", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,A,D,F2,F2,F2,F2,A,D",
                (checker) => checker.checkShiftTypesInOneWeek(),
            );
            expect(analysis.statistic("WEEKS_WITH_ALL_SHIFTS").value).toBe(0);
        });
        test("stats", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,A,N,F2,F2,F2,F2,A,D",
                (checker) => checker.checkShiftTypesInOneWeek(),
            );
            expect(analysis.statistic("WEEKS_WITH_ALL_SHIFTS").value).toBe(1);
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
        });
    });
    describe("checkN", () => {
        test("fine (none)", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,A,D,F2,F2,F2,F2,A,D",
                (checker) => checker.checkN(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.statistic("NIGHTS").value).toBe(0);
        });
        test("fine (with nights)", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "N,N,N,,F2,F2,F2,A,D,D,A,D,,D,D,D,D,D,F,D,D,D",
                (checker) => checker.checkN(),
            );
            expect(analysis.statistic("NIGHTS").value).toBe(3);
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
            expect(analysis._isNightWorker).toBe(false);
        });
        test("fine (night worker", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "N,N,N,N,N".split(","),
                    "N,N,N,N,N".split(","),
                    "N,N,N,N,N".split(","),
                    "N,N,N,N,N".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkN(),
            );
            expect(analysis.statistic("NIGHTS").value).toBe(20);
            expect(analysis.warnings().length).toBe(0);
            expect(analysis._isNightWorker).toBe(true);
        });
        test("Nights in a row stats", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "N,N,N,N".split(","),
                    "D,D,D,D,D".split(","),
                    "D,D,D,D,D".split(","),
                    "D,D,D,D,D".split(","),
                    "N,D,D,D,D".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkN(),
            );
            expect(analysis.statistic("NIGHTS").value).toBe(5);
            expect(analysis.statistic("MAX_NIGHTS_IN_A_ROW").value).toBe(4);
            expect(analysis.statistic("MIN_NIGHTS_IN_A_ROW").value).toBe(1);
        });
        test("Too many nights in a row (not night worker)", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "N,N,N,N".split(","),
                    "D,D,D,D,D".split(","),
                    "D,D,D,D,D".split(","),
                    "D,D,D,D,D".split(","),
                    "D,D,D,D,D".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkN(),
            );
            expect(analysis.statistic("NIGHTS").value).toBe(4);
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "NIGHTS_IN_A_ROW",
                message: expect.stringMatching("for mange netter på rad"),
            });
        });
        test("Too many nights in a row (night worker)", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "N,N,N,N,N,N".split(","),
                    "N,N,N,N,N".split(","),
                    "N,N,N,N,N".split(","),
                    "N,N,N,N,N".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkN(),
            );
            expect(analysis.statistic("NIGHTS").value).toBe(21);
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "NIGHTS_IN_A_ROW",
                message: expect.stringMatching("for mange netter på rad"),
            });
            expect(analysis._isNightWorker).toBe(true);
        });
    });
    describe("checkA", () => {
        test("fine (disabled)", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,A".split(","),
                    "A,A,A,A,A".split(","),
                    "A,A,A,A,A".split(","),
                    "A,A,A,A,A".split(","),
                    "A,A,A,A,A".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkA(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
        });
        test("fine (enabled)", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,D,D".split(","),
                    "A,A,A,D,D".split(","),
                    "A,A,A,D,D".split(","),
                    "A,A,A,D,D".split(","),
                    "A,A,A,D,D".split(","),
                ]: Array<Array<string>>),
                (checker, config) => {
                    config.setBooleanOption(
                        "enableMaxConsecutiveASHifts",
                        true,
                    );
                    checker.checkA();
                },
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
        });
        test("too many", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,D".split(","),
                    "A,A,A,D,D".split(","),
                    "A,A,A,D,D".split(","),
                    "A,A,A,D,D".split(","),
                    "A,A,A,D,D".split(","),
                ]: Array<Array<string>>),
                (checker, config) => {
                    config.setBooleanOption(
                        "enableMaxConsecutiveASHifts",
                        true,
                    );
                    checker.checkA();
                },
            );
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "A_IN_A_ROW",
                message: expect.stringMatching("for mange A-vakter på rad: 4"),
            });
        });
    });
    describe("checkConsecutive", () => {
        test("fine", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,A,D,F2,F2,F2,F2,A,D,D,D,F2,A,A",
                (checker) => checker.checkConsecutive(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.statistic("SHIFTS_IN_A_ROW").value).toBe(4);
            expect(analysis.statistic("D_IN_A_ROW").value).toBe(3);
            expect(analysis.statistic("A_IN_A_ROW").value).toBe(2);
        });
        test("fine (night worker)", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "N,N,N,N,N".split(","),
                    "N,N,N,N,N".split(","),
                    "N,N,N,N,N".split(","),
                    "N,N,N,N,N".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkConsecutive(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
            // Counts a night shift as crossing two days
            expect(analysis.statistic("SHIFTS_IN_A_ROW").value).toBe(6);
            expect(analysis.statistic("D_IN_A_ROW").value).toBe(0);
            expect(analysis.statistic("A_IN_A_ROW").value).toBe(0);
        });
        test("too many", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,A,D,F2,F2,F2,F2,A,D,D,D,F2,A,A,A,A,A,A",
                (checker) => checker.checkConsecutive(),
            );
            expect(analysis.statistic("SHIFTS_IN_A_ROW").value).toBe(6);
            expect(analysis.statistic("D_IN_A_ROW").value).toBe(3);
            expect(analysis.statistic("A_IN_A_ROW").value).toBe(6);
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "DAYS_IN_A_ROW",
                message: expect.stringMatching("for mange vaktdøgn på rad"),
            });
        });
        test("too many (night worker)", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "N,N,N,N,N,N".split(","),
                    ",N,N,N,N,N".split(","),
                    ",N,N,N,N,N".split(","),
                    ",N,N,N,N,N".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkConsecutive(),
            );
            expect(analysis.statistic("SHIFTS_IN_A_ROW").value).toBe(7);
            expect(analysis.statistic("D_IN_A_ROW").value).toBe(0);
            expect(analysis.statistic("A_IN_A_ROW").value).toBe(0);
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "DAYS_IN_A_ROW",
                message: expect.stringMatching("for mange vaktdøgn på rad"),
            });
        });
    });
    describe("checkPreN", () => {
        test("fine", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "N,N,N,,,A,D",
                (checker) => checker.checkPreN(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
        });
        test("D after N", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "N,N,N,,,D",
                (checker) => checker.checkPreN(),
            );
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "D_AFTER_N",
                message: expect.stringMatching("problematisk døgnrytme"),
            });
        });
        test("D before N", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,N,N,N",
                (checker) => checker.checkPreN(),
            );
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "D_BEFORE_N",
                message: expect.stringMatching("problematisk døgnrytme"),
            });
        });
        test("missing F after N", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "N,N,N,A",
                (checker) => checker.checkPreN(),
            );
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "MISSING_F_AFTER_N",
                message: expect.stringMatching(
                    "manglar fri etter N på torsdag",
                ),
            });
        });
    });
    describe("checkF1", () => {
        test("fine", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "A,D,D,D,D,,F1",
                (checker) => checker.checkF1(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
        });
        test("fine weekend", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,D,D,F1,D,A,D",
                (checker) => checker.checkF1(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
        });
        test("early", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "F1,A,D,D,D,D,A",
                (checker) => checker.checkF1(),
            );
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "EARLY_F1",
                message: expect.stringMatching("tidleg F1 dag"),
            });
        });
        test("not on sunday", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "F1,A,D,D,D,D",
                (checker) => checker.checkF1(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(1);
            expect(analysis.notes()[0]).toMatchObject({
                type: "MISPLACED_F1",
                message: expect.stringMatching("F1 ikkje på sundag"),
            });
        });
        test("missing", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "A,D,D,D,D",
                (checker) => checker.checkF1(),
            );
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "MISSING_F1",
                message: expect.stringMatching("manglar F1-dag"),
            });
        });
        test("early due to F3 is fine", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "A,D,F1,F3,D,A,D",
                (checker) => checker.checkF1(),
                "20210329",
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
        });
    });
    describe("checkWeekends", () => {
        test("fine", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,D,".split(","),
                    "A,A,A,A,".split(","),
                    "A,A,A,A,F1,A,A".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkWeekends(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.statistic("WEEKENDS").value).toBe(2);
        });
        test("Two in a row", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,D,".split(","),
                    "A,A,A,A,F1,A,A".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkWeekends(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(2);
            expect(analysis.notes()[0]).toMatchObject({
                type: "WEEKENDS_TOO_CLOSE",
                message: expect.stringMatching("2 helger på rad"),
            });
            expect(analysis.notes()[1]).toMatchObject({
                type: "WEEKENDS_TOO_CLOSE",
                message: expect.stringMatching("helger følg ikkje 3.-kvar"),
            });
            expect(analysis.statistic("WEEKENDS").value).toBe(3);
        });
        test("Three in a row", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,D,".split(","),
                    "A,A,A,A,F1,A,A".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkWeekends(),
            );
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(1);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "WEEKENDS_TOO_CLOSE",
                message: expect.stringMatching("3 helger på rad"),
            });
            expect(analysis.notes()[0]).toMatchObject({
                type: "WEEKENDS_TOO_CLOSE",
                message: expect.stringMatching("helger følg ikkje 3.-kvar"),
            });
            expect(analysis.statistic("WEEKENDS").value).toBe(4);
        });
        test("Late F after weekend", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                (["A,A,A,A,F1,A,A".split(","), "A,A,A,F".split(",")]: Array<
                    Array<string>,
                >),
                (checker) => checker.checkWeekends(),
            );
            expect(analysis.warnings().length).toBe(1);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "LATE_F_AFTER_WEEKEND",
                message: expect.stringMatching(
                    "sein fridag etter helg \\(fredag\\)",
                ),
            });
        });
        test("Strange F", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,A,D,D,D,F3,F1",
                (checker) => checker.checkWeekends(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(1);
            expect(analysis.notes()[0]).toMatchObject({
                type: "STRANGE_F_PLACEMENT_ON_SATURDAY",
                message: expect.stringMatching(
                    "rar plassering av fridagskode på ein laurdag",
                ),
            });
        });
        test("Strange F2 (should be ignored)", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,A,D,D,D,F2,F1",
                (checker) => checker.checkWeekends(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
        });
        test("Inferred weekend count", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "D,D,D,D,F1,D,D",
                (checker) => checker.checkWeekends(),
                "20210322",
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.statistic("WEEKENDS").value).toBe(1);
            expect(
                analysis.statistic("INFERRED_STAIRCASE_WEEKENDS").tietaryValue,
            ).toBe(4);
            expect(
                analysis.statistic("INFERRED_STAIRCASE_WEEKENDS").value,
            ).toBe(5);
        });
    });
    describe("checkWeeksOff", () => {
        test("stats", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,A,".split(","),
                    [],
                    [],
                    [],
                    [],
                    [],
                    "A,A,A,A,F1,A,A".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkWeeksOff(),
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.statistic("WEEKS_OFF").value).toBe(5);
            expect(analysis.statistic("WEEKS_OFF")).toMatchSnapshot();
        });
    });
    describe("checkRedDays", () => {
        /* Red days in april of 2021:
         * mon-fri: 1,2,5
         * weekend: 4
         */
        test("fine", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F3,".split(","),
                    "F3,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,A,".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkRedDays(),
                "20210329",
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.statistic("RED_DAYS").value).not.toBe("ingen");
            expect(analysis.statistic("RED_DAYS")).toMatchSnapshot();
            expect(analysis.statistic("SPECIAL_DAYS").value).toBe("ingen");
        });
        test("F1 on F3", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,A,".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkRedDays(),
                "20210329",
            );
            expect(analysis.warnings().length).toBe(2);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.warnings()[0]).toMatchObject({
                type: "WRONG_OR_MISSING_F3",
                message: expect.stringMatching(
                    "feil fridagskode på.*Har F1, skulle vere F3",
                ),
            });
            expect(analysis.warnings()[1]).toMatchObject({
                type: "WRONG_OR_MISSING_F3",
                message: expect.stringMatching(
                    "feil fridagskode på.*Har F2, skulle vere F3",
                ),
            });
            expect(analysis.statistic("RED_DAYS").value).not.toBe("ingen");
            expect(analysis.statistic("RED_DAYS")).toMatchSnapshot();
            expect(analysis.statistic("SPECIAL_DAYS").value).toBe(
                "Påskeaftan (laurdag 03/04 - A)",
            );
        });
        test("jul", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                (["A,A,A,A,F1,A,A".split(",")]: Array<Array<string>>),
                (checker) => checker.checkRedDays(),
                "20211220",
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
            expect(analysis.statistic("RED_DAYS").value).toBe("ingen");
            expect(analysis.statistic("SPECIAL_DAYS").value).toBe(
                "Litle julaftan (torsdag 23/12 - A), 1. juledag (laurdag 25/12 - A), 2. juledag (sundag 26/12 - A)",
            );
            expect(analysis.statistic("SPECIAL_DAYS").secondaryValue).toBe("3");
        });
    });
    describe("checkStandalone", () => {
        test("fine", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F3,".split(","),
                    "F3,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,A,".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkStandalone(),
                "20210329",
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(0);
        });
        test("Lonely", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F3,".split(","),
                    "F3,A,,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,A,".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.checkStandalone(),
                "20210329",
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(2);
            for (const note of analysis.notes()) {
                expect(note).toMatchObject({
                    type: "LONELY_SHIFT",
                    message: expect.stringMatching("einsam vakt"),
                });
            }
        });
        test("One shift week", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "A",
                (checker) => checker.checkStandalone(),
                "20210329",
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(1);
            expect(analysis.notes()[0]).toMatchObject({
                type: "ONE_SHIFT_WEEK",
                message: expect.stringMatching("veke med berre ei enkelt vakt"),
            });
        });
    });
    describe("gatherShiftStats", () => {
        test("stats", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F3,".split(","),
                    "F3,A,,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,A,".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.gatherShiftStats(),
                "20210329",
            );
            expect(analysis.statistic("SHIFT_CODE_SPREAD")).toMatchSnapshot();
            expect(
                analysis.statistic("SHIFT_GENERIC_SPREAD"),
            ).toMatchSnapshot();
            expect(analysis.statistic("FREE_CODE_SPREAD")).toMatchSnapshot();
            expect(
                analysis.statistic("SINGLE_SHIFT_CODE_WEEKS"),
            ).toMatchSnapshot();
        });
    });
    describe("countWeekendTypes", () => {
        test("stats", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                ([
                    "A,A,A,A,F3,".split(","),
                    "F3,A,,A,F1,A,A".split(","),
                    "F2,A,A,A,F1,A,A".split(","),
                    "F2,A,A,A,,".split(","),
                    "A,A,,A,A,A,A".split(","),
                    "A,A,A,A,F1,A,A".split(","),
                ]: Array<Array<string>>),
                (checker) => checker.countWeekendTypes(),
                "20210329",
            );
            expect(analysis.statistic("LONG_WEEKENDS").value).toBe(2);
            expect(analysis.statistic("SHORT_WORK_WEEKENDS").value).toBe(2);
        });
    });
    describe("addBootstrappedNotes", () => {
        test("functionality", () => {
            const { analysis } = getAndAnalyzePersonCheckerWithData(
                "",
                (checker) => checker.addBootstrappedNotes(),
                "20210329",
                "Person",
                [
                    {
                        message: "test",
                        week: 1,
                    },
                ],
            );
            expect(analysis.warnings().length).toBe(0);
            expect(analysis.notes().length).toBe(1);
            expect(analysis.notes()[0]).toMatchObject({
                type: "BOOTSTRAPPED_NOTE",
                message: "test",
            });
        });
    });
    test("_getDateNDaysLater", () => {
        const { checker } = getPersonCheckerWithData("AAAA");
        const date = new Date(2021, 8, 26);
        const sevenDaysLater = checker._getDateNDaysLater(date, 7);
        expect(sevenDaysLater.getDay()).toBe(0);
        expect(sevenDaysLater.getDate()).toBe(3);
        expect(sevenDaysLater.getMonth()).toBe(9);
        const sevenDaysLaterRawCall = checker._dateAddOrSubtractNDays(
            date,
            7,
            "add",
        );
        expect(sevenDaysLaterRawCall.getDay()).toBe(0);
        expect(sevenDaysLaterRawCall.getDate()).toBe(3);
        expect(sevenDaysLaterRawCall.getMonth()).toBe(9);
    });
    test("_getDateNDaysEarlier", () => {
        const { checker } = getPersonCheckerWithData("AAAA");
        const date = new Date(2021, 8, 26);
        const sevenDaysEarlier = checker._getDateNDaysEarlier(date, 7);
        expect(sevenDaysEarlier.getDay()).toBe(0);
        expect(sevenDaysEarlier.getDate()).toBe(19);
        expect(sevenDaysEarlier.getMonth()).toBe(8);
        const sevenDaysEarlierRawCall = checker._dateAddOrSubtractNDays(
            date,
            7,
            "subtract",
        );
        expect(sevenDaysEarlierRawCall.getDay()).toBe(0);
        expect(sevenDaysEarlierRawCall.getDate()).toBe(19);
        expect(sevenDaysEarlierRawCall.getMonth()).toBe(8);
    });
});

/*
 * @flow
 */
/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { JobRunner } from "../src/JobRunner.js";

test("queueJob", () => {
    const runner = new JobRunner();
    expect(runner._queue).toHaveLength(0);
    runner.queueJob(() => {
        return false;
    });
    expect(runner._queue).toHaveLength(1);
});
describe("runJobs", () => {
    test("single job", async () => {
        let iterNo = 0;
        const fakeJob = jest.fn(() => {
            return ++iterNo < 100;
        });
        const runner = new JobRunner();
        expect(runner._queue).toHaveLength(0);
        runner.queueJob(fakeJob);
        await runner.runJobs();
        expect(iterNo).toBe(100);
        expect(fakeJob).toHaveBeenCalledTimes(iterNo);
    });
    test("multiple jobs", async () => {
        let iterOne = 0;
        let iterTwo = 0;
        const fakeJob1 = jest.fn(() => {
            return ++iterOne < 100;
        });
        const fakeJob2 = jest.fn(() => {
            return ++iterTwo < 200;
        });
        const runner = new JobRunner();
        expect(runner._queue).toHaveLength(0);
        runner.queueJob(fakeJob1);
        runner.queueJob(fakeJob2);
        await runner.runJobs();
        expect(iterOne).toBe(100);
        expect(iterTwo).toBe(200);
        expect(fakeJob1).toHaveBeenCalledTimes(iterOne);
        expect(fakeJob2).toHaveBeenCalledTimes(iterTwo);
    });
    test("multiple jobs, uneven", async () => {
        let iterOne = 0;
        let iterTwo = 0;
        const fakeJob1 = jest.fn(() => {
            return ++iterOne < 37;
        });
        const fakeJob2 = jest.fn(() => {
            return ++iterTwo < 221;
        });
        const runner = new JobRunner();
        expect(runner._queue).toHaveLength(0);
        runner.queueJob(fakeJob1);
        runner.queueJob(fakeJob2);
        await runner.runJobs();
        expect(iterOne).toBe(37);
        expect(iterTwo).toBe(221);
        expect(fakeJob1).toHaveBeenCalledTimes(iterOne);
        expect(fakeJob2).toHaveBeenCalledTimes(iterTwo);
    });
    test("multi-call protection", async () => {
        let iter = 0;
        const runner = new JobRunner();
        expect(runner._queue).toHaveLength(0);
        const fakeJob = jest.fn(() => {
            expect(() => {
                runner.runJobs();
            }).toThrow(
                "Attempt to runJobs() on object that is already running",
            );
            return ++iter > 100;
        });
        runner.queueJob(fakeJob);
        await runner.runJobs();
        expect(fakeJob).toHaveBeenCalled();
    });
});
describe("runBeforeEventLoop", () => {
    describe("runJobs", () => {
        test("single job", async () => {
            let iterNo = 0;
            const fakeJob = jest.fn(() => {
                return ++iterNo < 100;
            });
            const fakeEventloopListener = jest.fn();
            const runner = new JobRunner();
            expect(runner._queue).toHaveLength(0);
            runner.runBeforeEventLoop(fakeEventloopListener);
            runner.queueJob(fakeJob);
            await runner.runJobs();
            expect(iterNo).toBe(100);
            expect(fakeJob).toHaveBeenCalledTimes(iterNo);
            expect(fakeEventloopListener).toHaveBeenCalledTimes(2);
        });
        test("multiple jobs", async () => {
            let iterOne = 0;
            let iterTwo = 0;
            const fakeJob1 = jest.fn(() => {
                return ++iterOne < 100;
            });
            const fakeJob2 = jest.fn(() => {
                return ++iterTwo < 200;
            });
            const fakeEventloopListener = jest.fn();
            const runner = new JobRunner();
            expect(runner._queue).toHaveLength(0);
            runner.runBeforeEventLoop(fakeEventloopListener);
            runner.queueJob(fakeJob1);
            runner.queueJob(fakeJob2);
            await runner.runJobs();
            expect(fakeEventloopListener).toHaveBeenCalledTimes(6);
            expect(fakeJob1).toHaveBeenCalledTimes(iterOne);
            expect(fakeJob2).toHaveBeenCalledTimes(iterTwo);
        });
    });
    test("attempt to re-queue", () => {
        const fakeEventloopListener = jest.fn();
        const runner = new JobRunner();
        runner.runBeforeEventLoop(fakeEventloopListener);
        expect(() => {
            runner.runBeforeEventLoop(fakeEventloopListener);
        }).toThrow("Attempt to re-queue a new runBeforeEventLoop function");
    });
});

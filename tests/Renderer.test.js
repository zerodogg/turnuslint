/*
 * @flow
 * @jest-environment jsdom
 */
/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import {
    CollapseArrow,
    MaybeCollapse,
    MonthCounterRenderer,
    NotesSectionRenderer,
    PersonAnalysisNotesRenderer,
    PersonNameHeader,
    PersonReportRenderer,
    PersonStatisticsRenderer,
    ShiftNoticeRenderer,
    SimpleTooltip,
    SummaryItemRenderer,
} from "../src/Renderer";
import * as React from "react";
import { fakeDataGenerator } from "./testHelpers";
import { click } from "@testing-library/user-event/dist/click";

jest.useFakeTimers();

describe("SimpleTooltip", () => {
    test("Basic rendering and interaction", async () => {
        render(<SimpleTooltip tooltip="test">Hello world</SimpleTooltip>);
        expect(screen.getByText("Hello world")).toBeInTheDocument();
        expect(screen.queryByText("test")).not.toBeInTheDocument();
        userEvent.hover(screen.getByText("Hello world"));
        jest.advanceTimersByTime(500);
        expect(screen.queryByText("test")).toBeInTheDocument();
    });
});

describe("ShiftNoticeRenderer", () => {
    describe("Basic rendering", () => {
        test("not important, not hidden", () => {
            const notice = fakeDataGenerator.getFakeShiftNotice({});
            const { container } = render(
                <ShiftNoticeRenderer
                    entry={notice}
                    person={fakeDataGenerator.getFakeShiftAnalysisContainer()}
                />,
            );
            expect(container).toMatchSnapshot();
        });
        test("important", () => {
            const notice = fakeDataGenerator.getFakeShiftNotice({});
            const { container } = render(
                <ShiftNoticeRenderer
                    important
                    entry={notice}
                    person={fakeDataGenerator.getFakeShiftAnalysisContainer()}
                />,
            );
            expect(container).toMatchSnapshot();
        });
        test("hidden", () => {
            const notice = fakeDataGenerator.getFakeShiftNotice({});
            const { container } = render(
                <ShiftNoticeRenderer
                    hidden
                    entry={notice}
                    person={fakeDataGenerator.getFakeShiftAnalysisContainer()}
                />,
            );
            expect(container).toMatchSnapshot();
        });
    });
    describe("Interaction tests", () => {
        test("mark important", () => {
            const notice = fakeDataGenerator.getFakeShiftNotice({});
            const person = fakeDataGenerator.getFakeShiftAnalysisContainer();
            const { getByText } = render(
                <ShiftNoticeRenderer entry={notice} person={person} />,
            );
            expect(person.isImportant(notice)).toBe(false);
            click(getByText("Viktig"));
            expect(person.isImportant(notice)).toBe(true);
        });
        test("mark hidden", () => {
            const notice = fakeDataGenerator.getFakeShiftNotice({});
            const person = fakeDataGenerator.getFakeShiftAnalysisContainer();
            const { getByText } = render(
                <ShiftNoticeRenderer entry={notice} person={person} />,
            );
            expect(person.isHidden(notice)).toBe(false);
            click(getByText("Skjul"));
            expect(person.isHidden(notice)).toBe(true);
        });
        test("unmark important", () => {
            const notice = fakeDataGenerator.getFakeShiftNotice({});
            const person = fakeDataGenerator.getFakeShiftAnalysisContainer();
            person.markImportant(notice);
            const { getByText } = render(
                <ShiftNoticeRenderer
                    important
                    entry={notice}
                    person={person}
                />,
            );
            click(getByText("Ikkje viktig"));
            expect(person.isImportant(notice)).toBe(false);
        });
        test("unmark hidden", () => {
            const notice = fakeDataGenerator.getFakeShiftNotice({});
            const person = fakeDataGenerator.getFakeShiftAnalysisContainer();
            person.markHidden(notice);
            const { getByText } = render(
                <ShiftNoticeRenderer hidden entry={notice} person={person} />,
            );
            click(getByText("Ikkje skjul"));
            expect(person.isHidden(notice)).toBe(false);
        });
    });
});
describe("MaybeCollapse", () => {
    test("Empty", () => {
        const { container } = render(<MaybeCollapse isOpen={false} />);
        expect(container).toMatchSnapshot();
    });
    test("With content", () => {
        const { container } = render(
            <MaybeCollapse isOpen={false} text="Element" />,
        );
        expect(container).toMatchSnapshot();
    });
    test("With react content", () => {
        const node = <div />;
        const { container } = render(
            <MaybeCollapse isOpen={false} text={node} />,
        );
        expect(container).toMatchSnapshot();
    });
});
test("MonthCounterRenderer", () => {
    const months: Map<string, number> = new Map();
    for (let no = 0; no < 12; no++) {
        months.set(no + "", no);
    }
    const { container } = render(<MonthCounterRenderer months={months} />);
    expect(container).toMatchSnapshot();
});
describe("SummaryItemRenderer", () => {
    test("Basic rendering", () => {
        const { container } = render(
            <table>
                <tbody>
                    <SummaryItemRenderer value="Testvalue" type="D_IN_A_ROW" />
                </tbody>
            </table>,
        );
        expect(container).toMatchSnapshot();
    });
});
describe("PersonStatisticsRenderer", () => {
    test("Basic rendering", () => {
        const person = fakeDataGenerator.getFakeShiftAnalysisContainer();
        fakeDataGenerator.fillShiftAnalysiscontainerStats(person);
        const { container } = render(
            <PersonStatisticsRenderer person={person} />,
        );
        expect(container).toMatchSnapshot();
    });
});

test.todo("CollapsableReportSection");

describe("NotesSectionRenderer", () => {
    test("Basic rendering", () => {
        const notice = fakeDataGenerator.getFakeShiftNotice({});
        const person = fakeDataGenerator.getFakeShiftAnalysisContainer();
        const { container } = render(
            <NotesSectionRenderer
                header="header"
                entries={[notice]}
                person={person}
            />,
        );
        expect(container).toMatchSnapshot();
    });
    test.todo("Functionality");
});
describe("PersonAnalysisNotesRenderer", () => {
    test("Basic rendering", () => {
        const person = fakeDataGenerator.getFakeShiftAnalysisContainer();
        person.addWarning(
            "CLOSE_A/D",
            1,
            "tette A/D-vakter, " +
                1 +
                " og " +
                1 +
                " (berre " +
                0 +
                " " +
                " mellom)",
        );
        const { container } = render(
            <PersonAnalysisNotesRenderer person={person} />,
        );
        expect(container).toMatchSnapshot();
    });
});
describe("CollapseArrow", () => {
    test("Basic rendering", () => {
        expect(() => {
            render(<CollapseArrow open={false} />);
        }).not.toThrow();
    });
    test.todo("Test functionality/props usage");
});
describe("PersonNameHeader", () => {
    describe("Snapshots", () => {
        test("Visible", () => {
            const { container } = render(
                <PersonNameHeader
                    ignored={false}
                    name="Test"
                    onToggle={() => {}}
                />,
            );
            expect(container).toMatchSnapshot();
        });
        test("Hidden", () => {
            const { container } = render(
                <PersonNameHeader
                    ignored={true}
                    name="Test"
                    onToggle={() => {}}
                />,
            );
            expect(container).toMatchSnapshot();
        });
    });
    describe("Functionality", () => {
        test("Hide", () => {
            const toggle = jest.fn();
            const { getByText } = render(
                <PersonNameHeader
                    ignored={false}
                    name="Test"
                    onToggle={toggle}
                />,
            );
            expect(toggle).not.toHaveBeenCalled();
            click(getByText("Skjul person"));
            expect(toggle).toHaveBeenCalled();
        });
        test("Show", () => {
            const toggle = jest.fn();
            const { getByText } = render(
                <PersonNameHeader
                    ignored={true}
                    name="Test"
                    onToggle={toggle}
                />,
            );
            expect(toggle).not.toHaveBeenCalled();
            click(getByText("Vis person"));
            expect(toggle).toHaveBeenCalled();
        });
    });
});
describe("PersonReportRenderer", () => {
    test("Basic rendering", () => {
        const person = fakeDataGenerator.getFakeShiftAnalysisContainer();
        const { container } = render(<PersonReportRenderer person={person} />);
        expect(container).toMatchSnapshot();
    });
    test.todo("Functionality (events)");
});

/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */

import type {
    DateObjectLiteral,
    WeekToDateMap,
    WeekToDateType,
    BootstrappedNote,
} from "../src/sharedtypes.js";
import type { GatData } from "../src/GatParser.js";
import {
    PersonChecker,
    ShiftAnalysisContainer,
    ShiftFeedbackFormatter,
} from "../src/TurnusLint";
import { TurnusLintLogger } from "../src/logger";
import { TurnusLintConfig } from "../src/Config";
import type { ShiftList } from "../src/ShiftIdentifier";
import type { ShiftNotice, ShiftNoticeSubtypes } from "../src/TurnusLint";
import getISOWeek from "date-fns/getISOWeek";
import addDays from "date-fns/addDays";
import dateParse from "date-fns/parse";
import { Holidays } from "../src/HolidaysClass";

const shiftOff = {
    start: {
        decimal: 0,
        hour: 0,
        minute: 0,
    },
    end: {
        decimal: 0,
        hour: 0,
        minute: 0,
    },
    off: true,
    crossesDays: false,
    shiftLength: 0,
};
type acceptableShiftParameters = Array<string> | string | Array<Array<string>>;
const fakeShiftList: ShiftList = {
    A: {
        start: {
            decimal: 14.5,
            hour: 14,
            minute: 30,
        },
        end: {
            decimal: 22,
            hour: 22,
            minute: 0,
        },
        off: false,
        crossesDays: false,
        shiftLength: 7.5,
    },
    D: {
        start: {
            decimal: 7.0,
            hour: 7,
            minute: 0,
        },
        end: {
            decimal: 15.0,
            hour: 15,
            minute: 0,
        },
        off: false,
        crossesDays: false,
        shiftLength: 8,
    },
    N: {
        start: {
            decimal: 21.5,
            hour: 21,
            minute: 30,
        },
        end: {
            decimal: 7.0,
            hour: 7,
            minute: 0,
        },
        off: false,
        crossesDays: true,
        shiftLength: 8,
    },
    F1: shiftOff,
    F2: shiftOff,
    F3: shiftOff,
};

function getFakeGatData(
    schedule: acceptableShiftParameters,
    startDate: string = "20210301",
    name: string = "Test person",
): GatData {
    const shiftMap = {};
    shiftMap[name] = [schedule];
    const nameToHash = {};
    nameToHash[name] = name;
    const weekToDate = {};
    weekToDate[name] = getFakeWeekToDateData(startDate, schedule.length);
    return {
        shifts: fakeShiftList,
        notes: {},
        people: shiftMap,
        weekToDate,
        nameToHash,
    };
}

type PersonCheckerReturn = {|
    logger: TurnusLintLogger,
    config: TurnusLintConfig,
    checker: PersonChecker,
    holidays: Holidays,
|};

type PersonCheckerWithAnalysis = {|
    ...PersonCheckerReturn,
    analysis: ShiftAnalysisContainer,
|};
function getAndAnalyzePersonCheckerWithData(
    schedule: acceptableShiftParameters,
    analyzeCallback: null | ((PersonChecker, TurnusLintConfig) => void) = null,
    startDate: string = "20210301",
    name: string = "Test person",
    bootstrappedNotes: Array<BootstrappedNote> = [],
): PersonCheckerWithAnalysis {
    const data = getPersonCheckerWithData(
        schedule,
        startDate,
        name,
        bootstrappedNotes,
    );
    if (analyzeCallback === null) {
        data.checker.analyze();
    } else {
        analyzeCallback(data.checker, data.config);
        data.checker._runQueuedAnalyzers();
    }
    return {
        analysis: data.checker.analysis,
        ...data,
    };
}

function getPersonCheckerWithData(
    schedule: acceptableShiftParameters,
    startDate: string = "20210301",
    name: string = "Test person",
    bootstrappedNotes: Array<BootstrappedNote> = [],
): PersonCheckerReturn {
    const generatedSchedule = buildScheduleArray(schedule);
    const numberOfEntries = generatedSchedule.reduce(
        (prevValue, currentValue) => prevValue + currentValue.length,
        0,
    );
    const logger = new TurnusLintLogger();
    const config = new TurnusLintConfig();
    const holidays = new Holidays(logger);
    const checker = new PersonChecker(
        name,
        fakeShiftList,
        generatedSchedule,
        getFakeWeekToDateData(startDate, numberOfEntries),
        config,
        logger,
        bootstrappedNotes,
        name,
        holidays,
    );
    return { logger, config, checker, holidays };
}

function getFakeWeekToDateData(
    startDate: string,
    length: number,
): WeekToDateType {
    let date = dateParse(startDate, "yyyyMMdd", new Date());
    const data: WeekToDateType = {};
    const weeksLength = length / 7;
    let weekNo = 0;
    while (Object.keys(data).length < weeksLength) {
        const result: WeekToDateMap = {};
        for (let i = 1; i < 8; i++) {
            result[i] = ({
                year: date.getFullYear(),
                month: date.getMonth() + 1,
                day: date.getDate(),
                week: getISOWeek(date),
            }: DateObjectLiteral);

            date = addDays(date, 1);
        }
        data[++weekNo + ""] = result;
    }
    return data;
}

function fillScheduleArray(schedule: Array<string>): Array<string> {
    while (schedule.length === 0 || schedule.length % 7 !== 0) {
        schedule.push("");
    }
    return schedule;
}

function buildScheduleArray(
    schedule: acceptableShiftParameters,
): Array<Array<string>> {
    if (typeof schedule === "string") {
        schedule = schedule.split(",");
    }
    if (Array.isArray(schedule[0])) {
        const result = [];
        for (const entry of schedule) {
            if (!Array.isArray(entry)) {
                throw "Mixed type schedule arrays are unsupported";
            }
            fillScheduleArray(entry);
            result.push(entry);
        }
        return result;
    } else {
        const result = [];
        let processing: Array<string> = [];
        for (let partI = 0; partI < schedule.length; partI++) {
            const entry = schedule[partI];
            if (typeof entry !== "string") {
                throw "Mixed type schedule arrays are unsupported";
            }
            processing.push(entry);
            if (processing.length === 7) {
                result.push(processing);
                processing = [];
            }
        }
        if (processing.length > 0) {
            fillScheduleArray(processing);
            result.push(processing);
        }
        return result;
    }
}

type ShiftNoticeFakeOpts = {|
    week?: string,
    type?: ShiftNoticeSubtypes,
    message?: string,
    tooltipText?: string | null,
    calendarWeek?: number,
    calendarYear?: number,
    key?: string,
|};
const fakeDataGenerator = {
    /**
     * Gets a a stubbed ShiftAnalysisContainer, optionally using `config` as the
     * config (auto-instantiates a TurnusLintConfig if config is omitted).
     */
    getFakeShiftAnalysisContainer(
        config?: TurnusLintConfig,
    ): ShiftAnalysisContainer {
        if (config === undefined) {
            config = new TurnusLintConfig();
        }
        return new ShiftAnalysisContainer(
            getFakeWeekToDateData("20200101", 52),
            "Namn Namnesen",
            config,
            new ShiftFeedbackFormatter(
                getFakeWeekToDateData("20200101", 52),
                "Namn Namnesen",
            ),
            "fakeHashForNamnNamnesen",
        );
    },

    /**
     * Fills a ShiftAnalysisContainer with fake statistics data
     */
    fillShiftAnalysiscontainerStats(
        container: ShiftAnalysisContainer,
    ): ShiftAnalysisContainer {
        const secondaryValue = "secondaryValue";
        container.setStatistic("D_IN_A_ROW", 5);
        container.setStatistic("A_IN_A_ROW", 3);
        container.setStatistic("SHIFTS_IN_A_ROW", 5);
        container.setStatistic("WEEKS_OFF", 5);
        container.setStatistic("RED_DAYS", "Dag 1, dag 2, dag 3", {
            secondaryValue: "3",
        });
        container.setStatistic("SPECIAL_DAYS", "Dag 5, dag 6", {
            secondaryValue: "2",
        });
        container.setStatistic("QUICK_RETURNS", 23);
        container.setStatistic("QUICK_RETURNS_IN_WEEKEND", 13);
        container.setStatistic("WEEKENDS", 16, {
            secondaryValue: "secondaryValue",
        });
        container.setStatistic("INFERRED_STAIRCASE_WEEKENDS", 18, {
            secondaryValue: "2020",
            tietaryValue: 2,
        });
        container.setStatistic("NIGHTS", 24, {
            metaObject: {
                type: "MONTH_SPREAD",
                months: fakeDataGenerator.getMonthSpreadObject(),
            },
        });
        container.setStatistic("MIN_NIGHTS_IN_A_ROW", 1);
        container.setStatistic("MAX_NIGHTS_IN_A_ROW", 3);
        container.setStatistic("FREE_CODE_SPREAD", "F1 20%");
        container.setStatistic("SINGLE_SHIFT_CODE_WEEKS", 10, {
            secondaryValue,
        });
        container.setStatistic("SHIFT_GENERIC_SPREAD", "F1 20%");
        container.setStatistic("WEEKS_WITH_ALL_SHIFTS", 2, { secondaryValue });
        container.setStatistic("SHORT_WORK_WEEKENDS", 2, { secondaryValue });
        container.setStatistic("LONG_WEEKENDS", 2, { secondaryValue });
        container.setStatistic("SHIFT_CODE_SPREAD", "F1 20%");
        return container;
    },

    getMonthSpreadObject(): Map<string, number> {
        const months: Map<string, number> = new Map();
        for (let no = 0; no < 12; no++) {
            months.set(no + "", no);
        }
        return months;
    },

    /**
     * Gets a ShiftNotice object based upon the one provided in `options` (which can be empty)
     */
    getFakeShiftNotice(options: ShiftNoticeFakeOpts): ShiftNotice {
        const fakeDefaults: ShiftNotice = {
            week: "1",
            type: "CLOSE_A/D",
            message: "Close A/D",
            tooltipText: "tooltip",
            calendarWeek: 1,
            calendarYear: 2020,
            key: "CLOSE_AD_FAKE",
        };
        return Object.assign(fakeDefaults, options);
    },
};

export {
    getFakeGatData,
    getPersonCheckerWithData,
    getAndAnalyzePersonCheckerWithData,
    fakeDataGenerator,
};

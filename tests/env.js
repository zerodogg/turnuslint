import "regenerator-runtime/runtime";
jest.mock("idb-keyval", () => {
    let idbKeyvalStore = {};
    return {
        set: (key, val) => (idbKeyvalStore[key] = val),
        setMany: (list) => {
            for (const entry of list) {
                const key = entry[0];
                const val = entry[1];
                idbKeyvalStore[key] = val;
            }
        },
        getMany: (list) => {
            return {
                then: (cb) => {
                    const result = [];
                    for (const key of list) {
                        result.push(idbKeyvalStore[key]);
                    }
                    cb(result);
                    return { catch: () => {} };
                },
            };
        },
        get: (key) => {
            return {
                then: (cb) => cb(idbKeyvalStore[key]),
            };
        },
        clearIdbMockData: () => {
            idbKeyvalStore = {};
        },
    };
});
/* global global */
global.TURNUSLINT_DEBUG = false;
global.TURNUSLINT_DEBUG_UNHANDLED_LINES = false;
global.USE_LIMITED_RULESET = false;
global.GIT_REVISION = "(git)";
global.GIT_REVISION_FULL = "git";
global.BUILD_DATE = "0000-00-00";

/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import { ShiftIdentifier } from "../src/ShiftIdentifier.js";
import type { ShiftList } from "../src/ShiftIdentifier.js";
import { TurnusLintLogger } from "../src/logger.js";
const shifts: ShiftList = {
    D: {
        start: { hour: 7, minute: 0, decimal: 7 },
        end: { hour: 15, minute: 0, decimal: 15 },
        off: false,
        crossesDays: false,
        shiftLength: 8,
    },
    D09: {
        start: { hour: 9, minute: 0, decimal: 9 },
        end: { hour: 17, minute: 0, decimal: 17 },
        off: false,
        crossesDays: false,
        shiftLength: 8,
    },
    A: {
        start: { hour: 14, minute: 30, decimal: 14.5 },
        end: { hour: 22, minute: 0, decimal: 22 },
        off: false,
        crossesDays: false,
        shiftLength: 7.5,
    },
    A1: {
        start: { hour: 14, minute: 0, decimal: 14 },
        end: { hour: 22, minute: 0, decimal: 22 },
        off: false,
        crossesDays: false,
        shiftLength: 8,
    },
    N: {
        start: { hour: 21, minute: 30, decimal: 21.5 },
        end: { hour: 7, minute: 0, decimal: 7 },
        off: false,
        crossesDays: true,
        shiftLength: 9.5,
    },
    F1: {
        start: { hour: 0, minute: 0, decimal: 0 },
        end: { hour: 0, minute: 0, decimal: 0 },
        off: true,
        crossesDays: false,
        shiftLength: 0,
    },
    F2: {
        start: { hour: 0, minute: 0, decimal: 0 },
        end: { hour: 0, minute: 0, decimal: 0 },
        off: true,
        crossesDays: false,
        shiftLength: 0,
    },
    F3: {
        start: { hour: 0, minute: 0, decimal: 0 },
        end: { hour: 0, minute: 0, decimal: 0 },
        off: true,
        crossesDays: false,
        shiftLength: 0,
    },
    D1: {
        start: { hour: 7, minute: 30, decimal: 7.5 },
        end: { hour: 15, minute: 0, decimal: 15 },
        off: false,
        crossesDays: false,
        shiftLength: 7.5,
    },
};
test("timeBetweenShifts", () => {
    const identifier = new ShiftIdentifier(shifts, new TurnusLintLogger());
    expect(identifier.timeBetweenShifts("A", "D")).toBe(9);
    expect(identifier.timeBetweenShifts("D", "D")).toBe(16);
    expect(identifier.timeBetweenShifts("N", "A")).toBe(7.5);
    expect(identifier.timeBetweenShifts("N", "D")).toBe(0);
    expect(identifier.timeBetweenShifts("D", "A")).toBe(23.5);
    expect(identifier.timeBetweenShifts("A", "D09")).toBe(11);
});

test("identify", () => {
    const identifier = new ShiftIdentifier(shifts, new TurnusLintLogger());
    expect(identifier.identify("D")).toBe("D");
    expect(identifier.identify("D1")).toBe("D");
    expect(identifier.identify("A")).toBe("A");
    expect(identifier.identify("A1")).toBe("A");
    expect(identifier.identify("N")).toBe("N");
    expect(identifier.identify("F1")).toBe("F");
    expect(identifier.identify("F2")).toBe("F");
    expect(identifier.identify("F3")).toBe("F");
    expect(identifier.identify("HELLO WORLD")).toBe("UNKNOWN");
    expect(identifier.identify("")).toBe("F");
    // $FlowExpectedError[incompatible-call]
    expect(identifier.identify(undefined)).toBe("UNKNOWN");
});
test("_getShiftFrom", () => {
    const identifier = new ShiftIdentifier(shifts, new TurnusLintLogger());
    expect(identifier._getShiftFrom("A")).toBe("A");
    expect(identifier._getShiftFrom({ obj: "A" })).toBe("A");
});
test("isA", () => {
    const identifier = new ShiftIdentifier(shifts, new TurnusLintLogger());
    expect(identifier.isA("A")).toBe(true);
    expect(identifier.isA("D")).toBe(false);
    expect(identifier.isA("N")).toBe(false);
    expect(identifier.isA("F1")).toBe(false);
});
test("isD", () => {
    const identifier = new ShiftIdentifier(shifts, new TurnusLintLogger());
    expect(identifier.isD("D")).toBe(true);
    expect(identifier.isD("A")).toBe(false);
    expect(identifier.isD("N")).toBe(false);
    expect(identifier.isD("F1")).toBe(false);
});
test("isN", () => {
    const identifier = new ShiftIdentifier(shifts, new TurnusLintLogger());
    expect(identifier.isN("N")).toBe(true);
    expect(identifier.isN("A")).toBe(false);
    expect(identifier.isN("D")).toBe(false);
    expect(identifier.isN("F1")).toBe(false);
});
test("isF", () => {
    const identifier = new ShiftIdentifier(shifts, new TurnusLintLogger());
    expect(identifier.isF("F1")).toBe(true);
    expect(identifier.isF("A")).toBe(false);
    expect(identifier.isF("D")).toBe(false);
    expect(identifier.isF("N")).toBe(false);
});
test("isAD", () => {
    const identifier = new ShiftIdentifier(shifts, new TurnusLintLogger());
    expect(identifier.isAD("A", "D")).toBe(true);
    expect(identifier.isAD("A", "D09")).toBe(false);
    expect(identifier.isAD("N", "D")).toBe(false);
    expect(identifier.isAD("D", "A")).toBe(false);
    expect(identifier.isAD("F1", "D")).toBe(false);
});

/*
 * @jest-environment jsdom
 */
/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* global process */
import fs from "fs";
import GatParser from "../src/GatParser";
import * as pdfjs from "pdfjs-dist";
import pdfjsWorker from "pdfjs-dist/build/pdf.worker.entry.js";
import glob from "glob";
import path from "path";
import { TurnusLintLogger } from "../src/logger";

/*
 * @flow
 */
if (!fs.existsSync("./tests/data/e-full.json")) {
    // eslint-disable-next-line jest/no-disabled-tests
    test.skip("Private test data is not checked out", () => {});
} else {
    test("Parses e-full.pdf correctly", async () => {
        window.console.log = () => {};
        pdfjs.GlobalWorkerOptions.workerSrc = pdfjsWorker;
        const logger = new TurnusLintLogger();
        const parser = new GatParser(logger);
        const pdf = fs.readFileSync("./tests/data/e-full.pdf");
        await parser.load(pdf, () => {});
        const result = parser.getData();
        // The result contains an 'undefined' value. We need it to be null,
        // since JSON.stringify has converted the undefined to null
        for (const convertPerson of Object.keys(result.people)) {
            if (result.people[convertPerson][0] === undefined) {
                result.people[convertPerson][0] = null;
            }
        }
        expect(result).toStrictEqual(
            JSON.parse(fs.readFileSync("./tests/data/e-full.json").toString()),
        );
    });
}
const entries = glob.sync("./private/*pdf");
const bootstrapMode = process.env.TL_GATPARSER_BOOTSTRAP_MODE === "1";
const logger = window.console.log;
for (const entry of entries) {
    const json = entry.replace(/pdf$/, "json");
    let bootstrap = bootstrapMode;
    if (!fs.existsSync(json) && bootstrapMode === false) {
        bootstrap = true;
        logger("Bootstrapping " + entry + ": no accompanying JSON file");
    }
    jest.setTimeout(30000);
    test("Parses " + path.basename(entry) + " correctly", async () => {
        window.console.log = () => {};
        pdfjs.GlobalWorkerOptions.workerSrc = pdfjsWorker;
        const logger = new TurnusLintLogger();
        const parser = new GatParser(logger);
        const pdf = fs.readFileSync(entry);
        await parser.load(pdf, () => {});
        const result = parser.getData();
        // The result contains an 'undefined' value. We need it to be null,
        // since JSON.stringify has converted the undefined to null
        for (const convertPerson of Object.keys(result.people)) {
            if (result.people[convertPerson][0] === undefined) {
                result.people[convertPerson][0] = null;
            }
        }
        if (bootstrap) {
            fs.writeFileSync(json, JSON.stringify(result, null, 1));
        } else {
            // eslint-disable-next-line
            expect(result).toStrictEqual(
                JSON.parse(fs.readFileSync(json).toString()),
            );
        }
    });
}

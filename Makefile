SHELL:=/bin/bash
BUILD_TARGET=default
DEV_SERVER_PORT?=8181
YARN=$(shell if which yarnpkg &>/dev/null; then echo yarnpkg; else echo yarn;fi)
MINIFY=$(shell if which gominify &>/dev/null; then echo "gominify";else echo "minify";fi)

# Builds a complete, distribution ready version with webpack
distrib: production compress ccp
devdistrib: BUILD_TARGET=branch
devdistrib: production compress ccp
# Builds a production version of TurnusLint with webpack
production: clean
	NODE_ENV=production BUILD_TARGET=$(BUILD_TARGET) $(YARN) webpack --mode production
# Builds a staging version of TurnusLint with webpack
development:
	NODE_ENV=development $(YARN) webpack --mode development
# Starts a development server
devserver: clean
	NODE_ENV=development $(YARN) webpack serve --port $(DEV_SERVER_PORT) --mode development --no-client-overlay-warnings --allowed-hosts all
distPKG: DIST_BRANCH=$(shell git branch --show-current)
distPKG: DIST_REV=$(shell git rev-parse --short=6 HEAD)
distPKG: production ccp
	rm -rf turnuslint-$(DIST_BRANCH)-$(DIST_REV)
	rm -f dist/_headers
	mv dist/ turnuslint-$(DIST_BRANCH)-$(DIST_REV)
	tar -zcf turnuslint-$(DIST_BRANCH)-$(DIST_REV).tar.gz turnuslint-$(DIST_BRANCH)-$(DIST_REV)
	zip -q -l -9 -r turnuslint-$(DIST_BRANCH)-$(DIST_REV).zip turnuslint-$(DIST_BRANCH)-$(DIST_REV)/
	gpg --sign --detach-sign turnuslint-$(DIST_BRANCH)-$(DIST_REV).tar.gz
	gpg --sign --detach-sign turnuslint-$(DIST_BRANCH)-$(DIST_REV).zip
	rm -rf turnuslint-$(DIST_BRANCH)-$(DIST_REV)
ccp:
	rm -f dist/ccp.tar.gz
	tar -zcf dist/turnuslint-ccp.tar.gz COPYING Makefile *.md babel.config.json package.json src static tests tools webpack.config.js yarn.lock .eslintrc.yml .flowconfig .gitignore .gitlab-ci.yml .prettierignore .prettierrc
server: devserver
# Compresses our built files
compress:
	for ftype in html css; do\
		for file in $$(find dist \( -name "*.$$ftype" \)); do \
			mv "$$file" "$$file.tmp";\
			cat "$$file.tmp"|$(MINIFY) --html-keep-document-tags --type "$$ftype" -o "$$file";\
			rm -f "$$file.tmp";\
		done; \
	done
	find dist \( -name '*.html' -o -name '*.ics' -o -name '*.json' -o -name '*.css' -o -name '*.js' -o -name '*.txt' -o -name '*.xml' -o -name '*.map' \) -print0 | xargs -0 gzip -f -9 -k
	find dist \( -name '*.html' -o -name '*.ics' -o -name '*.json' -o -name '*.css' -o -name '*.js' -o -name '*.txt' -o -name '*.xml' -o -name '*.map' \) -print0 | xargs -0 brotli -k
# Run flow
flow:
	[ -d flow-typed ] || $(YARN) flow-typed install
	$(YARN) flow check --include-warnings
eslint:
	$(YARN) eslint src tests
jest:
	$(YARN) jest
test: flow eslint jest
gatParserTest:
	$(YARN) jest --color GatParser 2>&1 | tee GatParser-test.log
# Cleans up the tree
clean: DIST_BRANCH=$(shell git branch --show-current)
clean:
	rm -f *.tar.gz *.tar.gz.sig *.zip *.zip.sig
	rm -rf dist turnuslint-$(DIST_BRANCH)-*

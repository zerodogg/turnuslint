# TurnusLint

TurnusLint is a tool for going through shift plans made in the tool GAT. As its
usefulness is limited to Norway, the application and the rest of the README is
in Norwegian.

TurnusLint er eit program som hjelper tillitsvalde med å sjekke turnusplanar
laga i GAT.  Programmet tolker og analyserer PDFar eksportert frå GAT. Den er
laga for å analysere årsturnus (kalenderplan). Håpet er at TurnusLint kan gjere
tillitsvalde sitt arbeid med å gå gjennom turnusplanar litt enklare.

TurnusLint sjekker:

- Talet på A/D (dvs. vakter med lite tid mellom kvarandre)
- Spreiinga til A/D vakter
- Talet på nattevakter på rad (maks=3 for personar i 3-delt, 5 for nattevakter)
- Talet på vakter på rad (maks=5)
- At ein ikkje har D døgnet etter nattevakt
- At ein har god døgnrytme før nattevakt (dvs. anten fri eller A dagen før ein
    skal gå på natt)
- At dagar som skal ha F3 har F3 (helligdagar ein ikkje jobbar som fell utanfor
    helg)
- Plasseringa av F1 før arbeidshelg (nærast mogleg arbeidshelga)
- Plasseringa av fri etter arbeidshelg (nærast mogleg arbeidshelga)
- At arbeidshelger følg rytmen (dvs. to frihelger mellom kvar arbeidshelg)
- At ein ikkje har vakt same dag som ein går av nattevakt

I tillegg til dei overnemnte sjekkane, vil TurnusLint òg varsle om potensielle
problem, som kanskje ikkje er like alvorlege som dei overnemnte, men som ein
gjerne bør sjå på:

- Vakter som står åleine (dvs. fri før og etter ei enkelt vakt)
- Om kombinasjonen A/D og N er i nærleiken av kvarandre
- Veker som berre har ei enkelt vakt

Til slutt vil TurnusLint òg gi ein del nyttig statistikk:

- Talet på forskjellege vakter, t.d. talet på nattevakter, dagvakter,
    aftenvakter
- Talet på heile veker med samanhengjande fri (typisk ferie)
- Talet på, og kva for nokre, helligdagar som fell utanfor helg det er vakter
    på
- Kva andre merkedagar det er vakter på (t.d. julaftan, nyttårsaftan)

## LISENS

Dette programmet er ikkje assosiert med GAT, MinGAT eller Visma.

Opphavsretten til TurnusLint eies av Eskild Hustvedt - 2019-2020.

Dette programmet er fri programvare: du kan redistribuere og/eller endre dette
programmet under vilkåra i [GNU Affero General Public
License](https://www.gnu.org/licenses/agpl.html), som publisert av [Free
Software Foundation](https://www.fsf.org/), anten [versjon
3](https://www.gnu.org/licenses/agpl-3.0.html) av lisensen, eller (om du vil)
ein seinare versjon.

Programmet vart distribuert i håp om at det er nyttig, men UTAN NOKON FORM FOR
GARANTI; ikkje ein gang ein implisitt garanti om SALGBARHET eller EGNETHET TIL
EIT SPESIELT FORMÅL. Sjå [GNU Affero General Public
License](https://www.gnu.org/licenses/agpl.html) for meir informasjon.

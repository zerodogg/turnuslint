/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
/* eslint-disable no-console */
/**
 * The various log levels.
 */
export type LogLevels = "DEBUG" | "NOTICE" | "WARNING" | "ERROR" | "CRITICAL";
declare var TURNUSLINT_DEBUG: boolean;
declare var BUILD_DATE: string;
declare var GIT_REVISION: string;
declare var GIT_REVISION_FULL: string;
/**
 * Codes specifying various messages. Used so that the UI can display
 * information about errors and critical messages.
 */
type LogMessageCode =
    | "GENERAL"
    | "FILECHECKER_NO_VALID_SCHEDULES"
    | "FILECHECKER_LOAD_FAILED";
/**
 * A single log entry.
 */
export type LogEntry = {|
    time: Date,
    from: string,
    message: string,
    level: LogLevels,
    code: LogMessageCode,
    extraInfo?: string,
    stack?: string,
|};
/**
 * A list of log entries in each LogLevel.
 */
type entries = {|
    [LogLevels]: Array<LogEntry>,
|};
/**
 * The options that can be provided to add().
 */
// eslint-disable-next-line flowtype/require-exact-type
type LoggerEntryOptions = {
    stack?: boolean,
    extraInfo?: string,
    code?: LogMessageCode,
};
/**
 * Logging object.
 */
class TurnusLintLogger {
    _entries: entries = {
        NOTICE: [],
        WARNING: [],
        ERROR: [],
        CRITICAL: [],
        DEBUG: [],
    };
    _allEntries: Array<LogEntry> = [];
    _levelOrder: Array<LogLevels> = [
        "DEBUG",
        "NOTICE",
        "WARNING",
        "ERROR",
        "CRITICAL",
    ];
    _listeners: Array<(LogEntry) => void> = [];

    constructor() {
        this.outputMessage(
            this.notice(
                "TurnusLintLogger",
                `TurnusLint ${GIT_REVISION} (${GIT_REVISION_FULL}), built on ${BUILD_DATE} initialized`,
            ),
        );
        if (TURNUSLINT_DEBUG) {
            window.turnusLintLogger = this;
        }
    }

    /**
     * Add a single message to the log.
     */
    add(
        level: LogLevels,
        sourceComponentName: string,
        message: string,
        options: LoggerEntryOptions = {},
    ): LogEntry {
        const entry: LogEntry = {
            time: new Date(),
            from: sourceComponentName,
            code: "GENERAL",
            message,
            level,
        };
        if (TURNUSLINT_DEBUG && options.stack) {
            entry.stack = new Error().stack;
        }
        if (options.extraInfo !== undefined && options.extraInfo !== null) {
            entry.extraInfo = options.extraInfo;
        }
        if (options.code !== undefined && options.code !== null) {
            entry.code = options.code;
        }
        this._entries[level].push(entry);
        this._allEntries.push(entry);
        if (
            TURNUSLINT_DEBUG === true &&
            (level === "WARNING" || level === "ERROR" || level === "CRITICAL")
        ) {
            this.outputMessage(entry);
        }
        for (const cb of this._listeners) {
            try {
                cb(entry);
            } catch (_) {} /* eslint-disable-line no-empty */
        }
        return entry;
    }

    /**
     * Add a debugging message to the log. This is a no-op in production.
     */
    debug(
        identifier: string,
        message: string,
        options: LoggerEntryOptions = {},
    ): LogEntry | null {
        // This is a no-op in production
        if (TURNUSLINT_DEBUG) {
            return this.add("DEBUG", identifier, message, options);
        }
        return null;
    }

    /**
     * Add a notice message to the log.
     */
    notice(
        identifier: string,
        message: string,
        options: LoggerEntryOptions = {},
    ): LogEntry {
        return this.add("NOTICE", identifier, message, options);
    }

    /**
     * Add a warning message to the log.
     */
    warning(
        identifier: string,
        message: string,
        options: LoggerEntryOptions = {},
    ): LogEntry {
        return this.add("WARNING", identifier, message, options);
    }

    /**
     * Add a error message to the log.
     */
    error(
        identifier: string,
        message: string,
        options: LoggerEntryOptions = {},
    ): LogEntry {
        return this.add("ERROR", identifier, message, options);
    }

    /**
     * Add a critical message to the log.
     */
    critical(
        identifier: string,
        message: string,
        options: LoggerEntryOptions = {},
    ): LogEntry {
        return this.add("CRITICAL", identifier, message, options);
    }

    /**
     * Get all messages.
     */
    getAll(): Array<LogEntry> {
        return this._allEntries;
    }

    /**
     * Outputs a single message to the console. Using console groups where
     * needed.
     */
    outputMessage(entry: LogEntry) {
        const message = this.formatMessage(entry);
        if (
            (entry.stack !== undefined && entry.stack !== null) ||
            (entry.extraInfo !== undefined && entry.extraInfo !== null)
        ) {
            console.groupCollapsed(message);
            if (typeof entry.stack === "string") {
                console.log(entry.stack);
            }
            if (typeof entry.extraInfo === "string") {
                console.log(entry.extraInfo);
            }
            console.groupEnd();
        } else {
            console.log(message);
        }
    }

    /**
     * Returns a formatted message with a timestamp.
     */
    formatMessage(entry: LogEntry): string {
        return (
            "[" +
            entry.time.getHours() +
            ":" +
            entry.time.getMinutes() +
            ":" +
            entry.time.getSeconds() +
            "] " +
            entry.from +
            " " +
            entry.level.toLowerCase() +
            ": " +
            entry.message
        );
    }

    /**
     * Retrieves all entries for a specific log level.
     */
    entries(level: LogLevels): Array<LogEntry> {
        return this._entries[level];
    }

    /**
     * Dumps all messages to the console. This is a no-op in production.
     */
    dumpToConsole() {
        // This is a no-op in production
        if (TURNUSLINT_DEBUG) {
            for (const type of this._levelOrder) {
                if (this._entries[type].length > 0) {
                    console.groupCollapsed(type);
                    for (const entry of this._entries[type]) {
                        this.outputMessage(entry);
                    }
                    console.groupEnd();
                }
            }
        }
    }

    /**
     * Listen for new log messages.
     */
    onNewLogMessage(cb: (LogEntry) => void) {
        this._listeners.push(cb);
    }
}

export { TurnusLintLogger };

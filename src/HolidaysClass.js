/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import type { LimitedDateObjectLiteralNumeric } from "./sharedtypes";
import { TurnusLintLogger } from "./logger.js";

declare var USE_LIMITED_RULESET: boolean;

const StringToSpecialMap: {| [string]: SpecialDays |} = {
    nyttårsdag: "NEW_YEAR_DAY",
    Grunnlovsdagen: "17MAY",
    "Arbeidarane sin dag": "1MAY",
    Skjærtorsdag: "EASTER_THURSDAY",
    Langfredag: "EASTER_FRIDAY",
    påskedag: "EASTER_MONDAY",
    pinsedag: "PINSE_MONDAY",
    himmelferdsdag: "ASCENSIONDAY",
    Julaftan: "YULEEVE",
    "Litle julaftan": "SMALLYULE",
    Påskeaftan: "EASTER_SATURDAY",
    "1. juledag": "1YULEDAY",
    "2. juledag": "2YULEDAY",
    Nyttårsaftan: "NEW_YEARS_EVE",
    Nyttårsdag: "NEW_YEAR_DAY",
};
import holidayList from "./holidays.js";
export type SpecialDays =
    | "NEW_YEARS_EVE"
    | "NEW_YEAR_DAY"
    | "17MAY"
    | "1MAY"
    | "EASTER_THURSDAY"
    | "EASTER_FRIDAY"
    | "EASTER_SATURDAY"
    | "EASTER_MONDAY"
    | "PINSE_MONDAY"
    | "ASCENSIONDAY"
    | "SMALLYULE"
    | "YULEEVE"
    | "1YULEDAY"
    | "2YULEDAY";
export type SpecialDayTypes = "RED" | "NONRED";
export type SingleYearlyHoliday = {|
    sortKey: number,
    year: number,
    month: number,
    day: number,
    name: string,
    identifier: SpecialDays,
|};
export type HolidaysForYear = Array<SingleYearlyHoliday>;
type HolidayHardcoded = {|
    [string]: {|
        [string]: HolidayInternalStructure,
    |},
|};
type HolidayInternalStructure = {|
    dow?: ?string,
    dayType?: SpecialDayTypes,
    name: string,
    disabled?: boolean,
|};
type HolidayInfo = {|
    name: string,
    identifier: SpecialDays,
    dayType: SpecialDayTypes,
    dow: number,
|};
type HolidayCache = {|
    [string]: HolidayInfo,
|};
type HolidayIDToDate = {|
    [SpecialDays]: LimitedDateObjectLiteralNumeric,
|};
type Year = number;
type Month = number;
type Day = number;
type HolidayWithDate = {|
    ...HolidayInfo,
    year: Year,
    month: Month,
    day: Day,
|};
const hardcodedSpecial: HolidayHardcoded = {
    "12": {
        "23": {
            name: "Litle julaftan",
            dayType: "NONRED",
            disabled: USE_LIMITED_RULESET === true,
        },
        "24": {
            name: "Julaftan",
            dayType: "NONRED",
        },
        "25": {
            name: "1. juledag",
            dayType: "RED",
        },
        "26": {
            name: "2. juledag",
            dayType: "RED",
        },
        "31": {
            name: "Nyttårsaftan",
            dayType: "NONRED",
        },
    },
};
function getNorwegianDayOfTheWeek(dow: number): number {
    if (dow === 0) {
        return 7;
    }
    return dow;
}
class Holidays {
    _cache: HolidayCache = {};
    _lastSeenHolidayOfType: HolidayIDToDate = {};
    _logger: TurnusLintLogger;

    constructor(logger: TurnusLintLogger) {
        this._logger = logger;
    }

    isHoliday(year: Year, month: Month, day: Day): boolean {
        return (
            this._getDeepHoliday(year, month, day) !== null ||
            this._getDeepHardcoded(month, day) !== null
        );
    }

    getHolidayOnDate(year: Year, month: Month, day: Day): HolidayInfo | null {
        let dayType: SpecialDayTypes = "RED";
        const ckey = year + "" + month + "" + day;
        const fromCache = this._cache[ckey];
        if (fromCache !== undefined && fromCache !== null) {
            return fromCache;
        }
        const info = this._getAnyHoliday(year, month, day);
        if (info === null || info.disabled === true) {
            return null;
        }
        const identifier = this.getHolidayIdentifierFromString(info.name);
        if (identifier === null) {
            this._logger.warning(
                "Holidays",
                "Failed to find identifier for " + info.name,
            );
            return null;
        }
        if (info.dayType !== undefined) {
            dayType = info.dayType;
        }
        const dt = new Date(year, month - 1, day);
        const generatedInfo: HolidayInfo = {
            name: info.name,
            identifier,
            dayType,
            dow: getNorwegianDayOfTheWeek(dt.getDay()),
        };
        this._lastSeenHolidayOfType[identifier] = {
            year,
            month,
            day,
        };
        this._cache[ckey] = generatedInfo;
        return generatedInfo;
    }

    getHolidayIdentifierFromString(day: string): SpecialDays | null {
        day = day.toLowerCase();
        // List of all possible holidays, sorted by the length of the holiday string
        const possible = Object.keys(StringToSpecialMap).sort(
            (a, b) => b.length - a.length,
        );
        for (const attempt of possible) {
            if (day.indexOf(attempt.toLowerCase()) !== -1) {
                return StringToSpecialMap[attempt];
            }
        }
        return null;
    }

    getDateFor(
        identifier: SpecialDays,
    ): LimitedDateObjectLiteralNumeric | null {
        const date = this._lastSeenHolidayOfType[identifier];
        if (date !== null && date !== undefined) {
            return date;
        }
        return null;
    }

    getHolidayInfoFor(identifier: SpecialDays): HolidayWithDate | null {
        const date = this._lastSeenHolidayOfType[identifier];
        if (date === null || date === undefined) {
            return null;
        }
        const obj = this.getHolidayOnDate(date.year, date.month, date.day);
        if (obj === null) {
            return null;
        }
        return {
            ...obj,
            ...date,
        };
    }

    _getAnyHoliday(
        year: Year,
        month: Month,
        day: Day,
    ): HolidayInternalStructure | null {
        const holiday = this._getDeepHoliday(year, month, day);
        if (holiday !== null) {
            return holiday;
        }
        return this._getDeepHardcoded(month, day);
    }

    _getDeepHoliday(
        year: Year,
        month: Month,
        day: Day,
    ): HolidayInternalStructure | null {
        const strYear = year + "";
        const strMonth = month + "";
        const strDay = day + "";
        if (
            holidayList[strYear] !== undefined &&
            holidayList[strYear][strMonth] !== undefined &&
            holidayList[strYear][strMonth][strDay] !== undefined
        ) {
            return holidayList[strYear][strMonth][strDay];
        }
        return null;
    }
    _getDeepHardcoded(month: Month, day: Day): HolidayInternalStructure | null {
        const strMonth = month + "";
        const strDay = day + "";
        if (
            hardcodedSpecial[strMonth] !== undefined &&
            hardcodedSpecial[strMonth][strDay] !== undefined
        ) {
            return hardcodedSpecial[strMonth][strDay];
        }
        return null;
    }

    getHolidayDatesForYear(year: number): HolidaysForYear {
        if (holidayList[year] === null || holidayList[year] === undefined) {
            return [];
        }
        const yearList = {
            ...holidayList[year],
            ...hardcodedSpecial,
        };
        const result: HolidaysForYear = [];
        for (const month of Object.keys(yearList)) {
            if (typeof yearList[month] === "object") {
                for (const day of Object.keys(yearList[month])) {
                    const identifier = this.getHolidayIdentifierFromString(
                        yearList[month][day].name,
                    );
                    if (identifier === null) {
                        this._logger.warning(
                            "Holidays",
                            "Invalid identifier for " + month + " " + day,
                        );
                        continue;
                    }
                    const sortKey = parseInt(
                        year + month.padStart(2, "0") + day.padStart(2, "0"),
                    );
                    result.push({
                        sortKey,
                        year: parseInt(year),
                        month: parseInt(month),
                        day: parseInt(day),
                        name: yearList[month][day].name,
                        identifier,
                    });
                }
            }
        }
        return result.sort((a, b) => a.sortKey - b.sortKey);
    }

    yearRange(): {| first: number, last: number |} {
        const possible = Object.keys(holidayList);
        possible.sort((a, b) => parseInt(a) - parseInt(b));
        return {
            first: parseInt(possible[0]),
            last: parseInt(possible[possible.length - 1]),
        };
    }
}

export { Holidays };

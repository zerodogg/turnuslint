/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import * as React from "react";
import {
    UncontrolledTooltip,
    Table,
    Row,
    Col,
    Collapse,
    Tooltip,
} from "reactstrap";

import unreachable from "unreachable";
import { uniqueid } from "./utils.js";
import {
    ExclamationCircle,
    XCircle,
    SlashCircle,
    ChevronDoubleDown,
} from "react-bootstrap-icons";
import type {
    ShiftNotice,
    ShiftNotices,
    StatisticTypes,
    PersonStatistics,
} from "./TurnusLint.js";
import { ShiftAnalysisContainer } from "./TurnusLint.js";
import { GetTooltipStringFor } from "./Help";
import type { MonthCounter, StatisticsEntryMetadataObject } from "./TurnusLint";
/**
 * Where tooltips may be placed with SimpleTooltip.
 */
type TooltipPlacement = "top" | "top-start";
/**
 * Props for SimpleTooltip.
 */
type SimpleTooltipProps = {|
    tooltip: string,
    placement?: TooltipPlacement,
    children: React.Node | string,
|};

/**
 * An element that wraps other content with a tooltip, handling id generation
 * etc. automatically.
 */
class SimpleTooltip extends React.PureComponent<SimpleTooltipProps> {
    _uniqueId: number;
    constructor(props: SimpleTooltipProps) {
        super(props);
        this._uniqueId = uniqueid();
    }

    render(): React.Node {
        const { tooltip } = this.props;
        let { placement } = this.props;
        if (placement === undefined) {
            placement = "top";
        }
        return (
            <span key={"tlstooltip" + this._uniqueId}>
                <span id={"tlstooltip" + this._uniqueId}>
                    {this.props.children}
                </span>
                <UncontrolledTooltip
                    placement={placement}
                    delay={{ show: 500, hide: 250 }}
                    target={"tlstooltip" + this._uniqueId}
                >
                    {tooltip}
                </UncontrolledTooltip>
            </span>
        );
    }
}

/**
 * Renders a single notice line.
 */
class ShiftNoticeRenderer extends React.Component<{|
    entry: ShiftNotice,
    person: ShiftAnalysisContainer,
    important?: boolean,
    hidden?: boolean,
|}> {
    render(): React.Node {
        const { entry, person } = this.props;
        let { calendarWeek } = entry;
        calendarWeek = (calendarWeek + "").padStart(2, "0");
        const { message, tooltipText, week, calendarYear } = entry;
        const messagePrefix = (
            <SimpleTooltip
                tooltip={person.formatter.getDateRangeForWeekAsString(week)}
                key="date-hover-tooltip"
            >
                Veke {calendarWeek}-{calendarYear}:&nbsp;
            </SimpleTooltip>
        );
        const renderedMessage = message;
        const actionButtons = [];
        if (this.props.important === true) {
            actionButtons.push(
                <span
                    key="unmark-important"
                    className="actionButton unimportant mr-2"
                    onClick={() => person.unmarkImportant(entry)}
                >
                    <SlashCircle /> Ikkje viktig
                </span>,
            );
        } else {
            actionButtons.push(
                <span
                    key="mark-important"
                    className="actionButton important mr-2"
                    onClick={() => person.markImportant(entry)}
                >
                    <ExclamationCircle /> Viktig
                </span>,
            );
        }
        if (this.props.hidden === true) {
            actionButtons.push(
                <span
                    key="unhide"
                    className="actionButton hide"
                    onClick={() => person.unmarkHidden(entry)}
                >
                    <SlashCircle /> Ikkje skjul
                </span>,
            );
        } else {
            actionButtons.push(
                <span
                    key="hide"
                    className="actionButton hide"
                    onClick={() => person.markHidden(entry)}
                >
                    <XCircle /> Skjul
                </span>,
            );
        }
        let displayedMessage: React.Node = renderedMessage;
        if (tooltipText !== null) {
            displayedMessage = (
                <SimpleTooltip tooltip={tooltipText} key="main-message-tooltip">
                    {renderedMessage}
                </SimpleTooltip>
            );
        }
        return (
            <Row className="shiftNoticeRow">
                <Col xs={9}>
                    {messagePrefix}
                    {displayedMessage}
                </Col>
                <Col className="text-right">{actionButtons}</Col>
            </Row>
        );
    }
}

/**
 * Renders a collapse if the text prop is provided. Returns a null <span />
 * element if not.
 */
class MaybeCollapse extends React.PureComponent<{|
    isOpen: boolean,
    text?: string | React.Node,
|}> {
    render(): React.Node {
        const { isOpen, text } = this.props;
        if (text !== undefined && text !== null && text !== "") {
            return [
                <div key="arrow" className="float-right">
                    <span
                        className={
                            "show-more-hover " +
                            (isOpen === true ? "d-none" : "")
                        }
                    >
                        vis meir informasjon
                    </span>
                    <CollapseArrow open={isOpen} />
                </div>,
                <Collapse isOpen={isOpen} key="collapse-element">
                    <Row>
                        <Col>{text}</Col>
                    </Row>
                </Collapse>,
            ];
        }
        return <span />;
    }
}

/**
 * Renders a table of month counter information.
 */
class MonthCounterRenderer extends React.PureComponent<{|
    months: MonthCounter,
|}> {
    render(): React.Node {
        const { months } = this.props;
        const rows = [];
        let currentRow = [];
        rows.push(currentRow);
        for (const [month, value] of months) {
            if (currentRow.length >= 4) {
                currentRow = [];
                rows.push(currentRow);
            }
            currentRow.push(
                <td key={"month-" + month}>{month}</td>,
                <td className="value" key={"value" + month}>
                    {value}
                </td>,
            );
        }
        const renderEntries = [];
        for (let rowNo = 0; rowNo < rows.length; rowNo++) {
            renderEntries.push(<tr key={"row" + rowNo}>{rows[rowNo]}</tr>);
        }
        return [
            <div key="header">
                <b>Fordeling</b>
            </div>,
            <Table className="compressed" key="table">
                <tbody>{renderEntries}</tbody>
            </Table>,
        ];
    }
}

/**
 * Props for SummaryItemRenderer.
 */
type SummaryItemRendererProps = {|
    value: number | string,
    secondaryValue?: string | number | null,
    metaObject?: StatisticsEntryMetadataObject | null,
    type: StatisticTypes,
|};
/**
 * State for SummaryItemRenderer.
 */
type SummaryItemRendererState = {|
    uniqueid: number,
    viewMore: boolean,
    tooltipOpen: boolean,
|};
/**
 * Renders a single statistical entry for a person.
 */
class SummaryItemRenderer extends React.PureComponent<
    SummaryItemRendererProps,
    SummaryItemRendererState,
> {
    state: SummaryItemRendererState = {
        uniqueid: uniqueid(),
        viewMore: false,
        tooltipOpen: false,
    };
    render(): React.Node {
        const { value } = this.props;
        const { viewMore } = this.state;
        let { tooltipOpen } = this.state;
        const type: StatisticTypes = this.props.type;
        const { metaObject } = this.props;
        let { secondaryValue } = this.props;
        let tooltip;
        let additionalInformation: string | React.Node = "";
        let title;
        switch (type) {
            case "D_IN_A_ROW":
                title = "Maksimum D på rad:";
                tooltip = GetTooltipStringFor("D_IN_A_ROW");
                break;
            case "A_IN_A_ROW":
                title = "Maksimum A på rad:";
                tooltip = GetTooltipStringFor("A_IN_A_ROW");
                break;
            case "A_BEFORE_F_TIMES":
                title = "Ganger med A før fri:";
                tooltip = GetTooltipStringFor("A_BEFORE_F_TIMES");
                if (typeof secondaryValue === "string") {
                    additionalInformation = secondaryValue;
                }
                break;
            case "STANDALONE_F_TIMES":
                title = "Ganger med enkeltståande fridagar:";
                if (typeof secondaryValue === "string") {
                    additionalInformation = secondaryValue;
                }
                break;
            case "SHIFTS_IN_A_ROW":
                title = "Maksimum vaktdøgn på rad:";
                tooltip = GetTooltipStringFor("SHIFTS_IN_A_ROW");
                if (typeof secondaryValue === "string") {
                    additionalInformation = secondaryValue;
                    tooltip += " Trykk for å sjå kva veker det er.";
                }
                break;
            case "WEEKS_OFF":
                title = "Veker med samanhengjande fri:";
                tooltip = GetTooltipStringFor(type);
                if (typeof secondaryValue === "string") {
                    additionalInformation = "Veke " + secondaryValue;
                    tooltip += " Trykk for å sjå kva veker det er.";
                }
                break;
            case "RED_DAYS":
                if (secondaryValue === null || secondaryValue === undefined) {
                    secondaryValue = 0;
                }
                title = `Helligdagar (${secondaryValue}):`;
                tooltip = GetTooltipStringFor(type);
                break;
            case "SPECIAL_DAYS":
                if (secondaryValue === null || secondaryValue === undefined) {
                    secondaryValue = 0;
                }
                title = `Andre merkedagar (${secondaryValue}):`;
                tooltip = GetTooltipStringFor(type);
                break;
            case "QUICK_RETURNS":
                title = "Tal A/D (mindre enn 11 timar kviletid):";
                if (
                    metaObject !== null &&
                    metaObject !== undefined &&
                    metaObject.type === "MONTH_SPREAD"
                ) {
                    additionalInformation = (
                        <MonthCounterRenderer months={metaObject.months} />
                    );
                }
                break;
            case "QUICK_RETURNS_IN_WEEKEND":
                title = "Tal A/D (mindre enn 11 timar kviletid) i helg:";
                if (
                    metaObject !== null &&
                    metaObject !== undefined &&
                    metaObject.type === "MONTH_SPREAD"
                ) {
                    additionalInformation = (
                        <MonthCounterRenderer months={metaObject.months} />
                    );
                }
                break;
            case "WEEKENDS":
                title = "Arbeidshelger:";
                tooltip = GetTooltipStringFor(type);
                if (typeof secondaryValue === "string") {
                    additionalInformation = "Veke " + secondaryValue;
                    tooltip += " Trykk for å sjå kva veker det er.";
                }
                break;
            case "INFERRED_STAIRCASE_WEEKENDS":
                if (secondaryValue === null || secondaryValue === undefined) {
                    secondaryValue = "????";
                }
                title = `Arbeidshelger totalt i ${secondaryValue}:`;
                tooltip =
                    "Dette er eit estimat gitt vanleg rotasjon, tredje-kvar-helg. Relevant for dei tilsette som har avtale om trappetrinnsmodellen (LO/YS), som får høgare helgetillegg jo fleire helger ein jobber.";
                break;
            case "NIGHTS":
                title = "Nattevakter:";
                if (
                    metaObject !== null &&
                    metaObject !== undefined &&
                    metaObject.type === "MONTH_SPREAD"
                ) {
                    additionalInformation = (
                        <MonthCounterRenderer months={metaObject.months} />
                    );
                }
                break;
            case "LONELY_SHIFTS":
                title = "Enkle vakter:";
                if (
                    metaObject !== null &&
                    metaObject !== undefined &&
                    metaObject.type === "MONTH_SPREAD"
                ) {
                    additionalInformation = (
                        <MonthCounterRenderer months={metaObject.months} />
                    );
                }
                break;
            case "MIN_NIGHTS_IN_A_ROW":
                title = "Minimum netter på rad:";
                tooltip = GetTooltipStringFor("MIN_NIGHTS_IN_A_ROW");
                break;
            case "MAX_NIGHTS_IN_A_ROW":
                title = "Maksimum netter på rad:";
                tooltip = GetTooltipStringFor("MAX_NIGHTS_IN_A_ROW");
                break;
            case "FREE_CODE_SPREAD":
                title = "Fordeling av frikodar:";
                break;
            case "SINGLE_SHIFT_CODE_WEEKS":
                title = "Veker med kun éin vakttype:";
                if (secondaryValue !== null && secondaryValue !== undefined) {
                    additionalInformation = secondaryValue;
                }
                break;
            case "SHIFT_GENERIC_SPREAD":
                title = "Vaktkategori:";
                break;
            case "SHIFT_CODE_SPREAD":
                title = "Fordeling av vaktkodar:";
                break;
            case "WEEKS_WITH_ALL_SHIFTS":
                title = "Veker med alle vakttyper (D/A/N):";
                if (secondaryValue !== null && secondaryValue !== undefined) {
                    additionalInformation = "Veke " + secondaryValue;
                }
                tooltip = GetTooltipStringFor("WEEKS_WITH_ALL_SHIFTS");
                break;
            case "SHORT_WORK_WEEKENDS":
                title = "Korte arbeidshelger (berre laurdag/sundag):";
                break;
            case "LONG_WEEKENDS":
                title = "Lange frihelger (3+ dagar):";
                if (secondaryValue !== null && secondaryValue !== undefined) {
                    additionalInformation = secondaryValue;
                }
                break;
            case "NIGHT_SHIFT_WEEKENDS":
                title = "Nattevaktshelger:";
                break;
            case "TIME_REDUCTION":
                title = "Skal ha omregnet tid:";
                if (secondaryValue !== null && secondaryValue !== undefined) {
                    additionalInformation = "Begrunnelse: " + secondaryValue;
                }
                break;
            case "A_SHIFTS":
            case "D_SHIFTS":
                return "";
            default:
                return unreachable(type);
        }
        let hasAdditionalInformation = false;
        if (
            typeof additionalInformation === "object" ||
            (typeof additionalInformation === "string" &&
                additionalInformation !== "")
        ) {
            hasAdditionalInformation = true;
        }
        if (tooltip === null || tooltip === undefined) {
            tooltipOpen = false;
        }
        return (
            <tr
                className={hasAdditionalInformation ? "hand-cursor" : ""}
                id={"sit-" + this.state.uniqueid}
                onClick={() => this.setState({ viewMore: !viewMore })}
            >
                <td className="summary-title">{title}</td>
                <td>
                    {value}
                    <MaybeCollapse
                        isOpen={viewMore}
                        text={additionalInformation}
                    />
                    <Tooltip
                        target={"sit-" + this.state.uniqueid}
                        delay={{ show: 250, hide: 250 }}
                        placement="top-start"
                        isOpen={tooltipOpen}
                        toggle={() => {
                            this.setState({ tooltipOpen: !tooltipOpen });
                        }}
                    >
                        {tooltip}
                    </Tooltip>
                </td>
            </tr>
        );
    }
}

/** Props for PersonStatisticsRenderer. */
type PersonStatisticsRendererProps = {|
    person: ShiftAnalysisContainer,
|};
/**
 * Renders both statistics sections for a person.
 */
class PersonStatisticsRenderer extends React.PureComponent<PersonStatisticsRendererProps> {
    render(): React.Node {
        const statistics = this.props.person.statistics();
        const into = [];
        const statCopy: PersonStatistics = Object.assign({}, statistics);
        const aboveTheFold: Array<StatisticTypes> = [
            "QUICK_RETURNS",
            "QUICK_RETURNS_IN_WEEKEND",
            "WEEKS_WITH_ALL_SHIFTS",
            "SHIFT_GENERIC_SPREAD",
            "NIGHTS",
            "MIN_NIGHTS_IN_A_ROW",
            "MAX_NIGHTS_IN_A_ROW",
            "SHIFTS_IN_A_ROW",
            "WEEKENDS",
            "INFERRED_STAIRCASE_WEEKENDS",
            "WEEKS_OFF",
            "RED_DAYS",
            "SPECIAL_DAYS",
        ];
        const belowTheFoldInOrder: Array<StatisticTypes> = [
            "D_IN_A_ROW",
            "A_IN_A_ROW",
            "SHIFT_CODE_SPREAD",
            "FREE_CODE_SPREAD",
            "SINGLE_SHIFT_CODE_WEEKS",
        ];
        const ignoredTypes: { [StatisticTypes]: boolean } = {
            D_SHIFTS: true,
            A_SHIFTS: true,
        };
        into.push(
            <b key="summaryHeader" className="typeHeader">
                Oppsummering
            </b>,
        );
        const tableSummary = [];
        for (const entryID of aboveTheFold) {
            if (statCopy[entryID] !== undefined) {
                const { value, secondaryValue, metaObject } = statCopy[entryID];
                tableSummary.push(
                    <SummaryItemRenderer
                        key={entryID}
                        value={value}
                        type={entryID}
                        secondaryValue={secondaryValue}
                        metaObject={metaObject}
                    />,
                );
                delete statCopy[entryID];
            }
        }
        into.push(
            <Table
                hover
                borderless
                size="sm"
                className="no-padding turnuslint-table"
                key="summaryTable"
            >
                <tbody>{tableSummary}</tbody>
            </Table>,
        );
        if (Object.keys(statCopy).length) {
            const details = [];
            for (const entryID of belowTheFoldInOrder) {
                if (statCopy[entryID] !== undefined) {
                    const { value, secondaryValue, metaObject } =
                        statCopy[entryID];
                    details.push(
                        <SummaryItemRenderer
                            key={entryID}
                            value={value}
                            type={entryID}
                            secondaryValue={secondaryValue}
                            metaObject={metaObject}
                        />,
                    );
                    delete statCopy[entryID];
                }
            }
            for (const entryID of Object.keys(statCopy)) {
                if (entryID !== undefined && ignoredTypes[entryID] !== true) {
                    const { value, secondaryValue, metaObject } =
                        statCopy[entryID];
                    details.push(
                        <SummaryItemRenderer
                            key={entryID}
                            value={value}
                            type={entryID}
                            secondaryValue={secondaryValue}
                            metaObject={metaObject}
                        />,
                    );
                }
            }
            into.push(
                <CollapsableReportSection
                    header="Detaljar"
                    collapsedHeader="Vis fleire detaljar"
                    canCollapse={true}
                    defaultCollapseState={true}
                    key="CollapsableReportSection"
                >
                    <Table hover borderless size="sm" className="no-padding">
                        <tbody>{details}</tbody>
                    </Table>
                </CollapsableReportSection>,
            );
        }
        return into;
    }
}

type CollapsableReportSectionProps = {|
    header: string,
    collapsedHeader?: string,
    canCollapse: boolean,
    children: React.Node,
    headerClass?: string,
    defaultCollapseState?: boolean,
|};
type CollapsableReportSectionState = {|
    collapsed: boolean | null,
|};

/**
 * Renders a single "report" section. Optionally as a collapseable element if
 * canCollapse is true, otherwise it still returns a <Collapse> but it will
 * always be open. If canCollapse is true then it defaults to collapsed.
 */
class CollapsableReportSection extends React.PureComponent<
    CollapsableReportSectionProps,
    CollapsableReportSectionState,
> {
    state: CollapsableReportSectionState = { collapsed: null };

    render() {
        const { canCollapse, children, collapsedHeader } = this.props;
        let { header } = this.props;
        const isCollapsed: boolean =
            this.state.collapsed === null
                ? this.props.defaultCollapseState === true
                : this.state.collapsed;
        let headerClass = "typeHeader";
        if (this.props.headerClass !== undefined) {
            headerClass += " " + this.props.headerClass;
        }
        if (canCollapse) {
            headerClass += " collapsableTypeHeader";
            if (isCollapsed) {
                headerClass += " collapsedTypeHeader";
                if (collapsedHeader !== undefined) {
                    header = collapsedHeader;
                } else {
                    header = "Vis " + header.toLowerCase();
                }
            }
        }

        return [
            <div
                key="header"
                className={headerClass}
                onClick={() => this.setState({ collapsed: !isCollapsed })}
            >
                {header}
                <div className={canCollapse ? "float-right" : "d-none"}>
                    <CollapseArrow open={!isCollapsed} />
                </div>
            </div>,
            <Collapse key="collapse" isOpen={!canCollapse || !isCollapsed}>
                {children}
            </Collapse>,
        ];
    }
}

/**
 * Renders a complete "Notes" section. Either important, warnings, notes or
 * hidden. Made collapsable if canCollapse is true (and will then also default
 * to collapsed).
 */
class NotesSectionRenderer extends React.PureComponent<{|
    header: string,
    entries: ShiftNotices,
    person: ShiftAnalysisContainer,
    important?: boolean,
    hidden?: boolean,
    canCollapse?: boolean,
|}> {
    render(): React.Node {
        const { header, entries, person, important, hidden, canCollapse } =
            this.props;
        const entriesRender = [];
        for (let entryID = 0; entryID < entries.length; entryID++) {
            const entry = entries[entryID];
            entriesRender.push(
                <ShiftNoticeRenderer
                    key={entryID}
                    person={person}
                    entry={entry}
                    important={important}
                    hidden={hidden}
                />,
            );
        }
        return (
            <div key="importantList" className="infoSection">
                <CollapsableReportSection
                    header={header + " (" + entries.length + ")"}
                    canCollapse={canCollapse === true}
                    defaultCollapseState={true}
                >
                    {entriesRender}
                </CollapsableReportSection>
            </div>
        );
    }
}

/**
 * Props for PersonAnalysisNotesRenderer.
 */
type PersonAnalysisNotesRendererProps = {|
    person: ShiftAnalysisContainer,
|};
/**
 * Renders all analysis notes for a single person.
 */
class PersonAnalysisNotesRenderer extends React.PureComponent<PersonAnalysisNotesRendererProps> {
    _listenerID: number;
    /**
     * Subscribe to events on mount.
     */
    componentDidMount() {
        this._listenerID = this.props.person.subscribe((_) =>
            this.forceUpdate(),
        );
    }
    /**
     * Unsubscribe from events on mount.
     */
    componentWillUnmount() {
        this.props.person.unsubscribe(this._listenerID);
    }

    render(): React.Node {
        const { person } = this.props;
        const into = [];
        const viewMoreElements = [];
        const warnings: ShiftNotices = person.warnings();
        const important: ShiftNotices = person.important();
        const hidden: ShiftNotices = person.hidden();
        const notes = person.notes();
        if (important.length) {
            into.push(
                <NotesSectionRenderer
                    key="important"
                    header="Viktig"
                    entries={important}
                    person={person}
                    important={true}
                />,
            );
        }
        if (warnings.length) {
            into.push(
                <NotesSectionRenderer
                    key="warnings"
                    header="Advarslar"
                    entries={warnings}
                    person={person}
                />,
            );
        }
        if (notes.length) {
            into.push(
                <NotesSectionRenderer
                    key="notes"
                    header="Merknadar"
                    entries={notes}
                    person={person}
                    canCollapse={true}
                />,
            );
        }
        if (hidden.length) {
            into.push(
                <NotesSectionRenderer
                    key="hidden"
                    header="Skjulte"
                    entries={hidden}
                    person={person}
                    hidden={true}
                    canCollapse={true}
                />,
            );
        }
        if (viewMoreElements.length > 0) {
            into.push(
                <div key="viewMoreSection" className="showMoreWrapper">
                    {viewMoreElements}
                </div>,
            );
        }
        return into;
    }
}

/**
 * A generic, re-usable arrow indicating when a Collapse is open or closed,
 * with animations when transitioning between states (through CSS).
 */
function CollapseArrow(props: {|
    open: boolean,
    className?: string,
|}): React.Node {
    const { open } = props;
    let { className } = props;
    if (className === undefined) {
        className = "arrow-faded";
    }
    return (
        <ChevronDoubleDown
            className={
                className + (open === true ? " arrow-open" : " arrow-closed")
            }
        />
    );
}

/**
 * The header element for people.
 */
class PersonNameHeader extends React.PureComponent<{|
    ignored: boolean,
    name: string,
    onToggle: () => void,
|}> {
    render(): React.Node {
        const { ignored, onToggle } = this.props;
        let { name } = this.props;
        let text = "Skjul person";
        let cName = "h3";
        let textClass = "float-right";
        if (ignored) {
            textClass += " text-muted";
            text = "Vis person";
            cName += " hiddenPerson";
            name += " (skjult)";
        } else {
            textClass += " on-person-hover";
        }
        return (
            <div onClick={onToggle} className="personHeader">
                <span className={cName}>{name}</span>
                <div className={textClass}>
                    {text} <CollapseArrow open={ignored !== true} />
                </div>
            </div>
        );
    }
}

/**
 * Renders a complete report for a single person.
 */
class PersonReportRenderer extends React.Component<{|
    person: ShiftAnalysisContainer,
|}> {
    _listenerID: number;
    /**
     * Subscribe to updates to the person object.
     */
    componentDidMount() {
        this._listenerID = this.props.person.subscribe((_) =>
            this.forceUpdate(),
        );
    }
    /**
     * Unsubscribe from updates when unmounting.
     */
    componentWillUnmount() {
        this.props.person.unsubscribe(this._listenerID);
    }

    render(): React.Node {
        const { person } = this.props;
        const ignored = person.ignored();
        const nightWorker = [];
        if (person.isNightWorker()) {
            nightWorker.push(
                <div key="nightWorker" className="mb-2">
                    <b>Merk:</b> personen er ein nattarbeidar. Går ut frå at
                    vedkommande har særavtale som gir tilgang til eit auka tal
                    med netter på rad.
                </div>,
            );
        }
        return (
            <div className="personEntry">
                <PersonNameHeader
                    name={person.name()}
                    ignored={ignored}
                    onToggle={() => {
                        person.toggleIgnored();
                    }}
                />
                <Collapse isOpen={!ignored}>
                    {nightWorker}
                    <PersonAnalysisNotesRenderer person={person} />
                    <PersonStatisticsRenderer person={person} />
                </Collapse>
            </div>
        );
    }
}

export {
    SimpleTooltip,
    ShiftNoticeRenderer,
    SummaryItemRenderer,
    PersonStatisticsRenderer,
    PersonAnalysisNotesRenderer,
    PersonReportRenderer,
    CollapseArrow,
};
// Test exports
export {
    MaybeCollapse,
    MonthCounterRenderer,
    NotesSectionRenderer,
    PersonNameHeader,
};

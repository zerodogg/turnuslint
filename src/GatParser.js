/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import "regenerator-runtime";
import { ShiftIdentifier } from "./ShiftIdentifier.js";
import { TurnusLintLogger } from "./logger.js";
import { JobRunner } from "./JobRunner.js";
import * as pdfjs from "pdfjs-dist/webpack";
import getISOWeek from "date-fns/getISOWeek";
import addDays from "date-fns/addDays";
import { dayNamer } from "./TurnusLint.js";

declare var TURNUSLINT_DEBUG: boolean;
declare var TURNUSLINT_DEBUG_UNHANDLED_LINES: boolean;

import type {
    ShiftType,
    DateObjectLiteral,
    WeekToDateType,
    WeekToDateMap,
    BootstrappedNote,
} from "./sharedtypes.js";

/**
 * A map of X->Y->text.
 */
type CoordinatesToContentMap = Map<number, Map<number, string>>;

/**
 * An array of schedule entries representing a single work week.
 */
export type SingleScheduleArray = Array<string>;

/**
 * An array of SingleScheduleArray (ie. array of work weeks) representing a
 * full plan.
 */
type ScheduleArray = Array<SingleScheduleArray>;

/**
 * A map of "line index" to text strings.
 */
type TextContentMap = Map<number, Array<string | null>>;

function looksLikeNameDefinition(
    thing: string,
    cache: NameDefinitionCache = {},
) {
    if (cache[thing] === "boolean") {
        return cache[thing];
    }
    const isNotWeekDefinition = () => {
        const lastCharacter = thing.substr(thing.length - 1, 1);
        const firstCharacterAsNumber = Number.parseInt(thing.substr(0, 1));
        const lastCharacterAsNumber = Number.parseInt(lastCharacter);
        return (
            // First off, should not contain the string Arbeidsplanberegninger
            thing.indexOf("Arbeidsplanberegninger") === -1 &&
            // The first character should not be a number
            Number.isNaN(firstCharacterAsNumber) &&
            // The final character should not be a number
            Number.isNaN(lastCharacterAsNumber) &&
            // The final character should be ")"
            lastCharacter === ")"
        );
    };
    if (
        // If the string contains "Kalenderplan" or "Årsarbeidsplan", we
        // blindly assume it's a name definition.
        thing.indexOf("alenderplan") !== -1 ||
        thing.indexOf("rsarbeidsplan") !== -1
    ) {
        return true;
    } else if (
        // If the string contains a comma, that comma is followed by
        // whitespace, and isNotWeekDefinition() returns true for this string,
        // then we assume it's a name definition. This will find most names,
        // but might miss manually named lines that do not contain a comma.
        thing.indexOf(",") !== -1
    ) {
        const commaAt = thing.indexOf(",");
        const whitespaceShouldBeAt = commaAt + 1;
        if (thing.substr(whitespaceShouldBeAt, 1) === " ") {
            return isNotWeekDefinition();
        }
    } else if (
        // This is a kind of catch-all. If the string contains parenthesis,
        // does not start with "Uke" and isNotWeekDefinition() returns true,
        // then we assume it's a name definition.
        thing.indexOf("(") !== -1 &&
        thing.indexOf(")") !== -1 &&
        thing.indexOf("Uke") !== 0
    ) {
        return isNotWeekDefinition();
    }
    return false;
}

/**
 * An object of people to ScheduleArrays.
 */
export type ScheduleList = {|
    [string]: ScheduleArray,
|};

/**
 * A map of shift names to ShiftTypes.
 */
type ShiftTypeMap = {|
    [string]: ShiftType,
|};

/**
 * A map of people to WeekToDateType entries.
 */
type PersonToWeekMap = {|
    [string]: WeekToDateType,
|};
/**
 * A type alias to make it clearer when we want the name of a person.
 */
type PersonName = string;
/**
 * A map of real names to their hashed equivalents.
 */
export type NameToHashMap = {|
    [PersonName]: string,
|};

/**
 * The final data that we provide to callers.
 */
export type GatData = {|
    people: ScheduleList,
    shifts: ShiftTypeMap,
    weekToDate: PersonToWeekMap,
    notes: BootstrappedNotes,
    nameToHash: NameToHashMap,
|};

/**
 * The callback type that gets called when we have progress to report.
 */
type ProgressCallback = (number) => void;

/**
 * This actually reads the PDF, and parses that into datastructures that
 * GatParser uses to construct the final dataset.
 */
class LowLevelGatParser {
    _logger: TurnusLintLogger;

    constructor(logger: TurnusLintLogger) {
        this._logger = logger;
    }

    /**
     * Parse a PDF. Returns a promise that will resolve to the parsed data from
     * said PDF.
     */
    parse(data, progressCB: ProgressCallback) {
        return new Promise((resolve, reject) => {
            // Load the PDF using pdfjs
            pdfjs
                .getDocument({ data: data })
                .promise.then((pdf) => {
                    // Will contain the final text content of the PDF
                    const textLines: Array<string> = [];
                    // Will contain the interpreted shift schedule table
                    const shiftList = [];
                    // Iterator
                    let i = 0;
                    // Stored number of pages
                    const counter = pdf.numPages;
                    /*
                     * The pdf.js API is async, thus each page we construct is constructed
                     * async, and it's thus simpler to use a recursive function instead of
                     * looping directly.
                     *
                     * This function will iterate through each page, parsing them, and once
                     * all pages have been handled, it will resolve the result to the caller
                     */
                    const asyncPageReader = () => {
                        i++;
                        if (i < counter) {
                            this._progressed(i, counter, progressCB);
                            pdf.getPage(i).then((pageData) => {
                                this._parsePDFPage(pageData, i).then(
                                    (rendered) => {
                                        textLines.push(...rendered.textLines);
                                        shiftList.push(...rendered.indexed);
                                        asyncPageReader();
                                    },
                                );
                            });
                        } else {
                            resolve({ textLines, shiftList });
                        }
                    };
                    asyncPageReader();
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    /**
     * Internal parsing method that parses the data from a single PDF page.
     */
    _parsePDFPage(pageData, pageNo: number) {
        /*
         * Gotchas: PDFs are indexed from the bottom left
         */
        return pageData
            .getTextContent({
                normalizeWhitespace: false,
                disableCombineTextItems: false,
            })
            .then((textContent) => {
                let lastY, lastX;
                /*
                 * We're going to build an index of Y coordinates -> text
                 * blocks into this variable
                 */
                const indexMap: CoordinatesToContentMap = new Map();
                const textMap: TextContentMap = new Map();
                /*
                 * Each item is an object, containing the following data structure:
                 *
                 * dir - the rendering direction, ignored
                 * fontName - the internal font name, ignored
                 * height - the height of the box
                 * str - the text content
                 * transform - an array, containing the following elements:
                 *   [0,1,2,3] -> linear transformation numbers, ignored
                 *   [4] -> X coordinate
                 *   [5] -> Y coordinate
                 *
                 * width - the width of the box
                 */
                for (const item of textContent.items) {
                    /* First, parse into the index for use by _parseShiftTable */

                    // Make sure we have an object to write our index to
                    if (indexMap.get(item.transform[5]) === undefined) {
                        indexMap.set(item.transform[5], new Map());
                    }
                    let textArray = textMap.get(item.transform[5]);
                    if (textArray === undefined) {
                        textArray = [];
                        textMap.set(item.transform[5], textArray);
                    }
                    const lineEntry = indexMap.get(item.transform[5]);
                    if (lineEntry === undefined) {
                        throw "lineEntry missing";
                    }
                    // Write our indexed values
                    lineEntry.set(item.transform[4], item.str);

                    /* Then parse into a text string */

                    // If the current Y coordinate is the same as the last one
                    // (or we didn't have a last one), then we assume we're handling
                    // another text box on the same line
                    if (lastY == item.transform[5] || !lastY) {
                        // If there are more than 55 pixels since the last text
                        // entry, we need to add additional spacing to account
                        // for empty cells
                        if (lastX && item.transform[4] - lastX > 55) {
                            let no = Math.ceil(
                                (item.transform[4] - lastX) / 55,
                            );
                            while (no-- > 1) {
                                textArray.push(null);
                            }
                        }
                        // Render the string
                        textArray.push(item.str);
                        // Set this as lastX
                        lastX = Math.ceil(item.transform[4]);
                    } else {
                        // Render the string
                        textArray.push(item.str);
                        // Reset lastX to a known start value for tables
                        lastX = 214;
                    }
                    lastY = item.transform[5];
                }
                // Parse the index
                const indexed = this._parseShiftTable(
                    indexMap,
                    textMap,
                    pageNo,
                );
                const textLines: Array<string> = [];
                for (const line of textMap) {
                    textLines.push("" + this._cleanArray(line[1]).join("\0"));
                }
                return { textLines, indexed };
            });
    }

    _cleanArray(arr: Array<string | null>): Array<string | null> {
        while (arr[0] === "") {
            arr.shift();
        }
        return arr;
    }

    /**
     * This reads a table of text entries and constructs usable datastructures
     * for them, associating rows that don't have a "Uke ..." entry in the
     * first cell, with the nearest "Uke..." entry (by calling
     * _findAssociatedWeekDefinition). This magic is required to be able to
     * handle schedules where a single person has multiple shift lines, because
     * GAT will then produce a vertically centered "Uke ..." cell, which
     * doesn't align with any of the schedule lines, thus by just parsing the
     * text we can't know which schedule week we're handling (and because
     * entire rows can be empty, and thus not show up when we parse them, it's
     * hard to infer which line is associated with which week).
     */
    _parseShiftTable(
        index: CoordinatesToContentMap,
        textMap: TextContentMap,
        pageNo: number,
    ): ScheduleArray {
        // Contains a list of all of the Y coordinates
        const keys: Array<number> = [];
        // This is a modified variant of the index. Where the index has
        // both Y and X keys, this one is simply a mapping of Y -> array of text
        // entries. In this datastructure the cells will have been resolved, so
        // any empty cells that we have detected will be present in the array as
        // an empty string element.
        const partiallyParsedResult = {};
        // This is an index of Y coordinates to "Uke..." entries
        const weekEntryIndexer = {};
        // The final result that we return to the caller, an array of arrays
        const end: ScheduleArray = [];

        /*
         * First we generate our list of keys.
         */
        for (const entry of index.keys()) {
            keys.push(entry);
        }
        // Sort them numerically. And since PDFs are indexed from the bottom,
        // not the top, we sort them in descending order.
        keys.sort((a, b) => {
            return parseInt(b) - parseInt(a);
        });
        /*
         * Build our weekEntryIndexer and parse cells into partiallyParsedResult
         */
        for (const Y of keys) {
            const lineIndex = index.get(Y);
            if (lineIndex === undefined) {
                throw (
                    "Confused, lineIndex is undefined although " +
                    Y +
                    " existed"
                );
            }
            // Magic number, this is where the schedule cells begin,
            // so every 55 pixels from there will have a new cell
            let lastX = 214;
            const CELL_WIDTH = 55;
            // Make sure we have a data structure to write to
            if (!partiallyParsedResult[Y]) {
                partiallyParsedResult[Y] = [];
            }
            // Iterate through each cell
            for (const X of lineIndex.keys()) {
                const content = lineIndex.get(X);
                if (content === undefined) {
                    throw "Got undefined content for " + Y + "/" + X;
                }
                // Ignore cells containing only a single space
                if (content === " ") {
                    continue;
                }
                // If this cell is more than CELL_WIDTH px from the last one, we need
                // to add empty cell data
                if (X - lastX > CELL_WIDTH) {
                    // Add one empty cell for each increment of CELL_WIDTH since
                    // the last known cell
                    let no = Math.ceil((X - lastX) / CELL_WIDTH);
                    while (no-- > 1) {
                        partiallyParsedResult[Y].push("");
                    }
                }
                // Add this cell
                partiallyParsedResult[Y].push(content);
                // If we're further along the X axis than lastX, store
                // this value as lastX
                if (X > lastX) {
                    lastX = X;
                }
                // Index the week entry
                if (content.indexOf("Uke a:") === 0) {
                    weekEntryIndexer[Math.round(Y)] = content;
                }
            }
        }

        /*
         * Parse partiallyParsedResult. Ignoring unknown data, and adding
         * "Uke ..." entries where they are missing
         */
        for (const Y of keys) {
            const line = partiallyParsedResult[Y];
            // Used for regex tests
            const combined = line.join(" ");
            // Include "kalenderplan/årsarbeidsplan" lines, since they define the owner of the
            // following set of data
            if (looksLikeNameDefinition(combined)) {
                end.push([combined]);
            }
            // If this entry doesn't contain a "Uke ..." entry, and it looks like
            // a shift schedule entry (ie. is of length 6-7), then we find the
            // closest "Uke ..." entry, and prefix the line with that
            else if (
                combined.indexOf("Uke a:") !== 0 &&
                line.length >= 6 &&
                line.length <= 7
            ) {
                const closestWeek = this._findAssociatedWeekDefinition(
                    weekEntryIndexer,
                    Y,
                );
                if (closestWeek !== undefined) {
                    line.unshift(closestWeek);
                    const textEntry = textMap.get(Y);
                    if (textEntry !== undefined) {
                        textEntry.unshift(closestWeek);
                    }
                } else {
                    this._logger.warning(
                        "GatParser",
                        "Unable to find closest week to: '" +
                            line.join("|") +
                            "' on page " +
                            pageNo,
                    );
                }
                end.push(line);
            }
            // As long as the first entry is a "Uke ..." entry we process it
            else if (combined.indexOf("Uke a:") === 0) {
                end.push(line);
            }
        }
        return end;
    }

    /**
     * This will find the closest entry in index to Y and return it.
     * Will fail if there are no entries matching that are closer than 1000
     * increments away in either direction.
     */
    _findAssociatedWeekDefinition(index, Y) {
        let pastY = Math.round(Y);
        let futureY = pastY;

        for (let identifier = 1; identifier < 1000; identifier++) {
            pastY--;
            futureY++;
            if (index[pastY]) {
                return index[pastY];
            } else if (index[futureY]) {
                return index[futureY];
            }
        }
        return;
    }

    /**
     * Calls the progression callback with the progress, if needed.
     */
    _progressed(page: number, pages: number, cb: ProgressCallback) {
        if (cb !== null && cb !== undefined) {
            const percentage = (page / pages) * 60;
            cb(percentage);
        }
    }
}

/**
 * A cache of values from looksLikeNameDefinition
 */
type NameDefinitionCache = {|
    [string]: boolean,
|};

/**
 * A map of people to BootstrappedNote lists.
 */
type BootstrappedNotes = {|
    [string]: Array<BootstrappedNote>,
|};

/**
 * A cache for string values.
 */
type StringCache = {|
    [string]: string,
|};
/**
 * A cache for numeric values.
 */
type NumberCache = {|
    [string]: number,
|};
/**
 * The caches used by GatParser.
 */
type GatParserRuntimeCaches = {|
    weekDefinition: NumberCache,
    kalenderplan: StringCache,
    strToDate: {|
        [string]: {|
            [number]: DateObjectLiteral,
        |},
    |},
|};

/**
 * This uses LowLevelGatParser to get some raw datastructures, and then
 * interprets these, handing off directly-usable data to the caller.
 */
class GatParser {
    _schedules: ScheduleList;
    _types: ShiftTypeMap;
    _weekToDate: PersonToWeekMap;
    _identifier: ShiftIdentifier | null;
    _logger: TurnusLintLogger;
    _notes: BootstrappedNotes = {};
    _progressCB: ProgressCallback;
    _nameToHash: NameToHashMap = {};
    _nameDefinitionCache: NameDefinitionCache = {};
    _currentPercentage: number;
    _cache: GatParserRuntimeCaches = {
        weekDefinition: {},
        kalenderplan: {},
        strToDate: {},
    };

    constructor(logger: TurnusLintLogger) {
        if (logger === undefined || logger === null) {
            throw "GatParser requires a logger";
        }
        this._logger = logger;
        this._schedules = {};
        this._types = {};
        this._weekToDate = {};
        this._identifier = null;
    }

    /**
     * Returns the ShiftIdentifier instance.
     */
    identifier(): ShiftIdentifier {
        if (!this._identifier) {
            this._identifier = new ShiftIdentifier(this._types, this._logger);
        }
        return this._identifier;
    }

    /**
     * Queues a JobRunner job that deduplicates shifts.
     */
    queueShiftDeduplication(
        queueInto: JobRunner,
        shiftList: ScheduleArray,
        targetShiftList: ScheduleArray,
    ) {
        const weekToShifts = {};
        const totalCount = shiftList.length * 2;
        let currentCount = 0;
        let owner;
        let indexOwner;
        let entryNo = 0;
        // First, index the weeks
        queueInto.queueJob(() => {
            const entry = shiftList[entryNo];
            // "kalenderplan" or "årsarbeidsplan"
            if (this._looksLikeNameDefinitionCached(entry[0])) {
                indexOwner = this._parseKalenderplan(entry[0]);
            }
            if (weekToShifts[indexOwner] === undefined) {
                weekToShifts[indexOwner] = {};
            }
            if (entry[0].indexOf("Uke") !== -1) {
                const entryCopy = entry.slice();
                const week = entryCopy.shift();
                if (indexOwner !== undefined && indexOwner !== null) {
                    if (weekToShifts[indexOwner][week] === undefined) {
                        weekToShifts[indexOwner][week] = [];
                    }
                    weekToShifts[indexOwner][week].push(entryCopy);
                } else {
                    this._logger.critical(
                        "GatParser",
                        "Fant linje utan tilhøyrane eigar: " +
                            entry.join("|") +
                            ". Dette er truleg ein feil i TurnusLint. Hoppar over denne linja. Åtvaringar kan vere feil.",
                    );
                    return false;
                }
            }
            // Internal progression step 2
            this._progressed(++currentCount, totalCount, 2, 70);
            entryNo++;
            return entryNo < shiftList.length;
        });
        // Then perform deduplication
        queueInto.queueJob(() => {
            const entry = shiftList.shift();
            if (!Array.isArray(entry)) {
                return shiftList.length > 0;
            }
            // "kalenderplan" or "årsarbeidsplan"
            if (this._looksLikeNameDefinitionCached(entry[0])) {
                owner = this._parseKalenderplan(entry[0]);
            }
            // Prevents crashing with invalid owners
            if (weekToShifts[owner] === undefined) {
                weekToShifts[owner] = {};
            }
            if (entry[0].indexOf("Uke") === 0) {
                const week = entry.shift();
                let lineResult = [week];
                if (owner === null || owner === undefined) {
                    this._logger.critical(
                        "GatParser",
                        "Fant linje utan tilhøyrane eigar: " +
                            entry.join("|") +
                            ". Dette er truleg ein feil i TurnusLint. Hoppar over denne linja. Åtvaringar kan vere feil.",
                    );
                    return shiftList.length > 0;
                }
                if (!Array.isArray(weekToShifts[owner][week])) {
                    this._logger.critical(
                        "GatParser",
                        "Fant linje utan weekToShifts for " +
                            owner +
                            " veke " +
                            week,
                    );
                    return shiftList.length > 0;
                }
                if (weekToShifts[owner][week].length === 1) {
                    const line = weekToShifts[owner][week].shift();
                    while (line.length > 7) {
                        const popped = line.pop();
                        if (TURNUSLINT_DEBUG) {
                            if (
                                (!/^\s*\d+\s*[,.]*\s*\d*\s*$/.test(popped) &&
                                    popped !== "") ||
                                (this.identifier().isValidShift(popped) &&
                                    popped !== "")
                            ) {
                                this._logger.warning(
                                    "GatParser",
                                    `Reduced line for ${owner} because of length exceeding the expected week length, but the popped column contained non-numeric value "${popped}"`,
                                );
                            }
                        }
                    }
                    lineResult = lineResult.concat(line);
                } else {
                    const identifier = this.identifier();
                    for (const day of [0, 1, 2, 3, 4, 5, 6]) {
                        let winner;
                        for (const line of weekToShifts[owner][week]) {
                            const thisDay = line[day];
                            if (thisDay !== undefined && thisDay !== null) {
                                if (winner === undefined) {
                                    winner = thisDay;
                                } else if (
                                    identifier.isF(winner) &&
                                    !identifier.isF(thisDay)
                                ) {
                                    winner = thisDay;
                                } else if (
                                    winner != thisDay &&
                                    !identifier.isF(winner) &&
                                    !identifier.isF(thisDay)
                                ) {
                                    let ownerNotice = "";
                                    if (owner) {
                                        ownerNotice = "for " + owner + " ";
                                    }
                                    const prettyWeek =
                                        this._getWeekFromDefinitionLine(week);
                                    this.pushNote(
                                        owner,
                                        prettyWeek,
                                        "Fleire forskjellege ikkje-fri vakter på forskjellege linjer " +
                                            thisDay +
                                            " og " +
                                            winner +
                                            " på " +
                                            dayNamer(day + 1),
                                    );
                                    if (TURNUSLINT_DEBUG) {
                                        this._logger.debug(
                                            "GatParser",
                                            "Fleire forskjellege ikkje-fri vakter " +
                                                ownerNotice +
                                                "på forskjellege linjer i veke " +
                                                prettyWeek +
                                                " " +
                                                thisDay +
                                                " og " +
                                                winner +
                                                " på " +
                                                dayNamer(day + 1),
                                        );
                                    }
                                }
                            }
                        }
                        if (winner === undefined) {
                            if (weekToShifts[owner][week].length > 0) {
                                let extraInfo;
                                if (TURNUSLINT_DEBUG) {
                                    extraInfo = JSON.stringify(
                                        weekToShifts[owner][week],
                                    );
                                }
                                this._logger.debug(
                                    "GatParser",
                                    `Ingen vinnar under deduplisering av dag ${day} i veke ${week} for ${owner}.`,
                                    { extraInfo },
                                );
                            }
                            lineResult.push("");
                        } else {
                            lineResult.push(winner);
                        }
                    }
                }
                targetShiftList.push(lineResult);
            } else {
                targetShiftList.push(entry);
            }
            // Internal progression step 3
            this._progressed(++currentCount, totalCount, 3, 72);
            return shiftList.length > 0;
        });
    }

    pushNote(owner: string, week: number, message: string) {
        if (this._notes[owner] === undefined) {
            this._notes[owner] = [];
        }
        // Check for and ignore duplicate messages
        if (this._notes[owner].length > 0) {
            const prev = this._notes[owner][this._notes[owner].length - 1];
            if (
                prev !== undefined &&
                prev.week === week &&
                prev.message === message
            ) {
                return;
            }
        }
        this._notes[owner].push({
            week,
            message,
        });
    }

    /**
     * Loads a PDF. Returns a promise that resolves to this object when we are
     * done parsing, at which point it is safe to call .getData() to retrieve
     * the data.
     */
    load(data: ArrayBuffer, progressCB: ProgressCallback): Promise<GatParser> {
        this._progressCB = progressCB;
        const result = new Promise((resolve, reject) => {
            const lowlevel = new LowLevelGatParser(this._logger);
            lowlevel
                .parse(data, progressCB)
                .then((result) => {
                    // If we hit upon a line that contains nothing but a single
                    // string, we store it in here. We will keep appending
                    // single strings to this until we hit something that's not
                    // a single string, in which case this will be reset to an
                    // empty string (possibly after being used to complete
                    // whatever we were attempting to parse).
                    let previousUnhandledSingleString = "";
                    const PDFentries = result.textLines;
                    let owner = null;
                    // This is used to store the last seen text header in the shift
                    // definition list. We use this to be able to detect shifts
                    // that are actually "free", but which have a set length. In
                    // these cases the parser will check lastTypeHeader to see if
                    // it's the string "Fri", and if it is then the shift is
                    // treated as a free one.
                    let lastTypeHeader = null;

                    const progressTotal = PDFentries.length;
                    let progressEntry = 0;
                    const runner = new JobRunner();
                    runner.runBeforeEventLoop(() => {
                        if (progressCB !== null && progressCB !== undefined) {
                            progressCB(this._currentPercentage);
                        }
                    });
                    // Jobs are run in a loop until they return false, so
                    // .queueJob where first line is line = PDFentries.shift()
                    // is equivalent to for(const line of PDFentries) {}
                    runner.queueJob(() => {
                        const line = PDFentries.shift();
                        if (this._looksLikeNameDefinitionCached(line)) {
                            owner = this._parseKalenderplan(line);
                        } else if (line.indexOf("Uke") !== 0) {
                            const entries = line.split(/\0/);
                            if (
                                // If we have 11 or more entries, then we
                                // should have a look to see if this is a shift
                                // definition line
                                entries.length >= 11
                            ) {
                                if (entries[2].substr(2, 1) === ":") {
                                    // Shift definition line
                                    this._parseType(entries, lastTypeHeader);
                                } else if (
                                    entries[0].substr(2, 1) === ":" &&
                                    previousUnhandledSingleString != ""
                                ) {
                                    // This looks like a multi-line shift
                                    // definition line.
                                    // previousUnhandledSingleString most
                                    // likely contains the multi-line component
                                    // of this shift definition. We thus use
                                    // previousUnhandledSingleString to
                                    // "complete" this shift definition line,
                                    // and then parse the result
                                    entries.unshift("");
                                    entries.unshift(
                                        previousUnhandledSingleString,
                                    );
                                    this._parseType(entries, lastTypeHeader);
                                    previousUnhandledSingleString = "";
                                } else {
                                    if (
                                        TURNUSLINT_DEBUG === true &&
                                        TURNUSLINT_DEBUG_UNHANDLED_LINES ===
                                            true
                                    ) {
                                        if (!expectedError(line)) {
                                            this._logger.debug(
                                                "GatParser",
                                                "Unhandled line for " +
                                                    (owner === null
                                                        ? "(ukjend)"
                                                        : owner) +
                                                    " (which looked like a shift definition): " +
                                                    line,
                                            );
                                        }
                                    }
                                }
                                previousUnhandledSingleString = "";
                            } else if (entries.length < 3) {
                                previousUnhandledSingleString += entries[0];
                                if (
                                    entries[0].indexOf("vakt") > 0 ||
                                    entries[0] === "Fri"
                                ) {
                                    lastTypeHeader = entries[0];
                                }
                            } else {
                                if (TURNUSLINT_DEBUG) {
                                    if (!expectedError(line)) {
                                        this._logger.debug(
                                            "GatParser",
                                            "Unhandled line (belonging to " +
                                                (owner === null
                                                    ? "(unknown)"
                                                    : owner) +
                                                "): " +
                                                line +
                                                " (entries.length=" +
                                                entries.length +
                                                ")",
                                        );
                                    }
                                }
                                previousUnhandledSingleString = "";
                            }
                        } else {
                            // The type section has ended, so reset lastTypeHeader.
                            lastTypeHeader = null;
                        }
                        // Internal progression step 1
                        this._progressed(
                            ++progressEntry,
                            progressTotal,
                            10,
                            60,
                        );
                        return PDFentries.length > 0;
                    });
                    const shiftsDeDuped = [];
                    let currentCountDeduped = 0;
                    let totalCountDeduped;
                    this.queueShiftDeduplication(
                        runner,
                        result.shiftList,
                        shiftsDeDuped,
                    );
                    runner.queueJob(() => {
                        owner = null;
                        totalCountDeduped = shiftsDeDuped.length;
                        return false;
                    });
                    runner.queueJob(() => {
                        const entry = shiftsDeDuped.shift();
                        const joined = entry.join("");
                        if (this._looksLikeNameDefinitionCached(joined)) {
                            owner = this._parseKalenderplan(entry.join(""));
                        } else if (
                            entry[0].indexOf("Uke") === 0 &&
                            owner !== null
                        ) {
                            this._parseWeek(entry, owner);
                        } else if (!/^\s*\d+(,\d+)*\s*$/.test(joined)) {
                            this._logger.error(
                                "GatParser",
                                "Unhandled line (after de-dupe): " + joined,
                            );
                        }
                        // Internal progression step 4
                        this._progressed(
                            ++currentCountDeduped,
                            totalCountDeduped,
                            5,
                            74,
                        );
                        return shiftsDeDuped.length > 0;
                    });
                    runner.queueJob(() => {
                        this.buildChecksums(() => {
                            resolve(this);
                        });
                        return false;
                    });
                    runner.runJobs();
                })
                .catch((err) => {
                    reject(err);
                });
        });
        return result;
    }

    /**
     * Builds checksums of all names in SHA-256 and populates this._nameToHash.
     */
    async buildChecksums(onDone: () => void) {
        for (const name of Object.keys(this._schedules)) {
            if (window.crypto !== undefined && window.crypto !== null) {
                const msgUint8 = new TextEncoder().encode(name);
                const hashBuffer = await window.crypto.subtle.digest(
                    "SHA-256",
                    msgUint8,
                );
                const hashArray = Array.from(new Uint8Array(hashBuffer));
                const hashHex = hashArray
                    .map((b) => b.toString(16).padStart(2, "0"))
                    .join("");
                this._nameToHash[name] = hashHex;
            }
        }
        onDone();
    }

    /**
     * Retrieves the parsed data. Throws if the data is invalid.
     */
    getData(): GatData {
        if (Object.keys(this._types).length === 0) {
            this._logger.critical(
                "GatParser",
                'Fant ingen vaktdefinisjonar. Har du gløymd å hake av for "vaktkoder"?',
            );
            throw "Fant ingen vaktdefinisjonar.";
        }
        return {
            people: this._schedules,
            notes: this._notes,
            shifts: this._types,
            weekToDate: this._weekToDate,
            nameToHash: this._nameToHash,
        };
    }

    /**
     * Parses a shift definition line.
     */
    _parseType(type: Array<string>, lastTypeHeader: string | null) {
        const times = type[2].split(/\s*-\s*/);

        const startTime = times[0].split(/:/);
        const endTime = times[1].split(/:/);

        const start = {
            hour: parseFloat(startTime[0]),
            minute: parseFloat(startTime[1]),
            decimal:
                parseFloat(startTime[0]) +
                parseFloat(parseFloat(startTime[1]) / 60),
        };
        const end = {
            hour: parseFloat(endTime[0]),
            minute: parseFloat(endTime[1]),
            decimal:
                parseFloat(endTime[0]) +
                parseFloat(parseFloat(endTime[1]) / 60),
        };

        let shiftLength = end.decimal - start.decimal;
        let off = false;
        let crossesDays = false;

        if (shiftLength < 0) {
            crossesDays = true;
            shiftLength = end.decimal + 24 - start.decimal;
        }

        if (
            times[0] == times[1] ||
            (lastTypeHeader === "Fri" && !crossesDays)
        ) {
            off = true;
        }

        this._types[type[0]] = {
            start,
            end,
            off,
            crossesDays,
            shiftLength,
        };
    }

    /**
     * Parses a single week.
     */
    _parseWeek(lineCont: Array<string>, name: string) {
        if (name === undefined || name == null) {
            throw (
                "Feil: Lest ein oppføring utan tilknyta namn: " +
                lineCont.join("|")
            );
        }
        if (lineCont.length !== 8) {
            if (lineCont[lineCont.length - 1] != "N") {
                let onlyF = true;
                let hadF1 = false;
                for (const entry of lineCont) {
                    if (
                        entry.indexOf("Uke") !== 0 &&
                        !this.identifier().isF(entry)
                    ) {
                        onlyF = false;
                    }
                    if (entry === "F1") {
                        hadF1 = true;
                    }
                }
                // If this week only had F's or we saw an entry for F1, then
                // assume the week is just weirdly set up, with F1 in the wrong
                // place. TurnusLint will notify about the strange F1 placement.
                if (!onlyF && !hadF1) {
                    this._logger.warning(
                        "GatParser",
                        "Systemadvarsel: vekeoppføring med lengde=" +
                            lineCont.length +
                            " (" +
                            lineCont.join("|") +
                            ") for " +
                            name,
                    );
                }
                while (lineCont.length < 8) {
                    lineCont.push("");
                }
            } else {
                lineCont.push("");
            }
        }
        const definitionLine = lineCont.shift();
        const weekNo = this._getWeekFromDefinitionLine(definitionLine);
        const content = lineCont;
        if (this._schedules[name] == null) {
            this._schedules[name] = [];
        }
        this._schedules[name][weekNo] = content;
        this._parseWeekDate(weekNo, definitionLine, name);
    }

    /**
     * Generic method that retrieves the week definition for a line.
     */
    _getWeekFromDefinitionLine(line: string): number {
        if (this._cache.weekDefinition[line] !== undefined) {
            return this._cache.weekDefinition[line];
        }
        const result = parseInt(line.replace(/.*a:(\d+)\s*.*/, "$1"), 10);
        this._cache.weekDefinition[line] = result;
        return result;
    }

    /**
     * Parses the date for a week, populating _weekToDate.
     */
    _parseWeekDate(week: number, str: string, name: string) {
        if (
            this._weekToDate[name] !== undefined &&
            this._weekToDate[name][week + ""] !== undefined
        ) {
            return;
        }
        let result: WeekToDateMap;
        if (this._cache.strToDate[str] !== undefined) {
            result = this._cache.strToDate[str];
        } else {
            // 14.10.2019
            const splitString = str
                .replace(/.*\((\d+\.\d+\.\d+)\D.*/, "$1")
                .split(".");
            const day = parseInt(splitString[0]);
            const month = parseInt(splitString[1]);
            const year = parseInt(splitString[2]);

            let date = new Date(year, month - 1, day);

            const week = getISOWeek(date);

            result = {};

            for (let i = 1; i < 8; i++) {
                result[i] = ({
                    year: date.getFullYear(),
                    month: date.getMonth() + 1,
                    day: date.getDate(),
                    week,
                }: DateObjectLiteral);

                date = addDays(date, 1);
            }
        }
        if (!this._weekToDate[name]) {
            this._weekToDate[name] = {};
        }
        this._weekToDate[name][week + ""] = result;
    }

    /**
     * Parses a "kalenderplan" definition line.
     */
    _parseKalenderplan(line: string): string {
        if (this._cache.kalenderplan[line] !== undefined) {
            return this._cache.kalenderplan[line];
        }
        const name = line
            .replace(" (test)", "")
            .replace(/^([^(]+).*/, "$1")
            .replace(/^\s+/, "")
            .replace(/\s+$/, "")
            .replace(" , ", ", ")
            .replace(/\0/g, "");
        this._cache.kalenderplan[line] = name;
        return name;
    }

    /**
     * Calls the progression callback.
     */
    _progressed(
        current: number,
        total: number,
        factor: number,
        append: number,
    ) {
        this._currentPercentage = (current / total) * factor + append;
    }

    /**
     * Wrapper around looksLikeNameDefinition that adds a cache.
     */
    _looksLikeNameDefinitionCached(thing: string): boolean {
        if (typeof this._nameDefinitionCache[thing] === "boolean") {
            return this._nameDefinitionCache[thing];
        }
        const result = looksLikeNameDefinition(thing);
        this._nameDefinitionCache[thing] = result;
        return result;
    }
}

/**
 * A function that checks if an error was expected or not. Essentially a no-op
 * when not in both TURNUSLINT_DEBUG mode and TURNUSLINT_DEBUG_UNHANDLED_LINES.
 */
function expectedError(line: string) {
    if (
        TURNUSLINT_DEBUG === true &&
        TURNUSLINT_DEBUG_UNHANDLED_LINES === true
    ) {
        const testLine = line.replace(/\0/g, "");
        const whitelists = [
            /\d+\.\d+\.\d+\s+-\s+\d+:\d+/,
            /\S\S\S+,\s+\S\S\S+/,
            "Lørdager",
            "Søndager",
            "Antall vakter av typen",
            "Kolonne",
            "Timer uke",
            "Timer totalt",
            "Vaktkoder",
            "Røde dager",
            "Kolonne",
            "Søndag",
            "Lørdag",
            "Helligdag",
            "Faktisk stillingsprosent",
            "Stillingsprosent",
            "Nr.Navn",
            "Arbeidsplanberegninger",
            "Beskrivelse",
            "Antall",
            "Side",
            /^F5F5$/,
            /^F3F3$/,
            /\d+\s+av\s+\d+/,
        ];
        for (const check of whitelists) {
            if (
                (typeof check === "string" && testLine.indexOf(check) !== -1) ||
                (typeof check !== "string" && check.test(testLine))
            ) {
                return true;
            }
        }
    }
    return false;
}

export default GatParser;

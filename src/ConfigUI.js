/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import * as React from "react";
import type { NumericConfigValidatorList } from "./Config.js";
declare var USE_LIMITED_RULESET: boolean;

import {
    Row,
    Col,
    Button,
    UncontrolledTooltip,
    FormGroup,
    Label,
    Input,
    Form,
    Modal,
    ModalHeader,
    ModalBody,
} from "reactstrap";
import { TurnusLintConfig } from "./Config.js";
import ModalFooter from "reactstrap/es/ModalFooter";

/**
 * Renders a configuration setting as a numeric input box, with minimum and
 * maximum values.
 */
class ConfigurationInputNumberBox extends React.Component<{|
    min: number,
    max: number,
    onChange: (SyntheticEvent<HTMLInputElement>) => void,
    value: number | string,
    id: string,
    disabled?: boolean,
|}> {
    render() {
        const { id, min, max, onChange, value, disabled } = this.props;
        return (
            <Input
                type="number"
                name="number"
                id={id}
                placeholder={min + " - " + max}
                onChange={onChange}
                value={value}
                disabled={!!disabled}
            />
        );
    }
}

/**
 * Renders a configuration setting with a label, and an
 * ConfigurationInputNumberBox.
 */
class ConfigurationValueNumber extends React.Component<{|
    min: number,
    max: number,
    onChange: (SyntheticEvent<HTMLInputElement>) => void,
    value: number | string,
    id: string,
    label: string,
|}> {
    render() {
        const { label, id, ...inputOptions } = this.props;
        return (
            <FormGroup>
                <Label for={id} className="mr-2">
                    {label}
                </Label>
                <ConfigurationInputNumberBox {...inputOptions} id={id} />
            </FormGroup>
        );
    }
}

/**
 * Renders a configuration setting with a checkbox that enables/disables the
 * setting, a label and an ConfigurationInputNumberBox, that is only active
 * when the setting is on.
 */
class ConfigurationComboCheckboxNumber extends React.Component<{|
    min: number,
    max: number,
    onChangeNumber: (SyntheticEvent<HTMLInputElement>) => void,
    onChangeBoolean: (SyntheticEvent<HTMLInputElement>) => void,
    valueNumber: number | string,
    valueBoolean: boolean,
    id: string,
    label: string,
|}> {
    render() {
        const {
            onChangeBoolean,
            onChangeNumber,
            valueNumber,
            valueBoolean,
            id,
            label,
            max,
            min,
        } = this.props;
        return [
            <FormGroup check key="checkbox">
                <Label check className="mr-2 mb-2">
                    <Input
                        type="checkbox"
                        checked={valueBoolean}
                        onChange={onChangeBoolean}
                    />{" "}
                    {label}
                </Label>
            </FormGroup>,
            <FormGroup key="number">
                <ConfigurationInputNumberBox
                    max={max}
                    min={min}
                    onChange={onChangeNumber}
                    value={valueNumber}
                    id={id}
                    disabled={!valueBoolean}
                />
            </FormGroup>,
        ];
    }
}

/**
 * Renders a complete configuration window.
 */
class ConfigurationSettings extends React.Component<{|
    config: TurnusLintConfig,
    visible?: boolean,
    toggleVisibility: () => void,
|}> {
    _listenerID: number = -1;
    /**
     * Listen for updates to the config.
     */
    componentDidMount() {
        this._listenerID = this.props.config.listen(() => this.forceUpdate());
    }
    /**
     * Unlisten to config changes.
     */
    componentWillUnmount() {
        this.props.config.unlisten(this._listenerID);
    }
    render(): React.Node {
        const { config, toggleVisibility, visible } = this.props;
        const validators: NumericConfigValidatorList = {
            minDaysBetweenAD: config.getNumericValidatorFor("minDaysBetweenAD"),
            maxConsecutiveNights: config.getNumericValidatorFor(
                "maxConsecutiveNights",
            ),
            maxConsecutiveNightsNightWorker: config.getNumericValidatorFor(
                "maxConsecutiveNightsNightWorker",
            ),
            maxConsecutiveAShifts: config.getNumericValidatorFor(
                "maxConsecutiveAShifts",
            ),
        };
        let daysBetweenAD = null;
        if (!USE_LIMITED_RULESET) {
            daysBetweenAD = (
                <Col key="daysBetweenAD">
                    <ConfigurationValueNumber
                        min={validators.minDaysBetweenAD.min}
                        max={validators.minDaysBetweenAD.max}
                        id="minDaysBetweenAD"
                        onChange={(ev) => {
                            config.setNumericOption(
                                "minDaysBetweenAD",
                                ev.currentTarget.value,
                            );
                        }}
                        value={config.getNumericOption("minDaysBetweenAD")}
                        label="Minimum dagar mellom A/D"
                    />
                </Col>
            );
        }
        return (
            <Modal isOpen={visible} toggle={toggleVisibility}>
                <ModalHeader toggle={toggleVisibility}>
                    Innstillingar
                </ModalHeader>
                <ModalBody>
                    <Form className="mt-2">
                        <Row form>
                            <Col>
                                <ConfigurationValueNumber
                                    min={validators.maxConsecutiveNights.min}
                                    max={validators.maxConsecutiveNights.max}
                                    id="maxConsecutiveNights"
                                    onChange={(ev) => {
                                        config.setNumericOption(
                                            "maxConsecutiveNights",
                                            ev.currentTarget.value,
                                        );
                                    }}
                                    value={config.getNumericOption(
                                        "maxConsecutiveNights",
                                    )}
                                    label="Maksimalt tal netter på rad"
                                />
                            </Col>
                            <Col>
                                <ConfigurationValueNumber
                                    min={
                                        validators
                                            .maxConsecutiveNightsNightWorker.min
                                    }
                                    max={
                                        validators
                                            .maxConsecutiveNightsNightWorker.max
                                    }
                                    id="maxConsecutiveNightsNightWorker"
                                    onChange={(ev) => {
                                        config.setNumericOption(
                                            "maxConsecutiveNightsNightWorker",
                                            ev.currentTarget.value,
                                        );
                                    }}
                                    value={config.getNumericOption(
                                        "maxConsecutiveNightsNightWorker",
                                    )}
                                    label="Maksimalt tal netter på rad (for nattarbeidarar)"
                                />
                            </Col>
                        </Row>
                        <Row form>
                            <Col>
                                <ConfigurationComboCheckboxNumber
                                    min={validators.maxConsecutiveAShifts.min}
                                    max={validators.maxConsecutiveAShifts.max}
                                    id="maxConsecutiveAShifts"
                                    onChangeNumber={(ev) => {
                                        config.setNumericOption(
                                            "maxConsecutiveAShifts",
                                            ev.currentTarget.value,
                                        );
                                    }}
                                    valueNumber={config.getNumericOption(
                                        "maxConsecutiveAShifts",
                                    )}
                                    valueBoolean={config.getBooleanOption(
                                        "enableMaxConsecutiveASHifts",
                                    )}
                                    onChangeBoolean={(_) => {
                                        config.setBooleanOption(
                                            "enableMaxConsecutiveASHifts",
                                            !config.getBooleanOption(
                                                "enableMaxConsecutiveASHifts",
                                            ),
                                        );
                                    }}
                                    label="Sjekk maksimalt tal på A-vakter etter kvarandre"
                                />
                            </Col>
                            {daysBetweenAD}
                        </Row>
                        <Row form>
                            <Col>
                                <FormGroup check>
                                    <Label
                                        check
                                        className="mr-2 mb-2"
                                        id="trappetrinnsmodellen"
                                    >
                                        <Input
                                            type="checkbox"
                                            checked={config.getBooleanOption(
                                                "enableInferredYearlyWorkWeekendcount",
                                            )}
                                            onChange={() => {
                                                config.setBooleanOption(
                                                    "enableInferredYearlyWorkWeekendcount",
                                                    !config.getBooleanOption(
                                                        "enableInferredYearlyWorkWeekendcount",
                                                    ),
                                                );
                                            }}
                                        />{" "}
                                        Vis estimat for totalt antall helger for
                                        kalenderåret (til trappetrinnsmodellen)
                                    </Label>
                                    <UncontrolledTooltip target="trappetrinnsmodellen">
                                        Trappetrinnsmodellen inneber at jo
                                        fleire helger ein jobbar i laupet av eit
                                        kalenderår, jo høgare helgetillegg vil
                                        ein få utbetalt. Modellen er avtalt med
                                        LO- og YS-forbunda.
                                    </UncontrolledTooltip>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row form>
                            <Col>
                                <FormGroup check>
                                    <Label
                                        check
                                        className="mr-2 mb-2"
                                        id="enkeltA"
                                    >
                                        <Input
                                            type="checkbox"
                                            checked={config.getBooleanOption(
                                                "enableSingularAChecks",
                                            )}
                                            onChange={() => {
                                                config.setBooleanOption(
                                                    "enableSingularAChecks",
                                                    !config.getBooleanOption(
                                                        "enableSingularAChecks",
                                                    ),
                                                );
                                            }}
                                        />{" "}
                                        Vis advarsel når ein person har ei
                                        enkeltståande A-vakt
                                    </Label>
                                    <UncontrolledTooltip target="enkeltA">
                                        At ein har minst to A-vakter når ein
                                        fyrst jobbar A. bidreg til å redusere
                                        talet på A/D. Merk at dette valet kun
                                        fungerer når vakta før A-vakta er ei
                                        vakt som startar med bokstaven «D»
                                        (dette for å filtrere bort spesielle
                                        vakter, t.d. studiedagar, kurs o.l.). Du
                                        vil heller ikkje få varsel om
                                        enkeltståande A-vakter som er på ein
                                        sundag.
                                    </UncontrolledTooltip>
                                </FormGroup>
                            </Col>
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggleVisibility}>
                        Lukk
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }
}

export { ConfigurationSettings };

/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */

import format from "date-fns/format";
import dateFnsNo from "date-fns/locale/nn";

const uniqueid: () => number = (function () {
    let id = 0;
    return () => {
        return id++;
    };
})();

/**
 * Calls the date-fns format() function with the given `Date` and `fmt`
 * (string) with the proper locale, and returns the result (string)
 */
function dateFnsGetFormat(date: Date, fmt: string): string {
    return format(date, fmt, { locale: dateFnsNo });
}
export { uniqueid, dateFnsGetFormat };

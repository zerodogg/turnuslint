/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
/**
 * The type of the job callbacks.
 */
type JobCallbacks = () => boolean;

/**
 * JobRrunner is a helper class that runs a defined set of "jobs" (callback
 * functions) in a loop until said job returns false. When that happens it
 * moves on to the next queued job, and keeps this up until all jobs have been
 * run. It will every _maxJobsPerIteration queue the next job with setTimeout()
 * so that the main javascript event loop can run, to ie. update UI. It also
 * has a special callback function, queued with .runBeforeEventLoop() that is
 * executed immediately prior to the setTimeout() call. This can be used to
 * queue up the UI changes only when the event loop will actually run (and
 * thus not waste time calling UI update functions when the UI is blocked).
 *
 * The .runJobs() method returns a promise that resolves once all jobs have
 * been run. It's perfectly safe to discard this.
 */
class JobRunner {
    _queue: Array<JobCallbacks> = [];
    _maxJobsPerIteration: number = 50;
    _iter: number = 0;
    _resolveFunction: (JobRunner) => void;
    _beforeEventLoop: () => void;
    _locked: boolean = false;

    /**
     * Queue up a callback that will be run before each setTimeout() call. Use
     * this to refresh the UI.
     */
    runBeforeEventLoop(cb: () => void) {
        if (this._beforeEventLoop !== undefined) {
            throw "Attempt to re-queue a new runBeforeEventLoop function";
        }
        this._beforeEventLoop = cb;
    }

    /**
     * Queue a job. Will be run in the order they are queued, and in a loop
     * until the callback returns false.
     */
    queueJob(job: JobCallbacks): JobRunner {
        this._queue.push(job);
        return this;
    }

    /**
     * Runs all the jobs that have been queud. Returns a promise that resolves
     * to this instance of `JobRunner` when all jobs have been run.
     */
    runJobs(): Promise<JobRunner> {
        if (this._locked === true) {
            throw "Attempt to runJobs() on object that is already running";
        }
        this._locked = true;
        return new Promise((resolve, _) => {
            this._resolveFunction = resolve;
            this._runJobs();
        });
    }

    /**
     * Resolves our promise.
     */
    _resolvePromise(): null {
        this._locked = false;
        this._resolveFunction(this);
        return null;
    }

    /**
     * Runs jobs until either all iterations have been exhausted or
     * _maxJobsPerIteration has been run.
     */
    _runJobs(): ?null {
        if (this._queue.length === 0) {
            return this._resolvePromise();
        }
        while (++this._iter < this._maxJobsPerIteration) {
            if (this._queue.length === 0) {
                return this._resolvePromise();
            }
            this.runSingleJob();
        }
        if (this._queue.length > 0) {
            this.runBackgroundJob();
        } else {
            return this._resolvePromise();
        }
    }

    /**
     * Runs a single job.
     */
    runSingleJob() {
        const job = this._queue[0];
        if (job === undefined) {
            throw "Attempted to .runSingleJob() with an undefined value in the queue";
        }
        const result = job();
        if (result === false) {
            this._queue.shift();
        }
    }

    /**
     * Runs _beforeEventLoop and then queues up a single iteration of the main
     * event loop.
     */
    runBackgroundJob() {
        this._iter = 0;
        if (
            this._beforeEventLoop !== null &&
            this._beforeEventLoop !== undefined
        ) {
            this._beforeEventLoop();
        }
        setTimeout(() => this._runJobs(), 0);
    }
}
export { JobRunner };

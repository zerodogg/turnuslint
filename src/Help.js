/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import * as React from "react";
import {
    Button,
    Card,
    CardHeader,
    Collapse,
    ModalBody,
    ModalFooter,
    Table,
} from "reactstrap";
import CardBody from "reactstrap/es/CardBody";
import Modal from "reactstrap/es/Modal";
import ModalHeader from "reactstrap/es/ModalHeader";
import { CollapseArrow } from "./Renderer";
import { XCircle } from "react-bootstrap-icons";

declare var USE_LIMITED_RULESET: boolean;

/**
 * A list of tooltips/helpful descriptions of words and features.
 */
const toolTips = {
    RED_DAYS: "Helligdagar som fell på måndag-fredag",
    SPECIAL_DAYS: {
        report: "Merkedagar (t.d. julaftan), samt helligdagar som fell på helg",
        description:
            "Spesielle dagar som ikkje er raude (t.d. julaftan og nyttårsaftan), samt helligdagar (raude dagar) som fell på laurdag eller sundag",
    },
    QUICK_RETURNS: {
        report: "Tal på vaktkombinasjonar kor tida mellom to vakter er mindre enn AML sitt krav (utan dispensasjon) på 11 timer",
        description:
            "Vaktkombinasjonar kor tida mellom to vakter er mindre enn AML sitt krav (utan dispensasjon) på 11 timer",
    },
    QUICK_RETURNS_IN_WEEKEND:
        "Tal på vaktkombinasjonar kor tida mellom to vakter er mindre enn 11 timer, og kor minst ein av vaktene ligg på laurdag eller sundag",
    WEEKS_OFF: "Kalenderveker kor ein har fri frå måndag til sundag.",
    SHORT_WORK_WEEKENDS:
        "Arbeidshelger kor ein berre jobber 2 dagar samanhengjande (lau-sun)",
    WEEKENDS: {
        report: "Talet på veker kor vedkommande arbeider laurdag og/eller sundag.",
        description: "Veker kor ein person arbeider laurdag og/eller sundag.",
    },
    LONG_WEEKENDS: "Helger med minst tre dagar samanhengjande fri",
    WEEKS_WITH_ALL_SHIFTS: "Kalenderveker kor ein har både D, A og N",
    WARNINGS: "Potensielle problem med turnusen som du bør ta stilling til",
    NOTES: "Mindre viktige merknadar til turnusen",
    IMPORTANT:
        "Advarslar eller merknadar som du har vald å markere som viktig. TurnusLint vil hugse desse til seinare, sånn at viss du analyserer ein anna versjon av turnusen seinare vil TurnusLint gi beskjed viss det er ting du har markert som viktig men som ikkje har blitt fiksa.",
    MAX_NIGHTS_IN_A_ROW:
        "Det høgste talet på nattevakter etter kvarandre personen har",
    MIN_NIGHTS_IN_A_ROW:
        "Det lågaste talet på nattevakter etter kvarandre personen har",
    SHIFTS_IN_A_ROW: ((USE_LIMITED_RULESET === true
        ? "Det høgste talet på vakter på rad personen har"
        : "Det høgste talet på vaktdøgn etter kvarandre personen har (døgn er rekna frå 00:00-23:59, og ei nattevakt strekk seg då over to døgn)"): string),
    INFERRED_STAIRCASE_WEEKENDS:
        "Eit estimat på kor mange helger ein person vil jobbe i laupet av kalenderåret, gitt at vedkommande held seg til vanleg tredje-kvar-helg rotasjon. Dette er nyttig for tilsette som vart omfatta av «trappetrinnsmodellen», som er avtalt med LO- og YS-forbunda. Modellen inneber at jo fleire helger ein jobber i laupet av eit kalenderår, jo høgare tillegg vil ein få for arbeid på helg.",
    D_IN_A_ROW: "Kor mange D-vakter ein har på rad i turnusen på det meste",
    A_IN_A_ROW: "Kor mange A-vakter ein har på rad i turnusen på det meste",
    A_BEFORE_F_TIMES: "Kor mange ganger ein har ei A-vakt før ein har fri",
};
/**
 * A type corresponding to all the keys in toolTips.
 */
export type PossibleTooltips = $Keys<typeof toolTips>;

/**
 * The types of tooltips there are. There's report (for use in the report) and
 * description (for use in the help window). Both are optional, and a single
 * string value will work for both of them.
 */
type tooltipSource = "report" | "description";

/**
 * Retrieve the tooltip for a given key, optionally the resource-specific one.
 */
function GetTooltipStringFor(
    key: PossibleTooltips,
    source: tooltipSource = "report",
): string {
    const entry = toolTips[key];
    if (entry === null || entry === undefined) {
        throw "GetTooltipStringFor() got an invalid key: " + key;
    }
    if (typeof entry === "object") {
        if (source === "description") {
            return entry.description;
        }
        return entry.report;
    }
    return entry;
}

/**
 * The kinds of headers we accept.
 */
type HeaderThing = "h1" | "h2" | "h3" | "h4" | "h5" | "";
/**
 * Props for LinkIshHeader.
 */
type LinkIshHeaderProps = {|
    children: React.Node,
    headerType?: HeaderThing,
    onClick?: () => void,
    id?: string,
|};
/**
 * A header element that pretends to be a link as well.
 */
class LinkIshHeader extends React.PureComponent<LinkIshHeaderProps> {
    render() {
        const { children, headerType, ...props } = this.props;
        let entryClass = "likeLink";
        if (headerType === null || headerType === undefined) {
            entryClass += " h5";
        } else {
            entryClass += " " + headerType;
        }
        return (
            <div {...props} className={"likeLink " + entryClass}>
                {children}
            </div>
        );
    }
}

/**
 * Help text describing the various words and phrases we use.
 */
function KvaBetyr(_: {||}) {
    // This is a map of "descriptive text string" to PossibleTooltips (which
    // will be requested using `GetTooltipStringFor`).
    const descriptions: Map<string, PossibleTooltips> = new Map([
        ["Helligdagar", "RED_DAYS"],
        ["Merkedagar", "SPECIAL_DAYS"],
        ["A/D, quick return, sein/tidleg", "QUICK_RETURNS"],
        ["Arbeidshelger", "WEEKENDS"],
        ["Korte arbeidshelger", "SHORT_WORK_WEEKENDS"],
        ["Veker med samanhengjande fri", "WEEKS_OFF"],
        ["Lange frihelger", "LONG_WEEKENDS"],
        ["Veker med A+D+N", "WEEKS_WITH_ALL_SHIFTS"],
        ["Advarslar", "WARNINGS"],
        ["Merknadar", "NOTES"],
        ["Markert som viktig", "IMPORTANT"],
        ["Arbeidshelger totalt i …", "INFERRED_STAIRCASE_WEEKENDS"],
        ["Maksimum D på rad", "D_IN_A_ROW"],
        ["Maksimum A på rad", "A_IN_A_ROW"],
        ["Minimum N på rad", "MIN_NIGHTS_IN_A_ROW"],
        ["Maksimum N på rad", "MAX_NIGHTS_IN_A_ROW"],
    ]);
    const tableRows = [];
    for (const [word, descriptionEntry] of descriptions) {
        tableRows.push(
            <tr key={word}>
                <td>{word}</td>
                <td>{GetTooltipStringFor(descriptionEntry, "description")}</td>
            </tr>,
        );
    }
    return (
        <span>
            <Table>
                <thead>
                    <tr>
                        <th>Ord</th>
                        <th>Forklaring</th>
                    </tr>
                </thead>
                <tbody>{tableRows}</tbody>
            </Table>
        </span>
    );
}

/**
 * Help text describing how to export from GAT.
 */
function KorleisEksportere(_: {||}) {
    return (
        <span>
            <b>Eksportering</b>
            <br />
            For å eksportere frå GAT:
            <ol>
                <li>Start GAT</li>
                <li>
                    Vel «Arbeidsplan»-fana:{" "}
                    <img
                        src="nonfree/gat-main-win-tab-bar.png"
                        alt="Arbeidsplan"
                    />
                </li>
                <li>
                    Vel{" "}
                    <img src="nonfree/gat-main-win-open-arbpl.png" alt="Åpne" />{" "}
                    for å åpne arbeidsplanen
                </li>
                <li>
                    Vel «Utskrift»-fana i vindauget som kjem opp:{" "}
                    <img src="nonfree/gat-tab-bar.png" alt="Utskrift" />
                </li>
                <li>
                    Vel{" "}
                    <img
                        src="nonfree/gat-pers-arb-plan-btn.png"
                        alt="Personlig arbeidsplan"
                    />
                </li>
                <li>
                    Vel følgjande innstillingar:
                    <ul>
                        <li>Arkstorleik: A4</li>
                        <li>Orientering: Liggjande</li>
                        <li>Utskriftsformat: Klassisk, vaktkoder</li>
                        <li>Innstillingar: Hak av for «Vaktkoder»</li>
                    </ul>
                    <img src="nonfree/gat-pers-arb-plan-win.png" alt="" />
                </li>
                <li>
                    Hak av for personane du vil analysere (Trykk «merk alle»
                    viss du vil analysere heile planen)
                </li>
                <li>Trykk på «Forhåndsvis»-knappen nedst til venstre</li>
                <li>
                    Eksporter førehandsvisningen som PDF (Fil -&gt; Eksporter
                    dokument -&gt; PDF Fil)
                    <img
                        src="nonfree/gat-pdf-eksport.png"
                        alt="Fil -> Eksporter dokument -> PDF Fil"
                    />
                </li>
                <li>
                    Opne PDFen i TurnusLint ved å trykke på «Opne fil …» i
                    menyen til TurnusLint
                </li>
            </ol>
        </span>
    );
}

/**
 * Help text describing which checks are performed.
 */
function KvaSjekker(_: {||}) {
    const checks = [
        <li key="natt-rad">Talet på nattevakter på rad</li>,
        <li key="vakt-rad">Talet på vakter på rad</li>,
        <li key="døgn-før-natt">
            At ein har god døgnrytme før nattevakt (fri eller A dagen før natt)
        </li>,
        <li key="f3-dagar">
            At dagar som skal ha F3 har F3 (helligdagar ein har fri som ikkje
            ligg i ei helg)
        </li>,
        <li key="plassering-f1">
            Plasseringa av F1 før arbeidshelg (nærmast mogleg arbeidshelga)
        </li>,
        <li key="plassering-fri-arbhelg">
            Plasseringa av fri etter arbeidshelg (nærmast mogleg arbeidshelga)
        </li>,
        <li key="helger-på-rad">Helger på rad</li>,
        <li key="vakter-åleine">Vakter som står åleine (t.d. F-D-F)</li>,
        <li key="ad-n">Om kombinasjonen A/D og N er nær kvarandre</li>,
        <li key="enkelt-vakt">Veker som berre har ei enkelt vakt</li>,
        <li key="helg-rytme">
            Om arbeidshelger følg rytmen med tredje-kvar helg
        </li>,
        <li key="a-rad">(valfritt) A-vakter på rad</li>,
    ];
    if (!USE_LIMITED_RULESET) {
        checks.push(
            <li key="d-etter-n">At ein ikkje har D døgnet etter nattevakt</li>,
            <li key="tid-to-ad">Tid mellom to A/D</li>,
        );
    }
    return (
        <div>
            TurnusLint sjekker mykje. Ikkje alt som TurnusLint vil påpeike er
            nødvendigvis eit problem for alle, så det finst varslar frå
            TurnusLint som ein gjerne må oversjå basert på situasjonen. I dei
            tilfellene kan du velje «Skjul» på meldinga, så slepp du at den kjem
            opp igjen på denne personen.
            <br />
            <br />
            Dette sjekker TurnusLint:
            <ul>{checks}</ul>
            Turnuslint vil òg gi deg ein del nyttig informasjon og statistikk:
            <br />
            <ul>
                <li>Talet på A/D-vaktkombinasjonar</li>
                <li>
                    Talet på A/D-vaktkombinasjonar som ligg inntil ei helg (kor
                    minst ein av vaktene er på ein laurdag eller sundag)
                </li>
                <li>Kva helligdagar ein person jobbar</li>
                <li>
                    Kva merkedagar som ikkje er raude ein person jobbar (t.d.
                    julaftan, nyttårsaftan)
                </li>
                <li>Kor mange A/D som er i kvar månad</li>
                <li>
                    Kor mange netter ein person jobbar, og korleis nettene er
                    fordelt gjennom planen
                </li>
                <li>Talet på helger i laupet av eit år</li>
                <li>Talet på forskjellege vakttypar</li>
                <li>Det maksimale talet på A, D og N på rad i turnusen</li>
                <li>
                    Kor mange periodar med ei veke samanhengjande fri ein person
                    har i turnusen
                </li>
                <li>
                    Kor mange lange frihelger ein har (tre dagar eller meir fri
                    samanhengjande)
                </li>
                <li>
                    Kor mange korte arbeidshelger ein har (kor ein berre jobber
                    laurdag og sundag, og då har fri fredag og måndag)
                </li>
            </ul>
        </div>
    );
}

/**
 * Help text describing how we identify shifts.
 */
function Vakttyper(_: {||}) {
    return (
        <div>
            <b>Vakttyper</b>
            <br />
            <i>
                TurnusLint les vaktkodane i den eksporterte fila frå GAT. I
                tillegg til å analysere desse vil TurnusLint klassifisere vakter
                etter følgjande regler:
            </i>
            <br />
            <b>F*</b> er definert som vakter som har typen «Fri» i GAT
            <br />
            <b>A</b> er definert som vakter som starter etter 12
            <br />
            <b>N</b> er definert som vakter som krysser to døgn
            <br />
            <b>D</b> er definert som alle vakter som ikkje er A, N eller F*
            <br />
            <b>A/D</b> er definert som vaktkombinasjonar kor det er mindre enn
            11 timar mellom A og D.
            <br />
            <br />I tillegg definerer TurnusLint <i>ein nattarbeidar</i> som ein
            person som har mindre enn 7 vakter som ikkje er N.
        </div>
    );
}

/**
 * Help text explaining the table.
 */
function TabellInfoOverload(_: {||}) {
    return (
        <div>
            TurnusLint viser mykje informasjon, men du kan skjule individuelle
            kolonner i tabellen. Hald musepeikaren over den overskriften du vil
            skjule, og trykk på <XCircle />
            -ikonet som kjem opp. Viss du vil ha tilbake noko du har skjult
            trykker du berre på «Vis alle kolonnene igjen» rett over tabellen.
            Merk at det ikkje er mogleg å skjule kolonner i heilagdagstabellen.
        </div>
    );
}

/**
 * The types of help content there are.
 */
type HelpContentType =
    | "vakttyper"
    | "korleisEksportere"
    | "kvaSjekker"
    | "kvaBetyr"
    | "tabellInfoOverload";

/**
 * An element that renders help texts as collapsable elements.
 */
class HelpSection extends React.PureComponent<{|
    type: HelpContentType,
    openType: HelpContentType | null,
    title: string,
    setVisibleSection: (HelpContentType) => void,
    children: React.Node,
    headerType?: HeaderThing,
    outerCard?: boolean,
    visible?: boolean,
|}> {
    render(): React.Node {
        const {
            type,
            openType,
            title,
            setVisibleSection,
            children,
            headerType,
            outerCard,
            visible,
        } = this.props;
        if (visible === false) {
            return <span />;
        }
        const header = (
            <LinkIshHeader
                onClick={() => setVisibleSection(type)}
                headerType={headerType}
            >
                {title}
                <div
                    key="arrow"
                    className={
                        /* Only display the arrow if outerCard is true */
                        "float-right " + (outerCard === true ? "" : "d-none")
                    }
                >
                    <CollapseArrow
                        open={openType === type}
                        className="arrow-solid arrow-small"
                    />
                </div>
            </LinkIshHeader>
        );
        if (outerCard === true) {
            return (
                <Card>
                    <CardHeader>{header}</CardHeader>
                    <Collapse isOpen={openType === type}>
                        <CardBody>{children}</CardBody>
                    </Collapse>
                </Card>
            );
        } else {
            return (
                <span>
                    {header}
                    <Collapse isOpen={openType === type}>
                        <Card>
                            <CardBody>{children}</CardBody>
                        </Card>
                    </Collapse>
                </span>
            );
        }
    }
}

/**
 * Props for HelpContent.
 */
type HelpContentProps = {|
    header?: HeaderThing,
    outerCard?: boolean,
    onlyEssential?: boolean,
|};
/**
 * HelpContent's state.
 */
type HelpContentState = {|
    visible: HelpContentType | null,
|};
/**
 * Renders all of our help as a list of collapsable elements that all default
 * to closed.
 */
class HelpContent extends React.Component<HelpContentProps, HelpContentState> {
    state: HelpContentState = { visible: null };
    render(): React.Node {
        const { visible } = this.state;
        const { header, outerCard, onlyEssential } = this.props;
        const setOpen = (type: HelpContentType) => {
            if (visible === type) {
                this.setState({ visible: null });
            } else {
                this.setState({ visible: type });
            }
        };
        return (
            <div>
                <HelpSection
                    type="vakttyper"
                    title="Korleis definerer TurnusLint vakttyper?"
                    openType={visible}
                    setVisibleSection={setOpen}
                    headerType={header}
                    outerCard={outerCard}
                >
                    <Vakttyper />
                </HelpSection>
                <HelpSection
                    type="korleisEksportere"
                    title="Korleis eksporterer eg frå GAT?"
                    openType={visible}
                    setVisibleSection={setOpen}
                    headerType={header}
                    outerCard={outerCard}
                >
                    <KorleisEksportere />
                </HelpSection>
                <HelpSection
                    type="kvaSjekker"
                    title="Kva sjekker TurnusLint?"
                    openType={visible}
                    setVisibleSection={setOpen}
                    headerType={header}
                    outerCard={outerCard}
                >
                    <KvaSjekker />
                </HelpSection>
                <HelpSection
                    type="kvaBetyr"
                    title="Kva meines med dei forskjellege orda og uttrykka?"
                    openType={visible}
                    setVisibleSection={setOpen}
                    headerType={header}
                    outerCard={outerCard}
                >
                    <KvaBetyr />
                </HelpSection>
                <HelpSection
                    type="tabellInfoOverload"
                    title="Eg vil skjule noko frå tabellen"
                    openType={visible}
                    setVisibleSection={setOpen}
                    headerType={header}
                    outerCard={outerCard}
                    visible={onlyEssential !== true}
                >
                    <TabellInfoOverload />
                </HelpSection>
            </div>
        );
    }
}

/**
 * Renders our HelpWindow.
 */
class HelpWindow extends React.Component<{|
    open: boolean,
    toggleVisibility: () => void,
|}> {
    render(): React.Node {
        const { open, toggleVisibility } = this.props;
        return (
            <Modal isOpen={open} toggle={toggleVisibility}>
                <ModalHeader toggle={toggleVisibility}>Hjelp</ModalHeader>
                <ModalBody className="help-window">
                    <HelpContent outerCard={true} />
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggleVisibility}>
                        Lukk
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }
}

export { HelpWindow, HelpContent, GetTooltipStringFor };

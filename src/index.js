/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import * as React from "react";
import ReactDOM from "react-dom";
import { FileChecker } from "./TurnusLint.js";
import GatParser from "./GatParser.js";
import { TurnusLintLogger } from "./logger.js";
import type { LogLevels, LogEntry } from "./logger.js";
import {
    Navbar,
    NavbarBrand,
    Nav,
    Container,
    Row,
    Col,
    Alert,
    Spinner,
    Button,
    ButtonGroup,
    UncontrolledTooltip,
    Input,
    Modal,
    ModalHeader,
    ModalBody,
    InputGroupAddon,
    InputGroup,
    ModalFooter,
} from "reactstrap";
import { XCircle } from "react-bootstrap-icons";
// $FlowExpectedError[cannot-resolve-module]
import "./style.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import { TableRenderer } from "./TableRenderer.js";
import { TurnusLintConfig } from "./Config.js";
import type { ShiftAnalysisContainer } from "./TurnusLint.js";
import { HelpContent, HelpWindow } from "./Help.js";
import { PersonReportRenderer } from "./Renderer";
import { ConfigurationSettings } from "./ConfigUI.js";
import { HolidayTableRenderer } from "./HolidayTableRenderer.js";
import { HolidayMetaWindow } from "./HolidayTableRenderer";
import { ShiftCountTableRenderer } from "./ShiftCountTableRenderer.js";
import { Holidays } from "./HolidaysClass";

declare var GIT_REVISION: string;
declare var GIT_REVISION_FULL: string;
declare var TURNUSLINT_DEBUG: boolean;
declare var USE_LIMITED_RULESET: boolean;
declare var BUILD_DATE: string;

/**
 * Renders an array of ShiftAnalysisContainer's as a report view using @{link PersonReportRenderer}.
 */
class ResultRenderer extends React.Component<{|
    people: Array<ShiftAnalysisContainer>,
|}> {
    render() {
        const { people } = this.props;
        const rendered = [];
        for (const person of people) {
            rendered.push(
                <PersonReportRenderer key={person.id()} person={person} />,
            );
        }
        return <div>{rendered}</div>;
    }
}

/**
 * Renders a single error, along with help text to assist the user in resolving it.
 */
class ErrorRenderer extends React.Component<
    {|
        error: LogEntry,
    |},
    {| showHelp: boolean |},
> {
    constructor(props) {
        super(props);
        this.state = {
            showHelp: false,
        };
    }

    render() {
        const { code, message } = this.props.error;
        let helpPrompt, help;
        if (this.state.showHelp) {
            let helpText;
            if (code === "FILECHECKER_LOAD_FAILED") {
                helpText = (
                    <span>
                        Fila du prøvde å laste inn i TurnusLint vart ikkje
                        korrekt motteke. Dette er som oftast eit problem med
                        nettlesaren din som vil løyse seg sjølv viss du prøver
                        igjen.
                    </span>
                );
            } else {
                helpText = (
                    <span>
                        Feil er oftast ein av to ting. Anten er det ein feil i
                        sjølve TurnusLint, t.d. noko i ein turnus som TurnusLint
                        ikkje forstår, eller så har du eksportert fila feil frå
                        GAT. Sjekk følgjande:
                        <ul>
                            <li>
                                At du har vald å eksportere i «klassisk» modus,
                                ikkje «grafisk» modus
                            </li>
                            <li>At du har haka av for «Vaktkoder»</li>
                            <li>
                                At papirstorleiken er A4 og formatet er
                                liggjande
                            </li>
                        </ul>
                        Når du eksporterer skal innstillignane vere sånn som er
                        markert på dette bilete:
                        <img
                            src="nonfree/gat-pers-arb-plan-win.png"
                            alt="Eksporter arbeidsplan-vindauget"
                            className="d-block"
                        />
                        Viss du er framleis får feil etter at du har sjekka at
                        fila er eksportert korrekt,{" "}
                        <a href="mailto:code@zerodogg.org">
                            kontakt meg på e-post
                        </a>
                        .
                    </span>
                );
            }
            help = (
                <div className="IntroErrorSection">
                    <div className="header">Hjelp til feilmelding</div>
                    {helpText}
                </div>
            );
        } else {
            helpPrompt = (
                <a
                    href="#"
                    onClick={(ev) => {
                        ev.preventDefault();
                        this.setState({ showHelp: true });
                    }}
                >
                    Hjelp meg.
                </a>
            );
        }
        const color = "danger";
        return (
            <div>
                <Alert color={color}>
                    {message} {helpPrompt}
                </Alert>
                {help}
            </div>
        );
    }
}

/**
 * Renders a list of errors using @{link ErrorRenderer}.
 */
class ErrorContainer extends React.Component<{|
    errors: Array<LogEntry>,
|}> {
    render() {
        const { errors } = this.props;
        if (errors.length === 0) {
            return null;
        }
        const renderWarnings = [];
        for (const entry of errors) {
            renderWarnings.push(
                <ErrorRenderer
                    key={"w-" + entry.time.getTime()}
                    error={entry}
                />,
            );
        }
        return renderWarnings;
    }
}

function AboutWindow(props: {| open: boolean, toggleVisibility: () => void |}) {
    const { open, toggleVisibility } = props;
    const isHelseVest = USE_LIMITED_RULESET === true ? " - Helse Vest" : "";
    return (
        <Modal isOpen={open}>
            <ModalHeader>Om TurnusLint</ModalHeader>
            <ModalBody>
                <h3>TurnusLint</h3>
                versjon {GIT_REVISION} ({GIT_REVISION_FULL + isHelseVest})
                <br />
                <br />
                Skrevet av Eskild Hustvedt i regi av seksjon for
                bemanningsløsninger og tarifforhandlinger i Helse Bergen.
            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={toggleVisibility}>
                    Lukk
                </Button>
            </ModalFooter>
        </Modal>
    );
}

type LogWindowProps = {|
    logger: TurnusLintLogger,
    toggleVisibility: () => void,
    open: boolean,
|};
type LogWindowState = {|
    display: null | LogLevels,
|};
class LogWindow extends React.Component<LogWindowProps, LogWindowState> {
    state: LogWindowState = {
        display: null,
    };
    render() {
        const { logger, open, toggleVisibility } = this.props;
        const { display } = this.state;
        if (open === false) {
            return <span />;
        }
        const log = [];
        let logEntries;
        if (display === null) {
            logEntries = [...logger.getAll()].reverse();
        } else {
            logEntries = [...logger.entries(display)].reverse();
        }
        let entryNo = 0;
        for (const entry of logEntries) {
            log.push(
                <div key={"log-" + entryNo++}>
                    <code>{logger.formatMessage(entry)}</code>
                </div>,
            );
        }
        return (
            <Modal isOpen toggle={toggleVisibility}>
                <ModalHeader toggle={toggleVisibility}>Logg</ModalHeader>
                <ModalBody>
                    <Alert>
                        Dette vindauget viser systemloggen til TurnusLint.
                        Informasjon her kan bidra til å feilsøkje problemer.
                    </Alert>
                    <ButtonGroup className="mb-2">
                        <InputGroupAddon
                            addonType="prepend"
                            className="no-select"
                        >
                            Vis
                        </InputGroupAddon>
                        <Button
                            color="primary"
                            onClick={() => this.setState({ display: null })}
                            active={this.state.display === null}
                            className={
                                TURNUSLINT_DEBUG === true ? "d-block" : "d-none"
                            }
                        >
                            Alt
                        </Button>
                        <Button
                            color="primary"
                            onClick={() => this.setState({ display: "DEBUG" })}
                            active={this.state.display === "DEBUG"}
                        >
                            Feilsøking ({logger.entries("DEBUG").length})
                        </Button>
                        <Button
                            color="primary"
                            onClick={() => this.setState({ display: "NOTICE" })}
                            active={this.state.display === "NOTICE"}
                        >
                            Merknad ({logger.entries("NOTICE").length})
                        </Button>
                        <Button
                            color="primary"
                            onClick={() =>
                                this.setState({ display: "WARNING" })
                            }
                            active={this.state.display === "WARNING"}
                        >
                            Advarsel ({logger.entries("WARNING").length})
                        </Button>
                        <Button
                            color="primary"
                            onClick={() => this.setState({ display: "ERROR" })}
                            active={this.state.display === "ERROR"}
                        >
                            Feil ({logger.entries("ERROR").length})
                        </Button>
                        <Button
                            color="primary"
                            onClick={() =>
                                this.setState({ display: "CRITICAL" })
                            }
                            active={this.state.display === "CRITICAL"}
                        >
                            Kritisk ({logger.entries("CRITICAL").length})
                        </Button>
                    </ButtonGroup>
                    <div className="logContent">{log}</div>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggleVisibility}>
                        Lukk
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }
}

type IntroScreenProps = {|
    logger: TurnusLintLogger,
|};
type IntroScreenState = {|
    displayHolidayList: boolean,
|};
class IntroScreen extends React.PureComponent<
    IntroScreenProps,
    IntroScreenState,
> {
    state: IntroScreenState = { displayHolidayList: false };
    _holidayObj: Holidays;
    holidayObj(logger: TurnusLintLogger): Holidays {
        if (this._holidayObj === undefined) {
            this._holidayObj = new Holidays(logger);
        }
        return this._holidayObj;
    }
    render(): React.Node {
        const { logger } = this.props;
        const { displayHolidayList } = this.state;
        const holidayList = [];
        if (displayHolidayList === true) {
            holidayList.push(
                <HolidayMetaWindow
                    key="holidayMetaIntro"
                    holidays={this.holidayObj(logger)}
                    toggleVisibility={() =>
                        this.setState({
                            displayHolidayList: !displayHolidayList,
                        })
                    }
                />,
            );
        }
        return [
            <div className="IntroErrorSection" key="introSection">
                <Row>
                    <Col>
                        <div className="h5 font-weight-bold">TurnusLint</div>
                        Opne ein PDF eksportert frå GAT for å analysere.
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <b>Hjelp</b>
                        <HelpContent header="" onlyEssential />
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <b>Verkty</b>
                        <div
                            className="likeLink"
                            onClick={() =>
                                this.setState({ displayHolidayList: true })
                            }
                        >
                            Sjå ein oversikt over heilagdagar.
                        </div>
                    </Col>
                </Row>
            </div>,
            holidayList,
        ];
    }
}

class SearchBox extends React.Component<{|
    search: string,
    onSearchChange: (string) => void,
|}> {
    render() {
        const { search, onSearchChange } = this.props;
        const hasSearchValue =
            search !== "" && search !== null && search !== undefined;
        return (
            <InputGroup className="has-validation">
                <Input
                    type="search"
                    value={search}
                    onChange={(e) => onSearchChange(e.target.value)}
                    placeholder="Søk …"
                />
                <span
                    className={
                        "form-control-clear " + (hasSearchValue ? "" : "d-none")
                    }
                >
                    <XCircle onClick={() => onSearchChange("")} />
                </span>
            </InputGroup>
        );
    }
}

function applySearchFilterToResult(
    search: string,
    people: Array<ShiftAnalysisContainer>,
) {
    let filteredPeople = people;
    if (
        search !== null &&
        search !== undefined &&
        search !== "" &&
        /\S/.test(search)
    ) {
        const searchStrings = search.toLowerCase().split(/\s+/);
        filteredPeople = people.reduce((previousValue, currentValue) => {
            let match = 0;
            const name = currentValue.name().toLowerCase();
            for (const str of searchStrings) {
                if (name.includes(str)) {
                    match++;
                }
            }
            if (match === searchStrings.length) {
                previousValue.push(currentValue);
            }
            return previousValue;
        }, []);
    }
    return filteredPeople;
}

type GatParserList = Array<{|
    filename: string,
    parser: GatParser,
|}>;

function ProgressIndicator(props: {|
    progress: string,
    config: TurnusLintConfig,
|}): React.Node {
    const { config } = props;
    let { progress } = props;
    let text = "Leser fil";
    if (progress !== "") {
        if (
            progress === "100%" &&
            config.getStringOption("displayMode") === "report"
        ) {
            text = "Lagar rapport";
            progress = "";
        } else {
            text = "Analyserer";
        }
    }
    return (
        <div id="analyzing">
            <Spinner size="sm" color="primary">
                {" "}
            </Spinner>{" "}
            {text} ... {progress}
        </div>
    );
}

type TopMenuProps = {|
    analyzing: boolean,
    checker: FileChecker,
    config: TurnusLintConfig,
    hasCriticalOrErrorLogEntries: boolean,
    logger: TurnusLintLogger,
    onSearchChange: (string) => void,
    search: string,
|};
type TopMenuState = {|
    progress: string,
    showLog: boolean,
    configVisible: boolean,
    helpVisible: boolean,
    aboutVisible: boolean,
|};
class TopMenu extends React.PureComponent<TopMenuProps, TopMenuState> {
    state: TopMenuState = {
        showLog: false,
        configVisible: false,
        helpVisible: false,
        aboutVisible: false,
        progress: "",
    };
    componentDidMount() {
        const { checker } = this.props;
        checker.listen((event, _, data) => {
            if (event === "ANALYZE_END") {
                this.setState({
                    progress: "",
                });
            } else if (event === "FILE_LOAD_PROGRESS" && data !== null) {
                this.setState({
                    progress: data,
                });
            }
        });
    }
    /**
     * This tracks the timestamp for when we opened the config window. We use
     * this to call config.hasChangedSince to detect if we need to .renalyze or
     * not.
     */
    _configDisplayedAt: number = -1;
    render() {
        const {
            analyzing,
            checker,
            config,
            hasCriticalOrErrorLogEntries,
            logger,
            onSearchChange,
            search,
        } = this.props;
        const { progress } = this.state;
        const { aboutVisible } = this.state;
        return [
            <Navbar color="light" className="sticky-top" light key="topmenu">
                <NavbarBrand className="no-select">
                    <span onClick={() => this.setState({ aboutVisible: true })}>
                        TurnusLint
                    </span>{" "}
                    <span
                        className={[
                            "align-top",
                            "git-revision",
                            hasCriticalOrErrorLogEntries === true
                                ? "text-danger"
                                : "",
                        ].join(" ")}
                        id="git-revision"
                        onClick={() => this.setState({ showLog: true })}
                    >
                        {GIT_REVISION}
                    </span>
                    <UncontrolledTooltip
                        placement="right"
                        target="git-revision"
                    >
                        Dette er TurnusLint revisjon {GIT_REVISION_FULL},
                        byggdato {BUILD_DATE}
                    </UncontrolledTooltip>
                </NavbarBrand>
                <Nav
                    key="fileSelector"
                    className={"mr-4 " + (analyzing ? "d-none" : "d-block")}
                    navbar
                >
                    <Button
                        color="secondary"
                        onClick={() => {
                            const fileSelector =
                                document.getElementById("fileSelector");
                            if (
                                fileSelector !== undefined &&
                                fileSelector !== null
                            ) {
                                fileSelector.click();
                            }
                        }}
                    >
                        Opne fil …
                    </Button>
                    <input
                        name="fileSelector"
                        id="fileSelector"
                        type="file"
                        accept=".pdf,application/pdf"
                        multiple
                        onChange={() => {
                            const fileSel =
                                document.getElementById("fileSelector");
                            if (
                                fileSel !== undefined &&
                                fileSel !== null &&
                                fileSel instanceof HTMLInputElement
                            ) {
                                checker.addFiles(fileSel.files, {
                                    mode: "exclusive",
                                });
                                // Chrome requires value to be reset if we're
                                // going to be able to "open" the same file two
                                // times in a row.
                                //
                                // Flow doesn't like this since it's not
                                // actually suppose to be nullable, but we
                                // still need to do it, so override the flow
                                // type error.
                                //
                                // $FlowExpectedError[incompatible-type]
                                fileSel.value = null;
                            }
                        }}
                    />
                </Nav>
                <Nav
                    key="configButton"
                    className={"mr-4 " + (analyzing ? "d-none" : "d-block")}
                    navbar
                >
                    <Button
                        color="secondary"
                        onClick={() => {
                            this.setState({ configVisible: true });
                        }}
                    >
                        Innstillingar
                    </Button>
                </Nav>
                <Nav
                    key="helpButton"
                    className={"mr-auto " + (analyzing ? "d-none" : "d-block")}
                    navbar
                >
                    <Button
                        color="secondary"
                        onClick={() => {
                            this.setState({ helpVisible: true });
                        }}
                    >
                        Hjelp
                    </Button>
                </Nav>
                <Nav
                    key="analyzing"
                    className={"mr-auto " + (analyzing ? "d-block" : "d-none")}
                    navbar
                >
                    <ProgressIndicator progress={progress} config={config} />
                </Nav>
                <Nav key="search" navbar className="mr-1">
                    <SearchBox
                        search={search}
                        onSearchChange={onSearchChange}
                    />
                </Nav>
                <Nav key="mode" navbar>
                    <ButtonGroup>
                        <InputGroupAddon
                            addonType="prepend"
                            className="no-select"
                        >
                            Vis
                        </InputGroupAddon>
                        <Button
                            color="primary"
                            onClick={() =>
                                config.setStringOption("displayMode", "report")
                            }
                            active={
                                config.getStringOption("displayMode") ===
                                "report"
                            }
                        >
                            Rapport
                        </Button>
                        <Button
                            color="primary"
                            onClick={() =>
                                config.setStringOption("displayMode", "table")
                            }
                            active={
                                config.getStringOption("displayMode") ===
                                "table"
                            }
                        >
                            Tabell
                        </Button>
                        <Button
                            color="primary"
                            onClick={() =>
                                config.setStringOption(
                                    "displayMode",
                                    "holidays",
                                )
                            }
                            active={
                                config.getStringOption("displayMode") ===
                                "holidays"
                            }
                        >
                            Heilagdagar
                        </Button>
                        <Button
                            color="primary"
                            onClick={() =>
                                config.setStringOption(
                                    "displayMode",
                                    "shiftCount",
                                )
                            }
                            active={
                                config.getStringOption("displayMode") ===
                                "shiftCount"
                            }
                        >
                            Vaktfordeling
                        </Button>
                    </ButtonGroup>
                </Nav>
            </Navbar>,
            <LogWindow
                key="log-window"
                logger={logger}
                open={this.state.showLog}
                toggleVisibility={() =>
                    this.setState({ showLog: !this.state.showLog })
                }
            />,
            <ConfigurationSettings
                key="config-window"
                config={config}
                visible={this.state.configVisible}
                toggleVisibility={() => {
                    if (this.state.configVisible === true) {
                        if (config.hasChangedSince(this._configDisplayedAt)) {
                            checker.reanalyze();
                        }
                    } else {
                        this._configDisplayedAt = new Date().getTime();
                    }
                    this.setState({
                        configVisible: !this.state.configVisible,
                    });
                }}
            />,
            <HelpWindow
                key="help-window"
                open={this.state.helpVisible}
                toggleVisibility={() => {
                    this.setState({
                        helpVisible: !this.state.helpVisible,
                    });
                }}
            />,
            <AboutWindow
                key="about-window"
                open={aboutVisible}
                toggleVisibility={() =>
                    this.setState({ aboutVisible: !aboutVisible })
                }
            />,
        ];
    }
}

type TurnusLintState = {|
    analyzing: boolean,
    checker: FileChecker,
    config: TurnusLintConfig,
    criticalLogEntries: Array<LogEntry>,
    hasCriticalOrErrorLogEntries: boolean,
    logger: TurnusLintLogger,
    parsers: GatParserList,
    search: string,
|};

class TurnusLint extends React.Component<{||}, TurnusLintState> {
    constructor(props) {
        super(props);

        const logger = new TurnusLintLogger();
        const config = new TurnusLintConfig();

        this.state = {
            analyzing: false,
            checker: new FileChecker(logger, config),
            criticalLogEntries: [],
            hasCriticalOrErrorLogEntries: false,
            parsers: [],
            search: "",
            config,
            logger,
        };
        this.state.logger.onNewLogMessage((message) => {
            if (message.level === "ERROR" || message.level === "CRITICAL") {
                this.setState({
                    hasCriticalOrErrorLogEntries: true,
                });
                if (message.level === "CRITICAL") {
                    this.setState({
                        criticalLogEntries:
                            this.state.criticalLogEntries.concat([message]),
                    });
                }
            }
        });
        this.state.checker.listen((event, _, __) => {
            if (event === "FILE_LOAD_START" || event === "ANALYZE_START") {
                this.setState({
                    analyzing: true,
                });
                if (event === "FILE_LOAD_START" && document.body !== null) {
                    document.body.classList.add("body-loading");
                }
            } else if (event === "ANALYZE_END") {
                this.setState({
                    analyzing: false,
                });
                if (document.body !== null) {
                    document.body.classList.remove("body-loading");
                }
            } else if (event === "NEW_RESULT_AVAILABLE") {
                this.setState({
                    criticalLogEntries: [],
                });
                this.forceUpdate();
            }
        });
        this.state.config.listen((event, option, _, __) => {
            if (event === "CONFIG_LOADED" || option === "displayMode") {
                this.forceUpdate();
            }
        });
    }

    onDrop(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = "copy";
        this.state.checker.addFiles(evt.dataTransfer.files, {
            mode: "exclusive",
        });
    }

    onDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = "copy";
    }

    componentDidMount() {
        // We don't remove the listeners in an componentDidUnmount since this
        // object should never unmount
        window.addEventListener("dragover", (ev) => this.onDragOver(ev), false);
        window.addEventListener(
            "drop",
            (ev) => {
                this.onDrop(ev);
            },
            false,
        );
        window.onerror = (message, source, lineno, colno, error) => {
            this.state.logger.critical(
                "TurnusLint>window.onerror",
                "Ein ukjend feil oppstod: " + message,
                { additional: error },
            );
        };
    }

    render() {
        let content;
        // Wait for the config to load before showing the UI
        if (this.state.config.hasLoaded() === false) {
            return <span />;
        }
        const className = ["primaryContainer"];
        if (this.state.checker.entries.length == 0 && !this.state.analyzing) {
            content = <IntroScreen logger={this.state.logger} />;
        } else {
            const result = applySearchFilterToResult(
                this.state.search,
                this.state.checker.entries,
            );
            if (result.length === 0 && this.state.checker.entries.length > 0) {
                content = (
                    <span>
                        <b>Ingen treff for «{this.state.search}».</b> Prøv andre
                        (eller ingen) søkjeord.
                    </span>
                );
            } else if (
                this.state.config.getStringOption("displayMode") === "report"
            ) {
                content = <ResultRenderer people={result} />;
            } else if (
                this.state.config.getStringOption("displayMode") ===
                    "shiftCount" &&
                this.state.checker.entries.length > 0
            ) {
                content = (
                    <ShiftCountTableRenderer
                        people={result}
                        config={this.state.config}
                        analyzing={this.state.analyzing}
                        search={this.state.search}
                    />
                );
                className.push("max-width-3000");
                className.push("report-view");
            } else if (
                this.state.config.getStringOption("displayMode") ===
                    "holidays" &&
                this.state.checker.entries.length > 0
            ) {
                content = (
                    <HolidayTableRenderer
                        people={result}
                        search={this.state.search}
                        config={this.state.config}
                        holidays={this.state.checker.holidays}
                        analyzing={this.state.analyzing}
                        onSearchAndSwitchToReport={(search) => {
                            this.state.config.setStringOption(
                                "displayMode",
                                "report",
                            );
                            this.setState({
                                search,
                            });
                        }}
                    />
                );
                className.push("max-width-2000");
                className.push("report-view");
            } else if (
                this.state.config.getStringOption("displayMode") === "table" &&
                this.state.checker.entries.length > 0
            ) {
                content = (
                    <TableRenderer
                        people={result}
                        search={this.state.search}
                        config={this.state.config}
                        onSearchAndSwitchToReport={(search) => {
                            this.state.config.setStringOption(
                                "displayMode",
                                "report",
                            );
                            this.setState({
                                search,
                            });
                        }}
                    />
                );
                className.push("max-width-2000");
                className.push("report-view");
            }
        }
        return (
            <div>
                <TopMenu
                    hasCriticalOrErrorLogEntries={
                        this.state.hasCriticalOrErrorLogEntries
                    }
                    checker={this.state.checker}
                    analyzing={this.state.analyzing}
                    onSearchChange={(update: string) =>
                        this.setState({ search: update })
                    }
                    search={this.state.search}
                    config={this.state.config}
                    logger={this.state.logger}
                />
                <Container className={className.join(" ")}>
                    <Row>
                        <Col>
                            <ErrorContainer
                                errors={this.state.criticalLogEntries}
                            />
                        </Col>
                    </Row>
                    {content}
                </Container>
            </div>
        );
    }
}

function main() {
    const root = document.getElementById("root");
    if (root) {
        ReactDOM.render(<TurnusLint />, root);
    }
}

main();

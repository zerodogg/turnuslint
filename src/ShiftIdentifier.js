/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * @flow
 */
import type { BaseShiftTypes, ShiftType } from "./sharedtypes.js";
export type ShiftList = {|
    [string]: ShiftType,
|};
type IDMap = {
    [string]: BaseShiftTypes,
};
type ShiftIshObject = {|
    obj: string,
|};
import { TurnusLintLogger } from "./logger.js";
type ShiftIshThing = ShiftIshObject | string;
class ShiftIdentifier {
    _shifts: ShiftList;
    _id: IDMap;
    _logger: TurnusLintLogger;
    _fuzzyIdentified: {|
        [string]: string | null,
    |} = {};
    _hasSeenUnidentified: {|
        [string]: boolean,
    |} = {};

    constructor(shifts: ShiftList, logger: TurnusLintLogger) {
        this._id = {};
        this._shifts = shifts;
        this._logger = logger;

        for (const shift in shifts) {
            const entry = shifts[shift];
            if (entry.crossesDays) {
                // This is N
                this._id[shift] = "N";
            } else if (entry.start.hour > 12) {
                this._id[shift] = "A";
            } else if (entry.off) {
                this._id[shift] = "F";
            } else {
                this._id[shift] = "D";
            }
        }
    }

    getShift(shift: string): ShiftType | null {
        if (this._shifts[shift] === undefined) {
            const alternateShift1 = this._fuzzyGetShift(shift);
            if (alternateShift1 !== null) {
                return this._shifts[alternateShift1];
            }
            return null;
        } else {
            return this._shifts[shift];
        }
    }

    timeBetweenShifts(shift1: string, shift2: string): number {
        const shift1Obj = this.getShift(shift1);
        const shift2Obj = this.getShift(shift2);
        if (shift1Obj === null || shift2Obj === null) {
            const errorShifts = [];
            if (shift1Obj === null) {
                errorShifts.push(shift1);
            }
            if (shift2Obj === null) {
                errorShifts.push(shift2);
            }
            this._logger.warning(
                "ShiftIdentifier",
                ".timeBetweenShifts(" +
                    JSON.stringify(shift1) +
                    "," +
                    JSON.stringify(shift2) +
                    "): " +
                    errorShifts.join(" and ") +
                    " could not be identified as a shift. Falling back to returning 0",
                { stack: true },
            );
            return 0;
        }
        const firstEnd = shift1Obj.end.decimal;
        const secondStart = shift2Obj.start.decimal;
        let timeBetween = 0;
        if (firstEnd > secondStart) {
            timeBetween = secondStart + 24 - firstEnd;
        } else {
            timeBetween = secondStart - firstEnd;
        }
        return timeBetween;
    }

    _getShiftFrom(obj: ShiftIshThing): string {
        let result = obj;
        // Allow objects with a "shift" key
        if (typeof result === "object") {
            if (typeof result.obj === "string") {
                result = result.obj;
            }
        }
        if (typeof result !== "string") {
            this._logger.warning(
                "ShiftIdentifier",
                "Attempt to ._getShiftFrom() a non-string value (falling back to 'UNKNOWN'): " +
                    "original value was " +
                    JSON.stringify(obj) +
                    ", after processing it was " +
                    JSON.stringify(result),
                { stack: true },
            );
            return "UNKNOWN";
        }
        return result;
    }

    _fuzzyGetShift(shift: ShiftIshThing): string | null {
        shift = this._getShiftFrom(shift);
        const fullShift = shift;
        if (this._shifts[shift] !== undefined) {
            return shift;
        }
        if (this._fuzzyIdentified[shift] !== undefined) {
            return this._fuzzyIdentified[shift];
        }
        while (shift.length > 0) {
            shift = shift.substr(0, shift.length - 1);
            const hits = [];
            for (const subtype of Object.keys(this._shifts)) {
                if (subtype.indexOf(shift) === 0) {
                    hits.push(subtype);
                }
            }
            if (hits.length === 1) {
                this._fuzzyIdentified[fullShift] = hits[0];
                return this._fuzzyIdentified[fullShift];
            } else if (hits.length > 1) {
                break;
            }
        }
        this._fuzzyIdentified[fullShift] = null;
        return null;
    }

    identify(shift: ShiftIshThing): BaseShiftTypes | "UNKNOWN" {
        if (shift === undefined) {
            this._logger.warning(
                "ShiftIdentifier",
                "Attempt to .identify() undefined",
                { stack: true },
            );
            return "UNKNOWN";
        }
        shift = this._getShiftFrom(shift);
        const identified = this._id[shift];
        if (identified) {
            return identified;
        } else if (shift === "") {
            return "F";
        } else {
            const fuzzy = this._fuzzyGetShift(shift);
            if (fuzzy !== null) {
                return this._id[fuzzy];
            }
            const shiftStr = JSON.stringify(shift);
            if (this._hasSeenUnidentified[shiftStr] !== true) {
                this._hasSeenUnidentified[shiftStr] = true;
                this._logger.error(
                    "ShiftIdentifier",
                    "Unidentified shift: " + shiftStr,
                );
            }
            return "UNKNOWN";
        }
    }

    isValidShift(shift: ShiftIshThing): boolean {
        if (shift === undefined) {
            return false;
        }
        shift = this._getShiftFrom(shift);
        const identified = this._id[shift];
        if (identified || shift === "") {
            return true;
        } else {
            return false;
        }
    }

    isA(shift: ShiftIshThing): boolean {
        return this.identify(shift) == "A";
    }

    isD(shift: ShiftIshThing): boolean {
        return this.identify(shift) == "D";
    }

    isN(shift: ShiftIshThing): boolean {
        return this.identify(shift) == "N";
    }

    isAD(shift1: ShiftIshThing, shift2: ShiftIshThing): boolean {
        if (this.identify(shift1) === "A" && this.identify(shift2) === "D") {
            if (
                this.timeBetweenShifts(
                    this._getShiftFrom(shift1),
                    this._getShiftFrom(shift2),
                ) < 11
            ) {
                return true;
            }
        }
        return false;
    }

    isF(shift: ShiftIshThing): boolean {
        if (
            (typeof shift === "string" && shift == "") ||
            shift == undefined ||
            this.identify(shift) == "F"
        ) {
            return true;
        }
        return false;
    }

    isAllF(shifts: Array<string>): boolean {
        for (const shift of shifts) {
            if (this.isF(shift) === false) {
                return false;
            }
        }
        return true;
    }

    // Checks if a shift is 'explicitly' F, that is to say it has an 'F' code
    // and is not simply empty.
    isExplicitF(shift: ShiftIshThing): boolean {
        shift = this._getShiftFrom(shift);
        if (shift == "" || shift == undefined) {
            return false;
        }
        return this.identify(shift) === "F";
    }
}

export { ShiftIdentifier };

/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import * as React from "react";
import { Table } from "reactstrap";
import type { SortDirection } from "./TableRenderer.js";

import uniq from "lodash/uniq";
import { ShiftAnalysisContainer } from "./TurnusLint.js";
import { TurnusLintConfig } from "./Config";
import { SortableTableHeader } from "./TableRenderer.js";
import { filterPeople } from "./TableRenderer";

type ShiftCountPersonRendererProps = {|
    person: ShiftAnalysisContainer,
    config: TurnusLintConfig,
    shiftTypes: Array<string>,
|};
class ShiftCountPersonRenderer extends React.Component<ShiftCountPersonRendererProps> {
    render(): React.Node {
        const { person, shiftTypes } = this.props;
        const row = [<td key="name">{person.name()}</td>];
        for (const sType of shiftTypes) {
            row.push(
                <td key={sType + "for" + person.id()}>
                    {person.shiftCountFor(sType)}
                </td>,
            );
        }
        return <tr>{row}</tr>;
    }
}

/**
 * Filters and sorts an array of ShiftAnalysisContainer.
 */
function sortPeople(
    people: Array<ShiftAnalysisContainer>,
    direction: SortDirection,
    sortKey: string | null,
): Array<ShiftAnalysisContainer> {
    if (sortKey !== null) {
        // Make a copy of the array to sort
        let filteredPeople = [...people];
        if (sortKey === "NAME") {
            if (direction === "asc") {
                filteredPeople = filteredPeople.sort((a, b) =>
                    a.name().localeCompare(b.name()),
                );
            } else {
                filteredPeople = filteredPeople.sort((a, b) =>
                    b.name().localeCompare(a.name()),
                );
            }
        } else {
            if (direction === "asc") {
                filteredPeople = filteredPeople.sort(
                    (a, b) =>
                        a.shiftCountFor(sortKey) - b.shiftCountFor(sortKey),
                );
            } else {
                filteredPeople = filteredPeople.sort(
                    (a, b) =>
                        b.shiftCountFor(sortKey) - a.shiftCountFor(sortKey),
                );
            }
        }
        return filteredPeople;
    }
    return people;
}

type ShiftCountTableRendererProps = {|
    people: Array<ShiftAnalysisContainer>,
    config: TurnusLintConfig,
    search: string,
    analyzing: boolean,
|};
/**
 * State for HolidayTableRenderer.
 */
type ShiftCountTableRendererState = {|
    direction: SortDirection,
    sortKey: string | null,
    includeHidden: boolean,
    metaWindowOpen: boolean,
|};
class ShiftCountTableRenderer extends React.Component<
    ShiftCountTableRendererProps,
    ShiftCountTableRendererState,
> {
    state: ShiftCountTableRendererState = {
        direction: "desc",
        sortKey: null,
        includeHidden: false,
        metaWindowOpen: false,
    };
    render(): React.Node {
        const { people, config, search } = this.props;
        const { sortKey, direction } = this.state;
        let shiftTypes = [];
        const shiftTypeInstances: {| [string]: number |} = {};
        const headers = [];
        const rows = [];
        const filteredPeople = sortPeople(
            filterPeople(people, search),
            direction,
            sortKey,
        );
        const onChangeSort = (key: string | null) => {
            let newDir = direction;
            if (key === sortKey) {
                newDir = direction === "desc" ? "asc" : "desc";
            } else {
                newDir = "desc";
            }
            this.setState({
                sortKey: key,
                direction: newDir,
            });
        };
        for (const person of filteredPeople) {
            for (const sType of person.shiftList()) {
                shiftTypes.push(sType);
                if (shiftTypeInstances[sType] === undefined) {
                    shiftTypeInstances[sType] = person.shiftCountFor(sType);
                } else {
                    shiftTypeInstances[sType] += person.shiftCountFor(sType);
                }
            }
        }
        shiftTypes = uniq(shiftTypes)
            .sort()
            .sort((a, b) => shiftTypeInstances[b] - shiftTypeInstances[a]);
        for (const sType of shiftTypes) {
            headers.push(
                <SortableTableHeader
                    key={sType + "header"}
                    sortedBy={sortKey}
                    sortDir={direction}
                    entryKey={sType}
                    onChangeSort={() => onChangeSort(sType)}
                    config={config}
                    neverHide
                    visible
                    toggleColumnVisibility={() => {}}
                >
                    {sType}
                </SortableTableHeader>,
            );
        }
        for (const person of filteredPeople) {
            rows.push(
                <ShiftCountPersonRenderer
                    person={person}
                    config={config}
                    key={"person-" + person.id()}
                    shiftTypes={shiftTypes}
                />,
            );
        }
        return (
            <Table
                size="sm"
                hover
                className="force-default-cursor"
                key="main-table-view"
                responsive
            >
                <thead>
                    <tr>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="NAME"
                            onChangeSort={() => onChangeSort("NAME")}
                            neverHide
                            visible
                            toggleColumnVisibility={() => {}}
                        >
                            Namn
                        </SortableTableHeader>
                        {headers}
                    </tr>
                </thead>
                <tbody>{rows}</tbody>
            </Table>
        );
    }
}

export { ShiftCountTableRenderer };

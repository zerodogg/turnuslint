/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import * as React from "react";
import {
    Button,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    Row,
    Col,
    ModalFooter,
} from "reactstrap";
import type { SortDirection } from "./TableRenderer.js";

import { ShiftAnalysisContainer } from "./TurnusLint.js";
import { TurnusLintConfig } from "./Config";
import { SortableTableHeader } from "./TableRenderer.js";
import { filterPeople } from "./TableRenderer";
import type { SpecialDays } from "./HolidaysClass.js";
import type { Holidays } from "./HolidaysClass";
import { dayNamer } from "./TurnusLint";
import { generate as CSVGenerate } from "lil-csv";
import getISOWeek from "date-fns/getISOWeek";
import { dateFnsGetFormat } from "./utils.js";
type HolidayTableType = Array<{|
    key: SpecialDays,
    title: string,
    shortTitle?: string,
    onlyVeryWideScreens?: boolean,
|}>;

const HolidayTableColumns: HolidayTableType = [
    {
        key: "EASTER_THURSDAY",
        title: "Skjærtorsdag",
        shortTitle: "Skjærtor.",
    },
    {
        key: "EASTER_FRIDAY",
        title: "Langfredag",
        shortTitle: "Langfre.",
    },
    {
        key: "EASTER_SATURDAY",
        title: "Påskeaftan",
    },
    {
        key: "EASTER_MONDAY",
        title: "2. påskedag",
    },
    {
        key: "PINSE_MONDAY",
        title: "2. pinsedag",
    },
    {
        key: "ASCENSIONDAY",
        title: "Kr. himmelferdsdag",
        shortTitle: "Kr.himmelf.",
    },
    {
        key: "1MAY",
        title: "1. mai",
    },
    {
        key: "17MAY",
        title: "17. mai",
    },
    {
        key: "YULEEVE",
        title: "Julaftan",
    },
    {
        key: "1YULEDAY",
        title: "1. juledag",
    },
    {
        key: "2YULEDAY",
        title: "2. juledag",
    },
    {
        key: "NEW_YEARS_EVE",
        title: "Nyttårsftan",
        shortTitle: "Nyttårsaf.",
    },
    {
        key: "NEW_YEAR_DAY",
        title: "1. nyttårsdag",
    },
];

type HolidayMetaWindowProps = {|
    holidays: Holidays,
    toggleVisibility: () => void,
|};
type HolidayMetaWindowState = {|
    year: number | null,
|};
class HolidayMetaWindow extends React.PureComponent<
    HolidayMetaWindowProps,
    HolidayMetaWindowState,
> {
    state: HolidayMetaWindowState = { year: null };
    year(): number {
        const { holidays } = this.props;
        const stateYear = this.state.year;
        if (stateYear === null) {
            const baseYear = holidays.getHolidayInfoFor("EASTER_MONDAY");
            if (baseYear !== null) {
                return baseYear.year;
            }
            return new Date().getFullYear();
        }
        return stateYear;
    }
    render(): React.Node {
        const { holidays, toggleVisibility } = this.props;
        const year = this.year();
        const range = holidays.yearRange();
        const tableRows = [];
        let redWeekdays = 0;
        for (const holiday of holidays.getHolidayDatesForYear(year)) {
            const date = new Date(holiday.year, holiday.month - 1, holiday.day);
            let calendarWeek = getISOWeek(date);
            if (holiday.month === 1 && calendarWeek > 10) {
                calendarWeek += " (" + (holiday.year - 1) + ")";
            } else if (holiday.month === 12 && calendarWeek < 40) {
                calendarWeek += " (" + (holiday.year + 1) + ")";
            }
            let isWeekday = true;
            let isRed = false;
            if (date.getDay() === 0 || date.getDay() === 6) {
                isWeekday = false;
            }
            const entry = holidays.getHolidayOnDate(
                holiday.year,
                holiday.month,
                holiday.day,
            );
            if (entry !== null && entry.dayType === "RED") {
                isRed = true;
            }
            if (isWeekday && isRed) {
                redWeekdays++;
            }
            tableRows.push(
                <tr key={holiday.name}>
                    <td className={isRed ? "text-danger" : ""}>
                        {holiday.name}
                    </td>
                    <td className={isWeekday ? "font-weight-bold" : ""}>
                        {dateFnsGetFormat(date, "eeee")}
                    </td>
                    <td>{dateFnsGetFormat(date, "dd. LLLL")}</td>
                    <td>{calendarWeek}</td>
                </tr>,
            );
        }
        return (
            <Modal isOpen={true} toggle={toggleVisibility}>
                <ModalHeader toggle={toggleVisibility}>
                    Heilagdagar i {year}
                </ModalHeader>
                <ModalBody>
                    <Row className="mb-2">
                        <Col className="text-right">
                            <Button
                                color="light"
                                disabled={range.first >= year}
                                onClick={() =>
                                    this.setState({ year: year - 1 })
                                }
                            >
                                &larr; førre år
                            </Button>
                        </Col>
                        <Col className="text-center h4">{year}</Col>
                        <Col>
                            <Button
                                color="light"
                                disabled={range.last <= year}
                                onClick={() =>
                                    this.setState({ year: year + 1 })
                                }
                            >
                                neste år &rarr;
                            </Button>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col>
                            Det er {redWeekdays} heilagdagar som fell mellom
                            måndag-fredag i {year}.
                        </Col>
                    </Row>
                    <Table hover size="sm">
                        <thead>
                            <tr>
                                <th></th>
                                <th>vekedag</th>
                                <th>dato</th>
                                <th>kalenderveke</th>
                            </tr>
                        </thead>
                        <tbody>{tableRows}</tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggleVisibility}>
                        Lukk
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }
}

/**
 * Generates a download of a specified string as a utf-8 text/csv file.
 */
function downloadStringToFile(str: string, filename: string) {
    // new Uint8Array([0xEF, 0xBB, 0xBF]), is the UTF8 BOM
    const blob = new Blob([new Uint8Array([0xef, 0xbb, 0xbf]), str], {
        type: "text/csv; charset=utf-8",
    });
    const blobPath = URL.createObjectURL(blob);
    const body = document.body;

    if (body === null || body === undefined) {
        throw "No document.body? Very very confused";
    }

    const downloadLink = document.createElement("a");
    downloadLink.href = blobPath;
    downloadLink.download = filename;
    body.appendChild(downloadLink);
    downloadLink.click();
    body.removeChild(downloadLink);
}
/**
 * Builds a CSV-string (with ; as delimiter) containing the same data as the table.
 */
function holidaysToCSV(people: Array<ShiftAnalysisContainer>): string {
    const buildArray: Array<Array<string>> = [];
    const header = ["Namn"];
    for (const column of HolidayTableColumns) {
        header.push(column.title);
    }
    buildArray.push(header);
    for (const person of people) {
        const row = [person.name()];
        for (const column of HolidayTableColumns) {
            const day = person.getSpecialDay(column.key);
            let value = "fri";
            if (day !== null) {
                value = day.shift;
            }
            row.push(value);
        }
        buildArray.push(row);
    }
    // Excel expects ;-delimited files
    return CSVGenerate(buildArray, {
        delimiter: ";",
    });
}
/**
 * Props for TableEntryRenderer.
 */
type HolidayTableEntryRendererProps = {|
    day: SpecialDays,
    person: ShiftAnalysisContainer,
    onlyWideScreens?: boolean,
    onlyVeryWideScreens?: boolean,
    neverHide?: boolean,
|};
/**
 * State for HolidayTableEntryRenderer.
 */
type HolidayTableEntryRendererState = {|
    uniqueid: number,
|};
/**
 * Renders a single column of the table view.
 */
class HolidayTableEntryRenderer extends React.PureComponent<
    HolidayTableEntryRendererProps,
    HolidayTableEntryRendererState,
> {
    render(): React.Node {
        const { day, person, onlyWideScreens, onlyVeryWideScreens } =
            this.props;
        let className = "";
        let value = "fri";
        if (onlyVeryWideScreens) {
            className = "d-none d-xxl-table-cell";
        } else if (onlyWideScreens) {
            className = "d-none d-xl-table-cell";
        }
        const special = person.getSpecialDay(day);
        if (special !== null) {
            value = special.shift;
        } else {
            className += " text-veryfaded";
        }
        return <td className={className}>{value}</td>;
    }
}
/**
 * Props for HolidayTableRowRenderer.
 */
type HolidayTableRowRendererProps = {|
    person: ShiftAnalysisContainer,
    onSearchAndSwitchToReport: (string) => void,
    config: TurnusLintConfig,
|};
/**
 * Renders the complete table row for a single person.
 */
class HolidayTableRowRenderer extends React.Component<HolidayTableRowRendererProps> {
    render() {
        const { person, onSearchAndSwitchToReport } = this.props;
        const onClickHandler = () => onSearchAndSwitchToReport(person.name());
        const entries = [];
        for (const column of HolidayTableColumns) {
            entries.push(
                <HolidayTableEntryRenderer
                    person={person}
                    day={column.key}
                    key={column.key + "column" + person.name()}
                />,
            );
        }
        return (
            <tr onClick={onClickHandler}>
                <td>{person.name()}</td>
                {entries}
            </tr>
        );
    }
}
function getSortableValue(
    person: ShiftAnalysisContainer,
    day: SpecialDays,
): number {
    const special = person.getSpecialDay(day);
    if (special === null) {
        return -1;
    }
    return special.sortValue;
}
/**
 * Filters and sorts an array of ShiftAnalysisContainer.
 */
function sortPeople(
    people: Array<ShiftAnalysisContainer>,
    direction: SortDirection,
    sortKey: SortableHolidayValues,
    sorter: (ShiftAnalysisContainer, SpecialDays) => number,
): Array<ShiftAnalysisContainer> {
    if (sortKey !== null) {
        // Make a copy of the array to sort
        let filteredPeople = [...people];
        if (sortKey === "NAME") {
            if (direction === "asc") {
                filteredPeople = filteredPeople.sort((a, b) =>
                    a.name().localeCompare(b.name()),
                );
            } else {
                filteredPeople = filteredPeople.sort((a, b) =>
                    b.name().localeCompare(a.name()),
                );
            }
        } else {
            if (direction === "asc") {
                filteredPeople = filteredPeople.sort(
                    (a, b) => sorter(a, sortKey) - sorter(b, sortKey),
                );
            } else {
                filteredPeople = filteredPeople.sort(
                    (a, b) => sorter(b, sortKey) - sorter(a, sortKey),
                );
            }
        }
        return filteredPeople;
    }
    return people;
}

type SortableHolidayValues = SpecialDays | "NAME" | null;

/**
 * Props for HolidayTableRenderer.
 */
type HolidayTableRendererProps = {|
    people: Array<ShiftAnalysisContainer>,
    onSearchAndSwitchToReport: (string) => void,
    search: string,
    config: TurnusLintConfig,
    holidays: Holidays,
    analyzing: boolean,
|};
/**
 * State for HolidayTableRenderer.
 */
type HolidayTableRendererState = {|
    direction: SortDirection,
    sortKey: SortableHolidayValues,
    includeHidden: boolean,
    metaWindowOpen: boolean,
|};
/**
 * Renders a complete table for all people provided.
 */
class HolidayTableRenderer extends React.Component<
    HolidayTableRendererProps,
    HolidayTableRendererState,
> {
    state: HolidayTableRendererState = {
        direction: "desc",
        sortKey: null,
        includeHidden: false,
        metaWindowOpen: false,
    };

    render(): React.Node {
        const {
            people,
            search,
            onSearchAndSwitchToReport,
            config,
            holidays,
            analyzing,
        } = this.props;
        const { direction, sortKey, includeHidden, metaWindowOpen } =
            this.state;
        let metaWindow;
        if (metaWindowOpen) {
            metaWindow = (
                <HolidayMetaWindow
                    holidays={holidays}
                    key="metawindow"
                    toggleVisibility={() =>
                        this.setState({ metaWindowOpen: !metaWindowOpen })
                    }
                />
            );
        }
        //let ignoredCounter: number = 0;
        // FIXME: Sorting
        const filteredPeople = sortPeople(
            filterPeople(people, search),
            direction,
            sortKey,
            getSortableValue,
        );
        const onChangeSort = (key: SortableHolidayValues) => {
            let newDir = direction;
            if (key === sortKey) {
                newDir = direction === "desc" ? "asc" : "desc";
            } else {
                newDir = "desc";
            }
            this.setState({
                sortKey: key,
                direction: newDir,
            });
        };
        let primaryYear: number = 0;
        let ignoredCounter: number = 0;
        const rows = [];
        const ignoredHeader = [];
        for (const person of filteredPeople) {
            if (person.ignored() === true) {
                ignoredCounter++;
            }
            if (person.ignored() !== true || includeHidden) {
                rows.push(
                    <HolidayTableRowRenderer
                        onSearchAndSwitchToReport={onSearchAndSwitchToReport}
                        person={person}
                        config={config}
                        key={"person-" + person.id()}
                    />,
                );
                if (primaryYear === 0) {
                    primaryYear = person.getStartYear();
                }
            }
        }
        const headers = [];
        for (const header of HolidayTableColumns) {
            let tooltip: ?string;
            const date = holidays.getDateFor(header.key);
            if (date !== null) {
                const holiday = holidays.getHolidayOnDate(
                    date.year,
                    date.month,
                    date.day,
                );
                if (holiday !== null) {
                    tooltip =
                        dayNamer(holiday.dow) +
                        " " +
                        date.day +
                        "/" +
                        date.month +
                        "/" +
                        date.year;
                }
            }
            headers.push(
                <SortableTableHeader
                    key={header.key + "header"}
                    sortedBy={sortKey}
                    sortDir={direction}
                    entryKey={header.key}
                    onChangeSort={() => onChangeSort(header.key)}
                    config={config}
                    neverHide
                    visible
                    toggleColumnVisibility={() => {}}
                    shortTitle={header.shortTitle}
                    tooltipLiteral={tooltip}
                >
                    {header.title}
                </SortableTableHeader>,
            );
        }
        if (ignoredCounter > 0) {
            if (includeHidden) {
                ignoredHeader.push(
                    <div key="hiddenPeople" className="mb-2">
                        Tabellen inkluderer {ignoredCounter}{" "}
                        {ignoredCounter > 1
                            ? "skjulte personar"
                            : "skjult person"}
                        .{" "}
                        <span
                            className="likeLink"
                            onClick={() => {
                                this.setState({ includeHidden: false });
                            }}
                        >
                            Gøym skjulte i tabellen.
                        </span>
                    </div>,
                );
            } else {
                ignoredHeader.push(
                    <div key="hiddenPeople" className="mb-2">
                        Du har skjult {ignoredCounter}{" "}
                        {ignoredCounter > 1 ? "personar" : "person"}.{" "}
                        <span
                            className="likeLink"
                            onClick={() => {
                                this.setState({ includeHidden: true });
                            }}
                        >
                            Inkluder skjulte i tabellen.
                        </span>
                    </div>,
                );
            }
        }
        return [
            <Button
                className="ml-2 mb-2"
                disabled={analyzing === true}
                key="csvButton"
                color="secondary"
                size="sm"
                onClick={() =>
                    downloadStringToFile(
                        holidaysToCSV(people),
                        "Heilagdagsoversikt-" + primaryYear + ".csv",
                    )
                }
            >
                Eksporter til Excel
            </Button>,
            <Button
                className="ml-2 mb-2"
                disabled={analyzing === true}
                key="metaButton"
                color="secondary"
                size="sm"
                onClick={() => this.setState({ metaWindowOpen: true })}
            >
                Vis heilagdagsliste
            </Button>,
            metaWindow,
            ignoredHeader,
            <Table
                size="sm"
                hover
                className="force-default-cursor turnuslint-table"
                key="main-table-view"
            >
                <thead>
                    <tr>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            entryKey="NAME"
                            onChangeSort={() => onChangeSort("NAME")}
                            config={config}
                            neverHide
                            visible
                            toggleColumnVisibility={() => {}}
                        >
                            Namn
                        </SortableTableHeader>
                        {headers}
                    </tr>
                </thead>
                <tbody>{rows}</tbody>
            </Table>,
        ];
    }
}

export { HolidayTableRenderer, HolidayMetaWindow };

/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import * as React from "react";
import { uniqueid } from "./utils.js";
import { XCircle } from "react-bootstrap-icons";
import type { StatisticTypes, StatisticsEntry } from "./TurnusLint.js";
import type { PossibleTooltips } from "./Help.js";
import { ShiftAnalysisContainer } from "./TurnusLint.js";
import { GetTooltipStringFor } from "./Help";
import { TurnusLintConfig } from "./Config";
import { UncontrolledTooltip, Table } from "reactstrap";

/**
 * The list of valid table columns.
 */
export type TableColumns =
    | StatisticTypes
    | "NAME"
    | "WARNINGS"
    | "NOTES"
    | "IMPORTANT";

/**
 * Props for TableEntryRenderer.
 */
type TableEntryRendererProps = {|
    statistic: StatisticsEntry,
    onlyWideScreens?: boolean,
    onlyVeryWideScreens?: boolean,
    neverHide?: boolean,
    visible: boolean,
    config: TurnusLintConfig,
|};
/**
 * State for TableEntryRenderer.
 */
type TableEntryRendererState = {|
    uniqueid: number,
|};
/**
 * Renders a single column of the table view.
 */
class TableEntryRenderer extends React.PureComponent<
    TableEntryRendererProps,
    TableEntryRendererState,
> {
    state: TableEntryRendererState = {
        uniqueid: uniqueid(),
    };
    render(): React.Node {
        const {
            statistic,
            onlyWideScreens,
            onlyVeryWideScreens,
            visible,
            config,
        } = this.props;
        const hiddenColumnsCounter =
            config.hiddenTableColumnsCountIncludingSystem();
        let className;
        if (visible === false) {
            className = "d-none";
        } else if (onlyVeryWideScreens && hiddenColumnsCounter < 5) {
            className = "d-none d-xxl-table-cell";
        } else if (onlyWideScreens && hiddenColumnsCounter < 3) {
            className = "d-none d-xl-table-cell";
        }
        let value = statistic.value;
        let tooltip;
        if (
            statistic.type === "RED_DAYS" ||
            statistic.type === "SPECIAL_DAYS"
        ) {
            let days = statistic.secondaryValue;
            if (days === "" || days === null || days === undefined) {
                days = "0";
            }
            tooltip = value;
            value = days;
        } else if (
            statistic.type === "NIGHTS" ||
            statistic.type === "QUICK_RETURNS" ||
            statistic.type === "QUICK_RETURNS_IN_WEEKEND" ||
            statistic.type === "WEEKS_WITH_ALL_SHIFTS" ||
            statistic.type === "A_IN_A_ROW" ||
            statistic.type === "A_BEFORE_F_TIMES" ||
            statistic.type === "STANDALONE_F_TIMES" ||
            statistic.type === "LONELY_SHIFTS" ||
            statistic.type === "LONG_WEEKENDS" ||
            statistic.type === "SHORT_WORK_WEEKENDS" ||
            statistic.type === "TIME_REDUCTION"
        ) {
            tooltip = statistic.secondaryValue;
        }
        if (tooltip === undefined || tooltip === null || tooltip === "") {
            return <td className={className}>{value}</td>;
        }
        return (
            <td id={"tertooltip-" + this.state.uniqueid} className={className}>
                {value}
                <UncontrolledTooltip
                    target={"tertooltip-" + this.state.uniqueid}
                    delay={{ show: 500, hide: 250 }}
                    placement="top-start"
                >
                    {tooltip}
                </UncontrolledTooltip>
            </td>
        );
    }
}
/**
 * Props for TableRowRenderer.
 */
type TableRowRendererProps = {|
    person: ShiftAnalysisContainer,
    onSearchAndSwitchToReport: (string) => void,
    config: TurnusLintConfig,
|};
/**
 * Renders the complete table row for a single person.
 */
class TableRowRenderer extends React.Component<TableRowRendererProps> {
    render() {
        const { person, onSearchAndSwitchToReport, config } = this.props;
        const onClickHandler = () => onSearchAndSwitchToReport(person.name());
        return (
            <tr onClick={onClickHandler}>
                <td>{person.name()}</td>
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("QUICK_RETURNS", "0")}
                    visible={config.shouldDisplayTableColumn("QUICK_RETURNS")}
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic(
                        "QUICK_RETURNS_IN_WEEKEND",
                        "0",
                    )}
                    visible={config.shouldDisplayTableColumn(
                        "QUICK_RETURNS_IN_WEEKEND",
                    )}
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("WEEKS_WITH_ALL_SHIFTS", "0")}
                    visible={config.shouldDisplayTableColumn(
                        "WEEKS_WITH_ALL_SHIFTS",
                    )}
                    onlyVeryWideScreens
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("D_SHIFTS", "0")}
                    visible={config.shouldDisplayTableColumn("D_SHIFTS")}
                    onlyWideScreens
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("A_SHIFTS", "0")}
                    visible={config.shouldDisplayTableColumn("A_SHIFTS")}
                    onlyWideScreens
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("NIGHTS", "0")}
                    visible={config.shouldDisplayTableColumn("NIGHTS")}
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("NIGHT_SHIFT_WEEKENDS", "0")}
                    visible={config.shouldDisplayTableColumn(
                        "NIGHT_SHIFT_WEEKENDS",
                    )}
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("MAX_NIGHTS_IN_A_ROW", "0")}
                    visible={config.shouldDisplayTableColumn(
                        "MAX_NIGHTS_IN_A_ROW",
                    )}
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("SHIFTS_IN_A_ROW", "0")}
                    visible={config.shouldDisplayTableColumn("SHIFTS_IN_A_ROW")}
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("A_IN_A_ROW", "0")}
                    visible={config.shouldDisplayTableColumn("A_IN_A_ROW")}
                    onlyVeryWideScreens
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("A_BEFORE_F_TIMES", "0")}
                    visible={config.shouldDisplayTableColumn(
                        "A_BEFORE_F_TIMES",
                    )}
                    onlyVeryWideScreens
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("STANDALONE_F_TIMES", "0")}
                    visible={config.shouldDisplayTableColumn(
                        "STANDALONE_F_TIMES",
                    )}
                    onlyVeryWideScreens
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("LONELY_SHIFTS", "0")}
                    visible={config.shouldDisplayTableColumn("LONELY_SHIFTS")}
                    onlyVeryWideScreens
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("WEEKS_OFF", "0")}
                    visible={config.shouldDisplayTableColumn("WEEKS_OFF")}
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("RED_DAYS", "0")}
                    visible={config.shouldDisplayTableColumn("RED_DAYS")}
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("SPECIAL_DAYS", "0")}
                    visible={config.shouldDisplayTableColumn("SPECIAL_DAYS")}
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("LONG_WEEKENDS", "0")}
                    visible={config.shouldDisplayTableColumn("LONG_WEEKENDS")}
                    onlyWideScreens
                />
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("SHORT_WORK_WEEKENDS", "0")}
                    visible={config.shouldDisplayTableColumn(
                        "SHORT_WORK_WEEKENDS",
                    )}
                    onlyWideScreens
                />
                <td
                    className={
                        config.shouldDisplayTableColumn("WARNINGS")
                            ? ""
                            : "d-none"
                    }
                >
                    {person.warnings().length}
                </td>
                <td
                    className={
                        config.shouldDisplayTableColumn("NOTES") ? "" : "d-none"
                    }
                >
                    {person.notes().length}
                </td>
                <td
                    className={
                        config.shouldDisplayTableColumn("IMPORTANT")
                            ? config.hiddenTableColumnsCountIncludingSystem() <
                              3
                                ? "d-none d-xl-table-cell"
                                : ""
                            : "d-none"
                    }
                >
                    {person.important().length}
                </td>
                <TableEntryRenderer
                    config={config}
                    statistic={person.statistic("TIME_REDUCTION", "0")}
                    visible={config.shouldDisplayTableColumn("TIME_REDUCTION")}
                    onlyVeryWideScreens
                />
            </tr>
        );
    }
}

/**
 * Callback to hide table columns.
 */
export type ToggleTableColumnsCB = (TableColumns) => void;

/**
 * The columns that can be numerically sorted.
 */
type NumericSortItems = StatisticTypes | "WARNINGS" | "NOTES" | "IMPORTANT";
/**
 * The keys that can be sorted on.
 */
type SortKey = TableColumns | null;
/**
 * The directions columns can be sorted in.
 */
export type SortDirection = "desc" | "asc";
/**
 * Props for SortableTableHeader.
 */
type SortableTableHeaderProps = {|
    children: string,
    entryKey: string,
    sortedBy: string | null,
    sortDir: SortDirection,
    toggleColumnVisibility: () => void,
    tooltipKey?: PossibleTooltips,
    tooltipLiteral?: ?string,
    onChangeSort: () => void,
    onlyWideScreens?: boolean,
    onlyVeryWideScreens?: boolean,
    shortTitle?: string,
    neverHide?: boolean,
    visible?: boolean,
    config: TurnusLintConfig,
|};
/**
 * A single table header column that can be sorted.
 */
class SortableTableHeader extends React.PureComponent<SortableTableHeaderProps> {
    render(): React.Node {
        const {
            children,
            entryKey,
            sortedBy,
            sortDir,
            onChangeSort,
            onlyWideScreens,
            onlyVeryWideScreens,
            neverHide,
            tooltipKey,
            tooltipLiteral,
            visible,
            toggleColumnVisibility,
            shortTitle,
            config,
        } = this.props;
        let renderChild = children;
        const hiddenColumnsCounter =
            config.hiddenTableColumnsCountIncludingSystem();
        const className = ["no-select"];
        if (visible === false && neverHide !== true) {
            className.push("d-none");
        } else if (onlyVeryWideScreens && hiddenColumnsCounter < 7) {
            className.push("d-none");
            className.push("d-xxl-table-cell");
        } else if (onlyWideScreens && hiddenColumnsCounter < 3) {
            className.push("d-none");
            className.push("d-xl-table-cell");
        }
        const sortString = sortDir === "desc" ? "▼" : "▲";
        let id = null;
        let tooltipElement = null;
        if (entryKey !== null) {
            id = "header" + entryKey;
        }
        let tooltipString: ?string;
        if (tooltipKey !== undefined) {
            tooltipString = GetTooltipStringFor(tooltipKey);
        } else if (tooltipLiteral !== undefined) {
            tooltipString = tooltipLiteral;
        }
        if (tooltipString !== undefined) {
            tooltipElement = (
                <UncontrolledTooltip
                    placement="top"
                    delay={{ show: 500, hide: 250 }}
                    target={id}
                >
                    {tooltipString}
                </UncontrolledTooltip>
            );
        }
        if (shortTitle !== undefined && shortTitle !== "") {
            renderChild = [
                <span key="xxl" className="d-none d-xxl-inline">
                    {children}
                </span>,
                <span key="normal" className="d-xxl-none">
                    {shortTitle}
                </span>,
            ];
        }
        return (
            <th onClick={onChangeSort} className={className.join(" ")} id={id}>
                <div className="tableColumnPositioningHack">
                    <span
                        className={
                            neverHide === true ? "d-none" : "hideColumnButton"
                        }
                        onClick={(ev) => {
                            ev.stopPropagation();
                            toggleColumnVisibility();
                        }}
                    >
                        <XCircle />
                    </span>
                    <span className="textContent">
                        {renderChild}
                        <span
                            className={
                                sortedBy === entryKey ? "visible" : "invisible"
                            }
                        >
                            {sortString}
                        </span>
                    </span>
                    {tooltipElement}
                </div>
            </th>
        );
    }
}

/**
 * Filters an array of ShiftAnalysisContainer.
 */
function filterPeople(
    people: Array<ShiftAnalysisContainer>,
    search: ?string,
): Array<ShiftAnalysisContainer> {
    let filteredPeople = people;
    if (
        search !== null &&
        search !== undefined &&
        search !== "" &&
        /\S/.test(search)
    ) {
        const searchStrings = search.toLowerCase().split(/\s+/);
        filteredPeople = people.reduce((previousValue, currentValue) => {
            let match = 0;
            const name = currentValue.name().toLowerCase();
            for (const str of searchStrings) {
                if (name.includes(str)) {
                    match++;
                }
            }
            if (match === searchStrings.length) {
                previousValue.push(currentValue);
            }
            return previousValue;
        }, []);
    }
    return filteredPeople;
}

/**
 * Filters and sorts an array of ShiftAnalysisContainer.
 */
function sortPeople(
    people: Array<ShiftAnalysisContainer>,
    direction: SortDirection,
    sortKey: SortKey,
    sorter: (ShiftAnalysisContainer, NumericSortItems) => number,
): Array<ShiftAnalysisContainer> {
    if (sortKey !== null) {
        // Make a copy of the array to sort
        let filteredPeople = [...people];
        if (sortKey === "NAME") {
            if (direction === "asc") {
                filteredPeople = filteredPeople.sort((a, b) =>
                    a.name().localeCompare(b.name()),
                );
            } else {
                filteredPeople = filteredPeople.sort((a, b) =>
                    b.name().localeCompare(a.name()),
                );
            }
        } else {
            if (direction === "asc") {
                filteredPeople = filteredPeople.sort(
                    (a, b) => sorter(a, sortKey) - sorter(b, sortKey),
                );
            } else {
                filteredPeople = filteredPeople.sort(
                    (a, b) => sorter(b, sortKey) - sorter(a, sortKey),
                );
            }
        }
        return filteredPeople;
    }
    return people;
}

/**
 * Props for TableRenderer.
 */
type TableRendererProps = {|
    people: Array<ShiftAnalysisContainer>,
    onSearchAndSwitchToReport: (string) => void,
    search: string,
    config: TurnusLintConfig,
|};
/**
 * State for TableRenderer.
 */
type TableRendererState = {|
    direction: SortDirection,
    sortKey: SortKey,
    includeHidden: boolean,
|};
/**
 * Retrieves a numeric value from a ShiftAnalysisContainer based upon the key
 * provided.
 */
function getSortableValue(
    from: ShiftAnalysisContainer,
    key: NumericSortItems,
): number {
    let retValue: number = 0;
    if (key === "WARNINGS") {
        retValue = from.warnings().length;
    } else if (key === "NOTES") {
        retValue = from.notes().length;
    } else if (key === "IMPORTANT") {
        retValue = from.important().length;
    } else if (key === "RED_DAYS" || key === "SPECIAL_DAYS") {
        retValue = parseInt(from.statistic(key, "0").secondaryValue);
    } else if (key === "TIME_REDUCTION") {
        const statistic = from.statistic(key, "0").tietaryValue;
        if (typeof statistic === "number") {
            retValue = statistic;
        }
    } else {
        retValue = parseInt(from.statistic(key, "0").value);
    }
    if (isNaN(retValue)) {
        retValue = 0;
    }
    return retValue;
}
/**
 * Renders a complete table for all people provided.
 */
class TableRenderer extends React.Component<
    TableRendererProps,
    TableRendererState,
> {
    state: TableRendererState = {
        direction: "desc",
        sortKey: null,
        includeHidden: false,
    };

    render(): React.Node {
        const { people, search, onSearchAndSwitchToReport, config } =
            this.props;
        const { direction, sortKey, includeHidden } = this.state;
        const toggleColumnVisibility: ToggleTableColumnsCB = (
            column: TableColumns,
        ) => {
            config.setShouldDisplayTableColumn(column, false);
            this.forceUpdate();
        };
        let ignoredCounter: number = 0;
        const filteredPeople = sortPeople(
            filterPeople(people, search),
            direction,
            sortKey,
            getSortableValue,
        );
        const rows = [];
        for (const person of filteredPeople) {
            if (person.ignored() === true) {
                ignoredCounter++;
            }
            if (person.ignored() !== true || includeHidden) {
                rows.push(
                    <TableRowRenderer
                        onSearchAndSwitchToReport={onSearchAndSwitchToReport}
                        person={person}
                        config={config}
                        key={"person-" + person.id()}
                    />,
                );
            }
        }
        const ignoredHeader: Array<React.Node> = [];
        const hiddenColumnsCounter = config.hiddenTableColumnsCount();
        if (hiddenColumnsCounter > 0) {
            ignoredHeader.push(
                <div key="hidden" className="mb-2">
                    Du har skjult {hiddenColumnsCounter}{" "}
                    {hiddenColumnsCounter === 1 ? "kolonne" : "kolonner"} i
                    tabellen.{" "}
                    <span
                        className="likeLink"
                        onClick={() => {
                            config.resetHiddenTableColumns();
                            this.forceUpdate();
                        }}
                    >
                        Vis alle kolonnene igjen.
                    </span>
                </div>,
            );
        }
        if (ignoredCounter > 0) {
            if (includeHidden) {
                ignoredHeader.push(
                    <div key="hiddenPeople" className="mb-2">
                        Tabellen inkluderer {ignoredCounter}{" "}
                        {ignoredCounter > 1
                            ? "skjulte personar"
                            : "skjult person"}
                        .{" "}
                        <span
                            className="likeLink"
                            onClick={() => {
                                this.setState({ includeHidden: false });
                            }}
                        >
                            Gøym skjulte i tabellen.
                        </span>
                    </div>,
                );
            } else {
                ignoredHeader.push(
                    <div key="hiddenPeople" className="mb-2">
                        Du har skjult {ignoredCounter}{" "}
                        {ignoredCounter > 1 ? "personar" : "person"}.{" "}
                        <span
                            className="likeLink"
                            onClick={() => {
                                this.setState({ includeHidden: true });
                            }}
                        >
                            Inkluder skjulte i tabellen.
                        </span>
                    </div>,
                );
            }
        }
        const onChangeSort = (key: SortKey) => {
            let newDir = direction;
            if (key === sortKey) {
                newDir = direction === "desc" ? "asc" : "desc";
            } else {
                newDir = "desc";
            }
            this.setState({
                sortKey: key,
                direction: newDir,
            });
        };
        return [
            ignoredHeader,
            <Table
                size="sm"
                hover
                className="force-default-cursor turnuslint-table"
                key="main-table-view"
            >
                <thead>
                    <tr>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="NAME"
                            onChangeSort={() => onChangeSort("NAME")}
                            neverHide
                            visible
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("NAME")
                            }
                        >
                            Namn
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="QUICK_RETURNS"
                            visible={config.shouldDisplayTableColumn(
                                "QUICK_RETURNS",
                            )}
                            onChangeSort={() => onChangeSort("QUICK_RETURNS")}
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("QUICK_RETURNS")
                            }
                            tooltipKey="QUICK_RETURNS"
                        >
                            A/D
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="QUICK_RETURNS_IN_WEEKEND"
                            visible={config.shouldDisplayTableColumn(
                                "QUICK_RETURNS_IN_WEEKEND",
                            )}
                            onChangeSort={() =>
                                onChangeSort("QUICK_RETURNS_IN_WEEKEND")
                            }
                            tooltipKey="QUICK_RETURNS_IN_WEEKEND"
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility(
                                    "QUICK_RETURNS_IN_WEEKEND",
                                )
                            }
                        >
                            A/D i helg
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="WEEKS_WITH_ALL_SHIFTS"
                            visible={config.shouldDisplayTableColumn(
                                "WEEKS_WITH_ALL_SHIFTS",
                            )}
                            onChangeSort={() =>
                                onChangeSort("WEEKS_WITH_ALL_SHIFTS")
                            }
                            tooltipKey="WEEKS_WITH_ALL_SHIFTS"
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("WEEKS_WITH_ALL_SHIFTS")
                            }
                            onlyVeryWideScreens
                        >
                            Veker med D+A+N
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="D_SHIFTS"
                            visible={config.shouldDisplayTableColumn(
                                "D_SHIFTS",
                            )}
                            onChangeSort={() => onChangeSort("D_SHIFTS")}
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("D_SHIFTS")
                            }
                            shortTitle="D"
                            onlyWideScreens
                        >
                            D-vakter
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="A_SHIFTS"
                            visible={config.shouldDisplayTableColumn(
                                "A_SHIFTS",
                            )}
                            onChangeSort={() => onChangeSort("A_SHIFTS")}
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("A_SHIFTS")
                            }
                            shortTitle="A"
                            onlyWideScreens
                        >
                            A-vakter
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="NIGHTS"
                            visible={config.shouldDisplayTableColumn("NIGHTS")}
                            onChangeSort={() => onChangeSort("NIGHTS")}
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("NIGHTS")
                            }
                            shortTitle="N"
                        >
                            Netter
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="NIGHT_SHIFT_WEEKENDS"
                            onChangeSort={() =>
                                onChangeSort("NIGHT_SHIFT_WEEKENDS")
                            }
                            neverHide
                            visible
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("NIGHT_SHIFT_WEEKENDS")
                            }
                        >
                            Nattevakts&shy;helger
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="MAX_NIGHTS_IN_A_ROW"
                            visible={config.shouldDisplayTableColumn(
                                "MAX_NIGHTS_IN_A_ROW",
                            )}
                            tooltipKey="MAX_NIGHTS_IN_A_ROW"
                            onChangeSort={() =>
                                onChangeSort("MAX_NIGHTS_IN_A_ROW")
                            }
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("MAX_NIGHTS_IN_A_ROW")
                            }
                            shortTitle="N på rad"
                        >
                            Netter på rad
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="SHIFTS_IN_A_ROW"
                            visible={config.shouldDisplayTableColumn(
                                "SHIFTS_IN_A_ROW",
                            )}
                            tooltipKey="SHIFTS_IN_A_ROW"
                            onChangeSort={() => onChangeSort("SHIFTS_IN_A_ROW")}
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("SHIFTS_IN_A_ROW")
                            }
                        >
                            Vaktdøgn på rad
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="A_IN_A_ROW"
                            visible={config.shouldDisplayTableColumn(
                                "A_IN_A_ROW",
                            )}
                            tooltipKey="A_IN_A_ROW"
                            onChangeSort={() => onChangeSort("A_IN_A_ROW")}
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("A_IN_A_ROW")
                            }
                            onlyVeryWideScreens
                        >
                            Maks A på rad
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="A_BEFORE_F_TIMES"
                            visible={config.shouldDisplayTableColumn(
                                "A_BEFORE_F_TIMES",
                            )}
                            tooltipKey="A_BEFORE_F_TIMES"
                            onChangeSort={() =>
                                onChangeSort("A_BEFORE_F_TIMES")
                            }
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("A_BEFORE_F_TIMES")
                            }
                            onlyVeryWideScreens
                        >
                            Ganger med A før fri
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="STANDALONE_F_TIMES"
                            visible={config.shouldDisplayTableColumn(
                                "STANDALONE_F_TIMES",
                            )}
                            onChangeSort={() =>
                                onChangeSort("STANDALONE_F_TIMES")
                            }
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("STANDALONE_F_TIMES")
                            }
                            onlyVeryWideScreens
                        >
                            Enkle fridagar
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="LONELY_SHIFTS"
                            visible={config.shouldDisplayTableColumn(
                                "LONELY_SHIFTS",
                            )}
                            onChangeSort={() => onChangeSort("LONELY_SHIFTS")}
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("LONELY_SHIFTS")
                            }
                            onlyVeryWideScreens
                        >
                            Enkle vakter
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="WEEKS_OFF"
                            visible={config.shouldDisplayTableColumn(
                                "WEEKS_OFF",
                            )}
                            onChangeSort={() => onChangeSort("WEEKS_OFF")}
                            tooltipKey="WEEKS_OFF"
                            shortTitle="Veker fri"
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("WEEKS_OFF")
                            }
                        >
                            Veker med fri
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="RED_DAYS"
                            visible={config.shouldDisplayTableColumn(
                                "RED_DAYS",
                            )}
                            onChangeSort={() => onChangeSort("RED_DAYS")}
                            tooltipKey="RED_DAYS"
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("RED_DAYS")
                            }
                        >
                            Hellig&shy;dagar
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="SPECIAL_DAYS"
                            visible={config.shouldDisplayTableColumn(
                                "SPECIAL_DAYS",
                            )}
                            onChangeSort={() => onChangeSort("SPECIAL_DAYS")}
                            tooltipKey="SPECIAL_DAYS"
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("SPECIAL_DAYS")
                            }
                        >
                            Merke&shy;dagar
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="LONG_WEEKENDS"
                            visible={config.shouldDisplayTableColumn(
                                "LONG_WEEKENDS",
                            )}
                            onChangeSort={() => onChangeSort("LONG_WEEKENDS")}
                            onlyWideScreens
                            tooltipKey="LONG_WEEKENDS"
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("LONG_WEEKENDS")
                            }
                        >
                            Lange frihelger
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="SHORT_WORK_WEEKENDS"
                            visible={config.shouldDisplayTableColumn(
                                "SHORT_WORK_WEEKENDS",
                            )}
                            onChangeSort={() =>
                                onChangeSort("SHORT_WORK_WEEKENDS")
                            }
                            onlyWideScreens
                            tooltipKey="SHORT_WORK_WEEKENDS"
                            shortTitle="Korte arb.helg."
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("SHORT_WORK_WEEKENDS")
                            }
                        >
                            Korte arbeidshelger
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="WARNINGS"
                            visible={config.shouldDisplayTableColumn(
                                "WARNINGS",
                            )}
                            onChangeSort={() => onChangeSort("WARNINGS")}
                            tooltipKey="WARNINGS"
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("WARNINGS")
                            }
                        >
                            Advarslar
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="NOTES"
                            visible={config.shouldDisplayTableColumn("NOTES")}
                            tooltipKey="NOTES"
                            onChangeSort={() => onChangeSort("NOTES")}
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("NOTES")
                            }
                            shortTitle="Merkn."
                        >
                            Merknadar
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="IMPORTANT"
                            visible={config.shouldDisplayTableColumn(
                                "IMPORTANT",
                            )}
                            tooltipKey="IMPORTANT"
                            onChangeSort={() => onChangeSort("IMPORTANT")}
                            onlyWideScreens
                            shortTitle="Markert viktig"
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("IMPORTANT")
                            }
                        >
                            Markert som viktig
                        </SortableTableHeader>
                        <SortableTableHeader
                            sortedBy={sortKey}
                            sortDir={direction}
                            config={config}
                            entryKey="TIME_REDUCTION"
                            visible={config.shouldDisplayTableColumn(
                                "TIME_REDUCTION",
                            )}
                            onChangeSort={() => onChangeSort("TIME_REDUCTION")}
                            onlyVeryWideScreens
                            shortTitle="Omregna tid"
                            toggleColumnVisibility={() =>
                                toggleColumnVisibility("TIME_REDUCTION")
                            }
                        >
                            Bruk omregna tid
                        </SortableTableHeader>
                    </tr>
                </thead>
                <tbody>{rows}</tbody>
            </Table>,
        ];
    }
}

export { TableRenderer, SortableTableHeader, filterPeople };
// Test exports
export { TableEntryRenderer };

/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */

declare var USE_LIMITED_RULESET: boolean;

import type { TableColumns } from "./TableRenderer.js";

import { getMany, setMany } from "idb-keyval";

const helseVestDisable: {
    [string]: boolean,
} = {
    A_BEFORE_F_TIMES: true,
    STANDALONE_F_TIMES: true,
    SINGLE_SHIFT_CODE_WEEKS: true,
};

/**
 * Valid values for display mode.
 */
type DisplayMode = "report" | "table" | "holidays" | "shiftCount";
/**
 * String config structure.
 */
type StringConfigType = {|
    displayMode: DisplayMode,
|};
/**
 * Numeric config structure.
 */
type NumericConfigType = {|
    minDaysBetweenAD: number,
    maxConsecutiveNights: number,
    maxConsecutiveNightsNightWorker: number,
    maxConsecutiveAShifts: number,
|};
/**
 * Boolean config structure.
 */
type BooleanConfigType = {|
    enableInferredYearlyWorkWeekendcount: boolean,
    enableMaxConsecutiveASHifts: boolean,
    enableSingularAChecks: boolean,
|};
type DisabledTableColumnsMap = {|
    [TableColumns]: boolean,
|};
/**
 * An alias to make values expecting names more intuitive.
 */
type IndividualName = string;
/**
 * The key for a single warning or notice. This is a string and should uniquely
 * identify a single warning or notice, of a single type, on a given date. It
 * should not include information about the person. Generally the  @{link
 * ShiftNoticeSubtypes} or @{link StatisticTypes} combined with a
 * date is sufficient.
 */
type AnalyzationEntryKey = string;
/**
 * A map of AnalyzationEntryKey's to boolean values: describes if we should
 * ignore (true) or not (false) an AnalyzationEntryKey.
 */
type AnalyzationEntryMap = {|
    [AnalyzationEntryKey]: boolean,
|};
/**
 * A map of individuals to their AnalyzationEntryMap.
 */
type StringMapConfigType = {|
    [IndividualName]: AnalyzationEntryMap,
|};
/**
 * A map of the ignore state for individual people.
 */
type IgnoredPeopleMap = {|
    [IndividualName]: boolean,
|};
/**
 * The combined type of all of the map types.
 */
type EntryMaps = {|
    markedImportant: StringMapConfigType,
    ignored: StringMapConfigType,
    peopleIgnored: IgnoredPeopleMap,
    disabledTableColumns: DisabledTableColumnsMap,
|};
/**
 * The EntryMaps keys that are AnalyzationEntryMaps.
 */
type AnalyzationEntryMapKeys = "markedImportant" | "ignored";
/**
 * The EntryMaps keys that are IgnoredPeopleMaps.
 */
type PeopleEntryMapKeys = "peopleIgnored";
/**
 * The EntryMaps keys that are DisabledTableColumnsMap
 */
type DisabledTableColumnsEntryMapKeys = "disabledTableColumns";
/**
 * The combined and final configuration type object.
 */
type ConfigType = {|
    ...NumericConfigType,
    ...BooleanConfigType,
    ...StringConfigType,
|};

const defaultConfig: ConfigType = {
    minDaysBetweenAD: 3,
    maxConsecutiveNights: 3,
    maxConsecutiveNightsNightWorker: 5,
    maxConsecutiveAShifts: 3,
    enableMaxConsecutiveASHifts: false,
    enableInferredYearlyWorkWeekendcount: true,
    enableSingularAChecks: false,
    displayMode: "report",
};

const defaultMaps: EntryMaps = {
    markedImportant: {},
    ignored: {},
    peopleIgnored: {},
    disabledTableColumns: {},
};

/**
 * A list of the config parameters that are booleans.
 */
type BooleanConfigParameters = $Keys<BooleanConfigType>;
/**
 * A list of the config parameters that are numeric.
 */
type NumericConfigParameters = $Keys<NumericConfigType>;
/**
 * A list of the config parameters that are strings.
 */
type StringConfigParameters = $Keys<StringConfigType>;
/**
 * A value type that matches all possible configuration options.
 */
type AbstractConfigOptionValueType = number | boolean | DisplayMode;
/**
 * A list of all config options.
 */
type ConfigParameters = $Keys<ConfigType>;
/**
 * The events that the config object may emit.
 */
type TurnusLintConfigEvent = "OPTION_CHANGED" | "CONFIG_LOADED";
/**
 * The callback type for listeners.
 */
type ListenerCB = (
    event: TurnusLintConfigEvent,
    option:
        | ConfigParameters
        | AnalyzationEntryMapKeys
        | PeopleEntryMapKeys
        | DisabledTableColumnsEntryMapKeys
        | null,
    value: AbstractConfigOptionValueType | null,
    previousValue: AbstractConfigOptionValueType | null,
) => void;

/**
 * A validator of numeric values.
 */
type NumericConfigValidator = {|
    min: number,
    max: number,
|};

/**
 * A map of config values -> validators.
 */
export type NumericConfigValidatorList = {|
    [ConfigParameters]: NumericConfigValidator,
|};

/**
 * TurnusLint persistent configuration.
 *
 * Whenever you set an option, it will notify subscribers and store the changed
 * config *if* the new value differs from the old value.
 */
class TurnusLintConfig {
    _loaded: boolean = false;
    _config: ConfigType = { ...defaultConfig };
    _lastChanged: number = -1;
    _entryMaps: EntryMaps = {
        ...defaultMaps,
    };
    _listeners: Array<ListenerCB | null> = [];

    constructor() {
        getMany(["tl.config", "tl.entrymaps"])
            .then(([config, maps]) => {
                if (typeof config === "object") {
                    this._config = Object.assign({}, defaultConfig, config);
                }
                if (typeof maps === "object") {
                    this._entryMaps = Object.assign({}, defaultMaps, maps);
                }
                this._hasChanged("CONFIG_LOADED");
            })
            .catch(() => {
                this._hasChanged("CONFIG_LOADED");
            });
    }

    /**
     * Check if we have loaded the config from storage. Returns true if we have loaded.
     * */
    hasLoaded(): boolean {
        return this._loaded;
    }

    /**
     * Retrieves a numeric option from the config.
     */
    getNumericOption(option: ConfigParameters): number {
        if (typeof this._config[option] !== "number") {
            throw (
                "Attempt to .getNumericOption for non-numeric option: " + option
            );
        }
        return this._config[option];
    }

    /**
     * Retrieves a boolean option from the config.
     */
    getBooleanOption(option: BooleanConfigParameters): boolean {
        return this._config[option] === true;
    }

    /**
     * Sets a boolean option.
     */
    setBooleanOption(option: BooleanConfigParameters, value: boolean): boolean {
        const prevValue = this._config[option];
        this._config[option] = value;
        this._hasChanged("OPTION_CHANGED", option, value, prevValue);
        return !value;
    }

    /**
     * Retrieves a string option.
     */
    getStringOption(option: StringConfigParameters): string {
        return this._config[option];
    }

    /**
     * Sets a string option.
     */
    setStringOption(
        option: StringConfigParameters,
        value: DisplayMode,
    ): string {
        const prevValue = this._config[option];
        this._config[option] = value;
        this._hasChanged("OPTION_CHANGED", option, value, prevValue);
        return prevValue;
    }

    /**
     * Set a person to be ignored.
     */
    setPersonIgnored(person: string, year: number, value: boolean): boolean {
        const prevValue = this.getPersonIgnored(person, year);
        this._entryMaps[person + "||" + year] = value;
        if (value !== prevValue) {
            this._hasChanged(
                "OPTION_CHANGED",
                "peopleIgnored",
                prevValue,
                value,
            );
        }
        return prevValue;
    }

    /**
     * Retrieves the current ignored status for a person.
     */
    getPersonIgnored(person: string, year: number): boolean {
        return !!this._entryMaps[person + "||" + year];
    }

    /**
     * Sets a numeric option.
     */
    setNumericOption(
        option: NumericConfigParameters,
        value: number | string,
    ): number {
        const prevValue = this._config[option];
        if (typeof value === "string") {
            value = Number.parseInt(value);
        }
        const validated = this.safeValidator(option, value);
        if (typeof validated !== "number") {
            throw "safeValidator returned non-number type: " + typeof validated;
        }
        value = validated;
        if (typeof this._config[option] !== "number") {
            throw (
                "Attempt to .setNumericOption for non-numeric option: " + option
            );
        }
        this._config[option] = value;
        this._hasChanged("OPTION_CHANGED", option, value, prevValue);
        return prevValue;
    }

    /**
     * Validates a config value if we have a validator for it.
     */
    safeValidator(
        option: ConfigParameters,
        value: AbstractConfigOptionValueType,
    ): AbstractConfigOptionValueType {
        const validator = this.getValidatorFor(option);
        if (
            typeof this._config[option] === "number" &&
            validator !== null &&
            typeof value === "number"
        ) {
            if (value > validator.max) {
                value = validator.max;
            } else if (value < validator.min) {
                value = validator.min;
            }
        }
        return value;
    }

    /**
     * Retrieves the validator object for a configuration option, if there is one.
     */
    getValidatorFor(option: ConfigParameters): NumericConfigValidator | null {
        if (typeof this._config[option] === "number") {
            return this.getNumericValidatorFor(option);
        }
        return null;
    }

    /**
     * Retrieves the numeric validator object for a configuration option. If
     * there isn't one, a dummy one will be returned.
     */
    getNumericValidatorFor(option: ConfigParameters): NumericConfigValidator {
        const validatorsNumeric: NumericConfigValidatorList = {
            minDaysBetweenAD: {
                min: 0,
                max: 7,
            },
            maxConsecutiveNights: {
                min: 1,
                max: 5,
            },
            maxConsecutiveNightsNightWorker: {
                min: 1,
                max: 6,
            },
            maxConsecutiveAShifts: {
                min: 1,
                max: 5,
            },
        };
        if (validatorsNumeric[option] !== undefined) {
            return validatorsNumeric[option];
        }
        return { min: 1, max: 1 };
    }

    /**
     * Subscribes the  `cb` to this object's events. Will be called whenever something changes.
     * Returns the subscriber number, which can be used to `.unlisten()` later.
     */
    listen(cb: ListenerCB): number {
        return this._listeners.push(cb) - 1;
    }

    /**
     * Unsubscribes the `id` from this object's events.
     */
    unlisten(id: number) {
        if (this._listeners[id] !== undefined && this._listeners[id] !== null) {
            this._listeners[id] = null;
        }
    }

    /**
     * Retrieves the "ignored" status for a given key belonging to a person.
     * See @{link AnalyzationEntryKey} for a description of the key.
     */
    getEntryIgnoredStatus(
        name: IndividualName,
        key: AnalyzationEntryKey,
    ): boolean {
        return this._getDeepMap("ignored", name, key);
    }
    /**
     * Set the "ignored" status for a given key belonging to a person.
     * See @{link AnalyzationEntryKey} for a description of the key.
     */
    setEntryIgnoredStatus(
        name: IndividualName,
        key: AnalyzationEntryKey,
        status: boolean,
    ): boolean {
        const oldState = this._setDeepMap("ignored", name, key, status);
        if (oldState !== status) {
            this._hasChanged("OPTION_CHANGED", "ignored", oldState, status);
        }
        return oldState;
    }

    /**
     * Get the "important" status for a given key beloning to a person.
     * See @{link AnalyzationEntryKey} for a description of the key.
     */
    getIsImportant(name: IndividualName, key: AnalyzationEntryKey): boolean {
        return this._getDeepMap("markedImportant", name, key);
    }
    /**
     * Set the "important" status for a given key beloning to a person.
     * See @{link AnalyzationEntryKey} for a description of the key.
     */
    setAsImportant(
        name: IndividualName,
        key: AnalyzationEntryKey,
        status: boolean,
    ): boolean {
        const oldState = this._setDeepMap("markedImportant", name, key, status);
        if (oldState !== status) {
            this._hasChanged(
                "OPTION_CHANGED",
                "markedImportant",
                oldState,
                !oldState,
            );
        }
        return oldState;
    }

    /**
     * Checks if the config has changed since a given timestamp. The timestamp
     * is in *ms*, ie. the default value from the Date object's getTime()
     * method.
     * */
    hasChangedSince(timestampInMiliseconds: number): boolean {
        return !(
            this._lastChanged === -1 ||
            this._lastChanged <= timestampInMiliseconds
        );
    }

    /**
     * Sets the state of a table column.
     */
    setShouldDisplayTableColumn(
        column: TableColumns,
        shouldDisplay: boolean,
    ): boolean {
        const prevValue = this._entryMaps.disabledTableColumns[column] !== true;
        this._entryMaps.disabledTableColumns[column] = !shouldDisplay;
        if (prevValue !== shouldDisplay) {
            this._hasChanged(
                "OPTION_CHANGED",
                "disabledTableColumns",
                prevValue,
                shouldDisplay,
            );
        }
        return prevValue;
    }

    /**
     * Get the state of a table column. Returns `true` if the column should be
     * displayed, `false` otherwise.
     */
    shouldDisplayTableColumn(column: TableColumns): boolean {
        // It is stored as a map of table columns to disable, so we return the
        // opposite of the value stored, defaulting to returning true if no
        // value exists.
        //
        // For Helse Vest builds we disable some columns by default
        if (USE_LIMITED_RULESET === true) {
            if (helseVestDisable[column] === true) {
                return false;
            }
        }
        return this._entryMaps.disabledTableColumns[column] !== true;
    }

    /**
     * Resets hidden table columns to none.
     */
    resetHiddenTableColumns(): void {
        if (Object.keys(this._entryMaps.disabledTableColumns).length > 0) {
            this._entryMaps.disabledTableColumns = {};
            this._hasChanged(
                "OPTION_CHANGED",
                "disabledTableColumns",
                true,
                false,
            );
        }
    }

    /**
     * Gets the number of hidden table columns.
     */
    hiddenTableColumnsCount(): number {
        let hidden = 0;
        for (const column of Object.keys(
            this._entryMaps.disabledTableColumns,
        )) {
            if (this._entryMaps.disabledTableColumns[column] === true) {
                hidden++;
            }
        }
        return hidden;
    }

    /**
     * Gets the number of hidden table columns.
     */
    hiddenTableColumnsCountIncludingSystem(): number {
        if (USE_LIMITED_RULESET === true) {
            return (
                this.hiddenTableColumnsCount() +
                Object.keys(helseVestDisable).length
            );
        } else {
            return this.hiddenTableColumnsCount();
        }
    }

    /**
     * Private helper that sets a value in _entryMaps, building parent objects as needed.
     */
    _setDeepMap(
        type: AnalyzationEntryMapKeys,
        name: IndividualName,
        key: AnalyzationEntryKey,
        value: boolean,
    ): boolean {
        if (typeof this._entryMaps[type] !== "object") {
            this._entryMaps[type] = {};
        }
        if (typeof this._entryMaps[type][name] !== "object") {
            this._entryMaps[type][name] = {};
        }
        const prev = this._getDeepMap(type, name, key);
        this._entryMaps[type][name][key] = value;
        return prev;
    }
    /**
     * Private helper that gets a value in _entryMaps, validating that a value
     * exists before returning. If no value exists then it defaults to `false`.
     */
    _getDeepMap(
        type: AnalyzationEntryMapKeys,
        name: IndividualName,
        key: AnalyzationEntryKey,
    ): boolean {
        if (
            typeof this._entryMaps[type][name] === "object" &&
            typeof this._entryMaps[type][name][key] === "boolean"
        ) {
            return this._entryMaps[type][name][key];
        }
        return false;
    }

    /**
     * Notifies listeners about a change in the config state. Saves the config if needed.
     */
    _hasChanged(
        event: TurnusLintConfigEvent,
        option:
            | ConfigParameters
            | AnalyzationEntryMapKeys
            | DisabledTableColumnsEntryMapKeys
            | PeopleEntryMapKeys
            | null = null,
        value: AbstractConfigOptionValueType | null = null,
        previousValue: AbstractConfigOptionValueType | null = null,
    ) {
        const emitEvent = () => {
            for (const cb of this._listeners) {
                if (cb !== null) {
                    try {
                        cb(event, option, value, previousValue);
                    } catch (_) {} // eslint-disable-line no-empty
                }
            }
        };
        if (event === "OPTION_CHANGED") {
            if (value !== previousValue) {
                emitEvent();
                // eslint-disable-next-line flowtype/no-weak-types
                const setter: Array<Array<any>> = [["tl.configVersion", 0]];
                if (
                    option === "markedImportant" ||
                    option === "ignored" ||
                    option === "peopleIgnored" ||
                    option === "disabledTableColumns"
                ) {
                    setter.push(["tl.entrymaps", this._entryMaps]);
                } else {
                    setter.push(["tl.config", this._config]);
                }
                setMany(setter);
                this._lastChanged = new Date().getTime();
            }
        } else if (event === "CONFIG_LOADED") {
            this._loaded = true;
            emitEvent();
        }
    }
}

export { TurnusLintConfig };

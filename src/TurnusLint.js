/*
 * Part of TurnusLint - a tool to analyze work schedules
 *
 * Copyright (C) Eskild Hustvedt 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */
import { ShiftIdentifier } from "./ShiftIdentifier.js";
import type { ShiftList } from "./ShiftIdentifier.js";
import GatParser from "./GatParser.js";
import type { GatData } from "./GatParser.js";
import type {
    DateObjectLiteral,
    DateObjectLiteralNumeric,
    WeekToDateType,
    BootstrappedNote,
} from "./sharedtypes.js";
import type { SpecialDays, SpecialDayTypes } from "./HolidaysClass.js";
import { Holidays } from "./HolidaysClass.js";
import { TurnusLintLogger } from "./logger.js";
import { TurnusLintConfig } from "./Config.js";
import { uniqueid, dateFnsGetFormat } from "./utils.js";
declare var TURNUSLINT_DEBUG: boolean;
declare var USE_LIMITED_RULESET: boolean;

type DateObjectCache = {|
    [string]: Date,
|};

const dayNumberToDayName: Map<number, string> = new Map([
    [1, "måndag"],
    [2, "tysdag"],
    [3, "onsdag"],
    [4, "torsdag"],
    [5, "fredag"],
    [6, "laurdag"],
    [7, "sundag"],
]);

function dayNamer(dayNo: number): string {
    const result = dayNumberToDayName.get(dayNo);
    if (typeof result !== "string") {
        throw "dayNamer() received invalid dayNo=" + dayNo;
    }
    return result;
}

class ShiftFeedbackFormatter {
    _weekToDateMap: WeekToDateType;
    _dateObjectCache: DateObjectCache;
    _owner: string;

    constructor(weekToDateMap: WeekToDateType, owner: string) {
        this._weekToDateMap = weekToDateMap;
        this._dateObjectCache = {};
        this._owner = owner;
    }

    // weekNo is for use in subclassed formatters
    // eslint-disable-next-line no-unused-vars
    dayNamer(dayNo: number, weekNo: number | null): string {
        return dayNamer(dayNo);
    }

    getCalendarDate(
        week: string | number,
        day: number,
        fetching: string = ".getCalendarDate",
    ): string {
        const map = this.getDateFromMap(week, day, fetching);
        return (
            (map.day + "").padStart(2, "0") +
            "/" +
            (map.month + "").padStart(2, "0")
        );
    }

    verboseShiftName(genericName: string, specificName: string): string {
        if (genericName == specificName) {
            return genericName;
        }
        return genericName + " (" + specificName + ")";
    }

    roundToDecimals(number: number, decimals: number): number {
        return Number(number.toFixed(decimals));
    }

    getDateFromMap(
        week: string | number,
        day: number,
        fetching: string = "unknown",
    ): DateObjectLiteral {
        if (day > 7) {
            throw (
                "getDateFromMap received invalid day integer " +
                day +
                " of week " +
                week +
                " for " +
                this._owner +
                " (while processing " +
                fetching +
                ")"
            );
        }
        // Make sure week is a string
        week += "";
        if (!this._weekToDateMap[week]) {
            throw (
                "No weekToDateMap for week " +
                week +
                " (while processing " +
                fetching +
                ")"
            );
        }
        if (!this._weekToDateMap[week][day]) {
            throw (
                "No weekToDateMap for day " +
                day +
                " of week " +
                week +
                " (while processing " +
                fetching +
                ")"
            );
        }
        return this._weekToDateMap[week][day];
    }

    getFirstWeekNumber(): number {
        const weeks = Object.keys(this._weekToDateMap).sort();
        if (typeof weeks[0] !== "number") {
            return Number.parseInt(weeks[0]);
        }
        return weeks[0];
    }

    getNumericDateFromMap(
        week: string | number,
        day: number,
        fetching: string = ".getNumericDateFromMap",
    ): DateObjectLiteralNumeric {
        const map = this.getDateFromMap(week, day, fetching);
        return {
            year: parseInt(map.year),
            month: parseInt(map.month),
            day: parseInt(map.day),
            week: parseInt(map.week),
        };
    }

    getDateObjectBasedOnMap(
        week: string | number,
        day: number,
        fetching: string = ".getDateObjectBasedOnMap",
    ): Date {
        const key = week + "-" + day;
        if (this._dateObjectCache[key] === undefined) {
            const map = this.getNumericDateFromMap(week, day, fetching);
            const zeroIndexedMonth = map.month - 1;
            this._dateObjectCache[key] = new Date(
                map.year,
                zeroIndexedMonth,
                map.day,
            );
        }
        return this._dateObjectCache[key];
    }

    getDateRangeForWeekAsString(
        week: string | number,
        fetching: string = ".getDateRangeForWeekAsString",
    ): string {
        const startDate = this.getDateObjectBasedOnMap(week, 1, fetching);
        const endDate = this.getDateObjectBasedOnMap(week, 7, fetching);
        let result: string;
        if (startDate.getMonth() === endDate.getMonth()) {
            result =
                startDate.getDate() +
                "-" +
                endDate.getDate() +
                ". " +
                dateFnsGetFormat(startDate, "LLLL");
        } else {
            result =
                dateFnsGetFormat(startDate, "dd. LLLL") +
                " - " +
                dateFnsGetFormat(endDate, "dd. LLLL");
        }
        return result;
    }
}

type WarningMetaContainer = {|
    text: string,
    calendarWeek: number,
    calendarYear: number,
|};
export type WarningMetaFormattedContainer = {|
    week: string,
    ...WarningMetaContainer,
|};
export type SummaryDetailContainer = {|
    text: string,
    tooltipText: string | null,
|};
type WeekIterCallback = (number, Array<string>) => boolean;
type ShiftIterCallback = (
    number,
    number,
    string,
    DateObjectLiteralNumeric,
) => boolean;

export type ShiftNoticeSubtypes =
    | "SINGULAR_A"
    | "NIGHTS_IN_A_ROW"
    | "A_IN_A_ROW"
    | "MISSING_F1"
    | "EARLY_F1"
    | "LATE_F_AFTER_WEEKEND"
    | "WEEKENDS_TOO_CLOSE"
    | "DAYS_IN_A_ROW"
    | "WRONG_OR_MISSING_F3"
    | "SHIFT_ON_DAY_AFTER_N"
    | "CLOSE_A/D"
    | "F1_NOT_CLOSEST_TO_WEEKEND"
    | "AN_ON_NON_WEEKEND_FRIDAY"
    | "STRANGE_F_PLACEMENT_ON_SATURDAY"
    | "CLOSE_N_AND_A/D"
    | "VERY_CLOSE_N_AND_A/D"
    | "LONELY_SHIFT"
    | "ONE_SHIFT_WEEK"
    | "D_BEFORE_N"
    | "D_AFTER_N"
    | "MISSING_F_AFTER_N"
    | "MISPLACED_F1"
    | "WRONG_F4_COUNT"
    | "BOOTSTRAPPED_NOTE";
export type StatisticTypes =
    | "D_IN_A_ROW"
    | "A_IN_A_ROW"
    | "A_BEFORE_F_TIMES"
    | "STANDALONE_F_TIMES"
    | "SHIFTS_IN_A_ROW"
    | "WEEKS_OFF"
    | "RED_DAYS"
    | "SPECIAL_DAYS"
    | "QUICK_RETURNS"
    | "QUICK_RETURNS_IN_WEEKEND"
    | "WEEKENDS"
    | "INFERRED_STAIRCASE_WEEKENDS"
    | "D_SHIFTS"
    | "A_SHIFTS"
    | "NIGHTS"
    | "MIN_NIGHTS_IN_A_ROW"
    | "MAX_NIGHTS_IN_A_ROW"
    | "FREE_CODE_SPREAD"
    | "SINGLE_SHIFT_CODE_WEEKS"
    | "SHIFT_GENERIC_SPREAD"
    | "WEEKS_WITH_ALL_SHIFTS"
    | "NIGHT_SHIFT_WEEKENDS"
    | "SHORT_WORK_WEEKENDS"
    | "LONG_WEEKENDS"
    | "SHIFT_CODE_SPREAD"
    | "LONELY_SHIFTS"
    | "TIME_REDUCTION";

export type TimeReductionStates = "Ja" | "Nei" | "Kanskje" | "Truleg ikkje";

export type MonthCounter = Map<string, number>;
export type MonthSpreadStatisticsEntry = {|
    type: "MONTH_SPREAD",
    months: MonthCounter,
|};
export type StatisticsEntryMetadataObject = MonthSpreadStatisticsEntry;
export type ShiftNotice = {|
    week: string,
    type: ShiftNoticeSubtypes,
    message: string,
    tooltipText: string | null,
    calendarWeek: number,
    calendarYear: number,
    key: string,
|};
export type ShiftNotices = Array<ShiftNotice>;
type SecondaryValueType = string | null;
type TietaryValueType = string | number | null;
export type StatisticsEntry = {|
    value: number | string,
    metaObject?: StatisticsEntryMetadataObject | null,
    secondaryValue: SecondaryValueType,
    tietaryValue: TietaryValueType,
    type: StatisticTypes,
|};
export type PersonStatistics = {|
    [StatisticTypes]: StatisticsEntry,
|};
type ShiftAnalysisCache = {|
    warnings: ShiftNotices,
    notes: ShiftNotices,
    errors: ShiftNotices,
    important: ShiftNotices,
    hidden: ShiftNotices,
|};
type CacheKey = $Keys<ShiftAnalysisCache>;

type ShiftAnalysisContainerCallback = (ShiftAnalysisContainer) => void;
type SpecialDayValues = {|
    shift: string,
    name: string,
    sortValue: number,
    type: SpecialDayTypes,
    ...DateObjectLiteralNumeric,
|};
type SpecialDayMap = {|
    [SpecialDays]: SpecialDayValues,
|};
export type ShiftCounter = {|
    [string]: number,
|};

type setStatisticMoreValues = {|
    secondaryValue?: SecondaryValueType,
    tietaryValue?: TietaryValueType,
    metaObject?: null | StatisticsEntryMetadataObject,
|};

class ShiftAnalysisContainer {
    _warnings: ShiftNotices = [];
    _errors: ShiftNotices = [];
    _notes: ShiftNotices = [];
    _specialDays: SpecialDayMap = {};
    _cache: ShiftAnalysisCache = {
        warnings: [],
        notes: [],
        errors: [],
        important: [],
        hidden: [],
    };
    _name: string;
    _hashedName: string;
    _id: number = uniqueid();
    _statistics: PersonStatistics = {};
    _isNightWorker: boolean = false;
    _dirty: boolean = true;
    _listeners: Array<ShiftAnalysisContainerCallback | null> = [];
    _startYear: number;
    _shiftCounters: ShiftCounter = {};
    config: TurnusLintConfig;
    formatter: ShiftFeedbackFormatter;

    _weekToDateMap: WeekToDateType;
    constructor(
        weekToDateMap: WeekToDateType,
        name: string,
        config: TurnusLintConfig,
        formatter: ShiftFeedbackFormatter,
        hashedName: string,
    ) {
        this._weekToDateMap = weekToDateMap;
        this._name = name;
        this._hashedName = hashedName;
        this.formatter = formatter;
        this.config = config;
    }

    setSpecialDay(
        name: string,
        shift: string,
        sortValue: number,
        type: SpecialDayTypes,
        date: DateObjectLiteralNumeric,
        specialDay: SpecialDays,
    ) {
        const value: SpecialDayValues = {
            ...date,
            name,
            shift,
            type,
            sortValue,
        };
        this._specialDays[specialDay] = value;
    }
    isWorkingOnSpecialDay(day: SpecialDays): boolean {
        return this._specialDays[day] !== undefined;
    }
    getSpecialDay(day: SpecialDays): SpecialDayValues | null {
        if (this._specialDays[day] !== undefined) {
            return this._specialDays[day];
        }
        return null;
    }

    setStatistic(
        type: StatisticTypes,
        value: number | string,
        more: setStatisticMoreValues | null = null,
    ) {
        this._statistics[type] = {
            type,
            value,
            secondaryValue:
                more !== null && more.secondaryValue !== undefined
                    ? more.secondaryValue
                    : null,
            tietaryValue:
                more !== null && more.tietaryValue !== undefined
                    ? more.tietaryValue
                    : null,
            metaObject:
                more !== null && more.metaObject !== undefined
                    ? more.metaObject
                    : null,
        };
    }

    setShiftCounter(counter: ShiftCounter) {
        this._shiftCounters = counter;
    }

    shiftList(): Array<string> {
        return Object.keys(this._shiftCounters);
    }

    shiftCountFor(shift: string): number {
        if (typeof this._shiftCounters[shift] === "number") {
            return this._shiftCounters[shift];
        }
        return 0;
    }

    addWarning(
        type: ShiftNoticeSubtypes,
        week: string | number,
        message: string,
        tooltipText: string | null = null,
    ) {
        this._addInfoToThing(this._warnings, type, week, message, tooltipText);
    }

    addError(
        type: ShiftNoticeSubtypes,
        week: string | number,
        message: string,
        tooltipText: string | null = null,
    ) {
        this._addInfoToThing(this._errors, type, week, message, tooltipText);
    }

    addNote(
        type: ShiftNoticeSubtypes,
        week: string | number,
        message: string,
        tooltipText: string | null = null,
    ) {
        this._addInfoToThing(this._notes, type, week, message, tooltipText);
    }

    _addInfoToThing(
        thing: ShiftNotices,
        type: ShiftNoticeSubtypes,
        week: string | number,
        message: string,
        tooltipText: string | null = null,
    ) {
        if (USE_LIMITED_RULESET === true) {
            const HelseVestIgnoreList: {
                [string]: boolean,
            } = {
                WEEKENDS_TOO_CLOSE: true,
            };
            if (HelseVestIgnoreList[type] === true) {
                return;
            }
        }
        week = week + "";
        let calendarWeek: ?number, calendarYear: ?number;
        if (this._weekToDateMap[week] && this._weekToDateMap[week][7]) {
            calendarWeek = this._weekToDateMap[week][7].week;
            calendarYear = this._weekToDateMap[week][7].year;
        }
        if (
            calendarWeek === undefined ||
            calendarWeek === null ||
            calendarYear === null ||
            calendarYear === undefined
        ) {
            throw "_addInfoToThing got undefined or null calendarWeek/calendarYear";
        }
        thing.push({
            type,
            week,
            message,
            tooltipText,
            calendarWeek,
            calendarYear,
            key: type + calendarWeek + calendarYear,
        });
        this._dirty = true;
        this._notifySubscribers();
    }

    notes(): ShiftNotices {
        return this._getFromCache("notes");
    }
    warnings(): ShiftNotices {
        return this._getFromCache("warnings");
    }
    errors(): ShiftNotices {
        return this._getFromCache("errors");
    }
    important(): ShiftNotices {
        return this._getFromCache("important");
    }
    hidden(): ShiftNotices {
        return this._getFromCache("hidden");
    }
    isImportant(entry: ShiftNotice): boolean {
        return this.config.getIsImportant(this._hashedName, entry.key);
    }
    markImportant(entry: ShiftNotice) {
        this.config.setAsImportant(this._hashedName, entry.key, true);
        this.unmarkHidden(entry);
        this._dirty = true;
        this._notifySubscribers();
    }
    unmarkImportant(entry: ShiftNotice) {
        if (this.config.getIsImportant(this._hashedName, entry.key) === true) {
            this.config.setAsImportant(this._hashedName, entry.key, false);
            this._dirty = true;
            this._notifySubscribers();
        }
    }
    isHidden(entry: ShiftNotice): boolean {
        return this.config.getEntryIgnoredStatus(this._hashedName, entry.key);
    }
    markHidden(entry: ShiftNotice) {
        this.config.setEntryIgnoredStatus(this._hashedName, entry.key, true);
        this.unmarkImportant(entry);
        this._dirty = true;
        this._notifySubscribers();
    }
    unmarkHidden(entry: ShiftNotice) {
        if (
            this.config.getEntryIgnoredStatus(this._hashedName, entry.key) ===
            true
        ) {
            this.config.setEntryIgnoredStatus(
                this._hashedName,
                entry.key,
                false,
            );
            this._dirty = true;
            this._notifySubscribers();
        }
    }
    statistics(): PersonStatistics {
        return this._statistics;
    }
    statistic(type: StatisticTypes, fallback: string = ""): StatisticsEntry {
        const entry = this._statistics[type];
        if (
            entry === null ||
            entry === undefined ||
            entry.value === null ||
            entry.value === undefined
        ) {
            return {
                type,
                value: fallback,
                secondaryValue: null,
                tietaryValue: null,
            };
        }
        return entry;
    }
    name(): string {
        // Hack that allows one to open up the console and enable "anonymous" mode
        if (window.TurnusLintAnonymize === true) {
            return "Test, Person (" + this.id() + ")";
        }
        return this._name;
    }
    id(): number {
        return this._id;
    }
    isNightWorker(): boolean {
        return this._isNightWorker;
    }
    setIsNightWorker(set: boolean): void {
        this._isNightWorker = set;
    }
    setStartYear(year: number): void {
        this._startYear = year;
    }
    getStartYear(): number {
        return this._startYear;
    }
    ignored(): boolean {
        if (this._startYear === undefined) {
            return false;
        }
        return this.config.getPersonIgnored(this._hashedName, this._startYear);
    }
    setIgnored(value: boolean): boolean {
        const result = this.config.setPersonIgnored(
            this._hashedName,
            this._startYear,
            value,
        );
        this._notifySubscribers();
        return result;
    }
    toggleIgnored(): boolean {
        const origState = this.ignored();
        return this.setIgnored(!origState);
    }
    subscribe(cb: (ShiftAnalysisContainer) => void): number {
        return this._listeners.push(cb) - 1;
    }
    unsubscribe(no: number) {
        this._listeners[no] = null;
    }
    _notifySubscribers() {
        for (const cb of this._listeners) {
            try {
                if (cb !== null) {
                    cb(this);
                }
            } catch (_) {} // eslint-disable-line no-empty
        }
    }
    _getFromCache(key: CacheKey): ShiftNotices {
        if (this._dirty === true) {
            this._rebuildCache();
        }
        return this._cache[key];
    }
    _rebuildCache() {
        this._dirty = false;
        this._cache.important = [];
        this._cache.hidden = [];
        this._buildCache("warnings", this._warnings);
        this._buildCache("notes", this._notes);
        this._buildCache("errors", this._errors);
    }
    _buildCache(type: CacheKey, source: ShiftNotices) {
        const target = [];
        for (const entry of source) {
            if (this.config.getIsImportant(this._hashedName, entry.key)) {
                this._cache.important.push(entry);
            } else if (
                this.config.getEntryIgnoredStatus(this._hashedName, entry.key)
            ) {
                this._cache.hidden.push(entry);
            } else {
                target.push(entry);
            }
        }
        this._cache[type] = target;
    }
}

type PersonShiftStructure = Array<Array<string>>;

type QueuedIteratorsStructure = {|
    day: Array<ShiftIterCallback>,
    week: Array<WeekIterCallback>,
    finalizers: Array<() => void>,
|};

class PersonChecker {
    _name: string;
    identifier: ShiftIdentifier;
    analysis: ShiftAnalysisContainer;
    _bootstrappedNotes: Array<BootstrappedNote>;
    _queuedIterators: QueuedIteratorsStructure;
    __analyzed: boolean;
    __isNightWorker: boolean | null;
    __isValidPerson: boolean | null;
    _weekToDateMap: WeekToDateType;
    _person: PersonShiftStructure;
    _logger: TurnusLintLogger;
    holidays: Holidays;
    config: TurnusLintConfig;
    format: ShiftFeedbackFormatter;
    constructor(
        name: string,
        shifts: ShiftList,
        person: PersonShiftStructure,
        weekToDateMap: WeekToDateType,
        config: TurnusLintConfig,
        logger: TurnusLintLogger,
        bootstrappedNotes: Array<BootstrappedNote>,
        hashedName: string,
        holidays: Holidays,
    ) {
        this._name = name;
        this._person = person;
        this._weekToDateMap = weekToDateMap;
        this.__isNightWorker = null;
        this.__isValidPerson = null;
        this.__analyzed = false;
        this._logger = logger;
        this.holidays = holidays;
        this._bootstrappedNotes = bootstrappedNotes;
        this._queuedIterators = {
            day: [],
            week: [],
            finalizers: [],
        };
        this.config = config;
        this.identifier = new ShiftIdentifier(shifts, this._logger);
        this.format = new ShiftFeedbackFormatter(weekToDateMap, name);
        this.analysis = new ShiftAnalysisContainer(
            weekToDateMap,
            name,
            config,
            this.format,
            hashedName,
        );
    }

    _queueShiftIterator(cb: ShiftIterCallback) {
        this._queuedIterators.day.push(cb);
    }

    _queueWeekIterator(cb: WeekIterCallback) {
        this._queuedIterators.week.push(cb);
    }

    _queueFinalizer(cb: () => void) {
        this._queuedIterators.finalizers.push(cb);
    }

    _isNightWorker(): boolean {
        if (this.__isNightWorker === null) {
            this._populatePersonMeta();
        }
        if (this.__isNightWorker === null) {
            throw "Unable to populate __isNightWorker";
        }
        return this.__isNightWorker;
    }

    _dateAddOrSubtractNDays(
        currentDate: Date,
        days: number,
        mode: "add" | "subtract",
    ): Date {
        const oneDay = 86400;
        let unixTime = currentDate.getTime() / 1000;
        if (mode === "add") {
            unixTime += oneDay * days;
        } else if (mode === "subtract") {
            unixTime -= oneDay * days;
        }
        return new Date(unixTime * 1000);
    }

    _getDateNDaysEarlier(currentDate: Date, daysEarlier: number): Date {
        return this._dateAddOrSubtractNDays(
            currentDate,
            daysEarlier,
            "subtract",
        );
    }
    _getDateNDaysLater(currentDate: Date, daysLater: number): Date {
        return this._dateAddOrSubtractNDays(currentDate, daysLater, "add");
    }

    _runQueuedAnalyzers() {
        const disabledIteratorsWeek = [];
        const disabledIteratorsDay = [];
        // Not all schedules start with week 1, so we fetch the first week
        // number that we actually have data for and use that as a starting off
        // point
        const firstWeek = this.format.getFirstWeekNumber();
        let weekNo = firstWeek - 1;
        for (const week of this._person) {
            if (week == null) {
                continue;
            }
            weekNo++;
            /*
             * Call our weekly callbacks, if it returns false, then we should
             * disable that callback
             */
            for (let n = 0; n < this._queuedIterators.week.length; n++) {
                if (disabledIteratorsWeek[n] === true) {
                    continue;
                }
                if (this._queuedIterators.week[n](weekNo, week) === false) {
                    disabledIteratorsWeek[n] = true;
                }
            }
            /**
             * Iterate over shifts for those queued for that
             */
            let shiftNo = 0;
            for (const shift of week) {
                shiftNo++;
                const dateObject = this.format.getNumericDateFromMap(
                    weekNo,
                    shiftNo,
                    "_runQueuedAnalyzers",
                );
                /*
                 * Call our weekly callbacks, if it returns false, then we should
                 * disable that callback
                 */
                for (let s = 0; s < this._queuedIterators.day.length; s++) {
                    if (disabledIteratorsDay[s] === true) {
                        continue;
                    }
                    if (
                        this._queuedIterators.day[s](
                            weekNo,
                            shiftNo,
                            shift,
                            dateObject,
                        ) === false
                    ) {
                        disabledIteratorsDay[s] = true;
                    }
                }
            }
        }

        for (const finalizer of this._queuedIterators.finalizers) {
            finalizer();
        }

        this._queuedIterators = {
            day: [],
            week: [],
            finalizers: [],
        };
    }

    // day is 1-indexed, not 0-indexed
    _getShiftForDay(weekNo: number, day: number): string {
        day--;
        if (
            Array.isArray(this._person[weekNo]) &&
            this._person[weekNo][day] !== undefined
        ) {
            return this._person[weekNo][day];
        }
        return "";
    }

    _populatePersonMeta() {
        // TODO: Should identify night workers based upon percentage of night
        // work, instead of the hardcoded permittedNonNShiftsForNightWorkers
        let nonNShifts = 0;
        let nonFShifts = 0;
        let NShifts = 0;
        const permittedNonNShiftsForNightWorkers = 7;
        for (const week of this._person) {
            let breakOut = false;
            if (week == null) {
                continue;
            }
            for (const day of week) {
                const isF = this.identifier.isF(day);
                const isN = this.identifier.isN(day);
                if (!isF && !isN) {
                    nonNShifts++;
                } else if (isN) {
                    NShifts++;
                }
                if (!isF) {
                    nonFShifts++;
                }
                if (nonNShifts > permittedNonNShiftsForNightWorkers) {
                    breakOut = true;
                }
            }
            if (breakOut) {
                break;
            }
        }
        if (
            nonNShifts <= permittedNonNShiftsForNightWorkers &&
            nonFShifts > 0 &&
            NShifts > 1
        ) {
            this.__isNightWorker = true;
            this.analysis.setIsNightWorker(true);
        } else {
            this.__isNightWorker = false;
        }
        if (nonFShifts > 0) {
            this.__isValidPerson = true;
        } else {
            this.__isValidPerson = false;
        }
    }

    analyze() {
        if (this.__analyzed === false) {
            if (this.isValidPerson()) {
                this.findFirstDate();
                this.checkAD();
                this.checkShiftTypesInOneWeek();
                this.checkN();
                this.checkA();
                this.checkConsecutive();
                this.checkPreN();
                this.checkF1();
                this.checkWeekends();
                this.checkWeeksOff();
                this.checkRedDays();
                this.checkStandalone();
                // This must be after .checkWeekends() because it uses values
                // from there
                this.gatherShiftStats();
                this.countWeekendTypes();
                this.addBootstrappedNotes();
                this.checkSingularA();
                this._runQueuedAnalyzers();
            }
            this.__analyzed = true;
        }
    }

    addBootstrappedNotes() {
        if (this._bootstrappedNotes !== undefined) {
            for (const note of this._bootstrappedNotes) {
                this.analysis.addNote(
                    "BOOTSTRAPPED_NOTE",
                    note.week,
                    note.message,
                );
            }
        }
    }

    isValidPerson(): boolean {
        if (this.__isValidPerson === null) {
            this._populatePersonMeta();
        }
        if (this.__isValidPerson === null) {
            throw "Unable to populate __isValidPerson";
        }
        return this.__isValidPerson;
    }

    findFirstDate() {
        this._queueShiftIterator((weekNo, dayNo, _, fullDate) => {
            this.analysis.setStartYear(fullDate.year);
            return false;
        });
    }

    checkShiftTypesInOneWeek() {
        let weeksWithAllTypes = 0;
        const weekNumbersWithAllTypes: Array<number> = [];
        let lastShiftLastWeek;
        this._queueWeekIterator((weekNo, week) => {
            let A = 0;
            let D = 0;
            let N = 0;
            for (const shift of week) {
                if (this.identifier.isD(shift)) {
                    D++;
                } else if (this.identifier.isA(shift)) {
                    A++;
                } else if (this.identifier.isN(shift)) {
                    N++;
                }
            }
            if (
                lastShiftLastWeek !== undefined &&
                lastShiftLastWeek !== null &&
                this.identifier.isN(lastShiftLastWeek)
            ) {
                N++;
            }
            if (N > 0 && D > 0 && A > 0) {
                weeksWithAllTypes++;
                const date = this.format.getDateFromMap(
                    weekNo,
                    7,
                    "checkShiftTypesInOneWeek",
                );
                weekNumbersWithAllTypes.push(date.week);
            }
            lastShiftLastWeek = week[6];
            return true;
        });
        this._queueFinalizer(() => {
            this.analysis.setStatistic(
                "WEEKS_WITH_ALL_SHIFTS",
                weeksWithAllTypes,
                {
                    secondaryValue:
                        weeksWithAllTypes > 0
                            ? "Veke " + weekNumbersWithAllTypes.join(", ")
                            : null,
                },
            );
        });
    }

    checkSingularA() {
        if (this.config.getBooleanOption("enableSingularAChecks") !== true) {
            return;
        }
        let prevShiftDOW;
        let prevShift;
        let prevShift2;
        this._queueShiftIterator((weekNo, dayNo, shift, _) => {
            if (
                dayNo > 1 &&
                dayNo < 7 &&
                this.identifier.identify(prevShift) === "A" &&
                this.identifier.identify(shift) !== "A" &&
                this.identifier.identify(prevShift2) !== "A" &&
                prevShiftDOW !== undefined &&
                this.identifier.isD(prevShift2) &&
                prevShift2.substr(0, 1) === "D"
            ) {
                this.analysis.addWarning(
                    "SINGULAR_A",
                    weekNo,
                    "enkeltståande " +
                        this.format.verboseShiftName("A", prevShift) +
                        " på " +
                        this.format.dayNamer(prevShiftDOW, null) +
                        ": mogleg å optimalisere?",
                    "Kanskje vedkommande kan få ei A vakt til, sånn at det minst er to på rad? Alternativt, kanskje vakta kan fjernast?",
                );
            }
            prevShift2 = prevShift;
            prevShift = shift;
            prevShiftDOW = dayNo;
            return true;
        });
    }

    checkAD() {
        let last = "";
        let lastDayNo;
        let lastDayWeek;
        let sincePrevAD = 99;
        let prevADdays: Array<string | number> = [];
        let ADs = 0;
        let ADsInWeekend = 0;
        const daysPluralizer = (days: number): string => {
            if (days === 1) {
                return "dag";
            }
            return "dagar";
        };
        const NTracker = {
            sincePrevN: 99,
            lastDayNo: -1,
            lastDayWeek: -1,
        };
        const ADMonthCounter: MonthCounter = new Map();
        const ADWeekendMonthCounter: MonthCounter = new Map();
        this._queueShiftIterator((weekNo, dayNo, shift, fullDate) => {
            const daysBetween = sincePrevAD;
            // Format the current date's month as Month/Year
            const monthOfYear =
                (fullDate.month + "").padStart(2, "0") + "/" + fullDate.year;
            // Initialize counter if it's not set. This is done outside the isAD check
            // to make sure we have entries with 0 A/D's for months that have none.
            let CurrentMonthCounter = ADMonthCounter.get(monthOfYear);
            let CurrentWeekendMonthCounter =
                ADWeekendMonthCounter.get(monthOfYear);
            if (CurrentMonthCounter === undefined) {
                CurrentMonthCounter = 0;
                ADMonthCounter.set(monthOfYear, CurrentMonthCounter);
            }
            if (CurrentWeekendMonthCounter === undefined) {
                CurrentWeekendMonthCounter = 0;
                ADWeekendMonthCounter.set(monthOfYear, CurrentMonthCounter);
            }
            if (this.identifier.isAD(last, shift)) {
                // Count this A/D
                ADMonthCounter.set(monthOfYear, ++CurrentMonthCounter);
                ADs++;

                // Count weekend A/D
                if (dayNo === 1 || dayNo >= 6) {
                    ADsInWeekend++;
                    ADWeekendMonthCounter.set(
                        monthOfYear,
                        ++CurrentWeekendMonthCounter,
                    );
                }

                const currentDays = [
                    this.format.dayNamer(lastDayNo, lastDayWeek),
                    "/",
                    this.format.dayNamer(dayNo, weekNo),
                ];
                if (
                    daysBetween <
                        this.config.getNumericOption("minDaysBetweenAD") &&
                    USE_LIMITED_RULESET === false
                ) {
                    if (prevADdays.length === 0) {
                        throw "No prevADdays";
                    }
                    this.analysis.addWarning(
                        "CLOSE_A/D",
                        weekNo,
                        "tette A/D-vakter, " +
                            prevADdays.join("") +
                            " og " +
                            currentDays.join("") +
                            " (berre " +
                            daysBetween +
                            " " +
                            daysPluralizer(daysBetween) +
                            " mellom)",
                    );
                } else if (
                    USE_LIMITED_RULESET !== true &&
                    NTracker.sincePrevN <
                        this.config.getNumericOption("minDaysBetweenAD")
                ) {
                    const daysBetween = NTracker.sincePrevN;
                    const message =
                        "tett mellom N (" +
                        this.format.dayNamer(
                            NTracker.lastDayNo,
                            NTracker.lastDayWeek,
                        ) +
                        ") og A/D (" +
                        currentDays.join("") +
                        ") (berre " +
                        daysBetween +
                        " " +
                        daysPluralizer(daysBetween) +
                        " mellom)";
                    // 0 days is a warning
                    if (daysBetween === 0) {
                        this.analysis.addWarning(
                            "VERY_CLOSE_N_AND_A/D",
                            weekNo,
                            message,
                        );
                    }
                    // More than 0 days is a notice
                    else {
                        this.analysis.addNote(
                            "CLOSE_N_AND_A/D",
                            weekNo,
                            message,
                        );
                    }
                }
                prevADdays = currentDays;
            } else if (this.identifier.isN(shift)) {
                NTracker.lastDayNo = dayNo;
                NTracker.lastDayWeek = weekNo;
                if (
                    !this.identifier.isN(last) &&
                    sincePrevAD <
                        this.config.getNumericOption("minDaysBetweenAD")
                ) {
                    const message =
                        "tett mellom A/D (" +
                        prevADdays.join("") +
                        ") og N (" +
                        this.format.dayNamer(
                            NTracker.lastDayNo,
                            NTracker.lastDayWeek,
                        ) +
                        ") (berre " +
                        sincePrevAD +
                        " " +
                        daysPluralizer(sincePrevAD) +
                        " mellom)";
                    if (sincePrevAD <= 1) {
                        this.analysis.addWarning(
                            "VERY_CLOSE_N_AND_A/D",
                            weekNo,
                            message,
                        );
                    } else {
                        this.analysis.addNote(
                            "CLOSE_N_AND_A/D",
                            weekNo,
                            message,
                        );
                    }
                }
                NTracker.sincePrevN = 0;
            }
            // Only bump sincePrevAD if the current shift is not the "D" in an A/D-block
            if (!this.identifier.isAD(last, shift)) {
                sincePrevAD++;
            } else {
                sincePrevAD = 0;
            }
            last = shift;
            lastDayNo = dayNo;
            lastDayWeek = weekNo;
            NTracker.sincePrevN++;
            return true;
        });
        this._queueFinalizer(() => {
            /*
            if (ADs > 36) {
                this.feedback.addGeneralWarning("Mange A/D vakter: " + ADs);
            }*/
            // Build a string with a summary of the months with A/D
            const formattedMonthCounter = [];
            const formattedWeekendMonthCounter = [];
            for (const [key, value] of ADMonthCounter) {
                formattedMonthCounter.push(key + ":\u00A0" + value);
            }
            for (const [key, value] of ADWeekendMonthCounter) {
                formattedWeekendMonthCounter.push(key + ":\u00A0" + value);
            }
            this.analysis.setStatistic("QUICK_RETURNS", ADs, {
                secondaryValue:
                    ADs > 0 ? formattedMonthCounter.join("\n") : null,
                metaObject: {
                    type: "MONTH_SPREAD",
                    months: ADMonthCounter,
                },
            });
            this.analysis.setStatistic(
                "QUICK_RETURNS_IN_WEEKEND",
                ADsInWeekend,
                {
                    secondaryValue:
                        ADsInWeekend > 0
                            ? formattedWeekendMonthCounter.join("\n")
                            : null,
                    metaObject:
                        ADsInWeekend > 0
                            ? {
                                  type: "MONTH_SPREAD",
                                  months: ADWeekendMonthCounter,
                              }
                            : null,
                },
            );
        });
    }
    checkA() {
        if (
            this.config.getBooleanOption("enableMaxConsecutiveASHifts") !== true
        ) {
            return;
        }
        let currentA = 0;
        let lastAWeek = -1;
        const maxConsecutiveAShifts = this.config.getNumericOption(
            "maxConsecutiveAShifts",
        );
        const runAValidation = () => {
            if (currentA > 0) {
                if (currentA > maxConsecutiveAShifts) {
                    this.analysis.addWarning(
                        "A_IN_A_ROW",
                        lastAWeek,
                        "for mange A-vakter på rad: " + currentA,
                    );
                }
            }
            currentA = 0;
        };
        this._queueShiftIterator((weekNo, dayNo, shift, _) => {
            if (this.identifier.isA(shift)) {
                currentA++;
                lastAWeek = weekNo;
            } else {
                runAValidation();
            }
            return true;
        });
        this._queueFinalizer(runAValidation);
    }

    checkN() {
        let currentN = 0;
        let lastNWeek = 0;
        let nightShifts = 0;
        let nightShiftWeekends = 0;
        let fewestConsecutiveNights = 999;
        let mostConsecutiveNights = -1;
        let maxConsecutiveNights = this.config.getNumericOption(
            "maxConsecutiveNights",
        );
        const NMonthCounter: MonthCounter = new Map();
        if (this._isNightWorker()) {
            maxConsecutiveNights = this.config.getNumericOption(
                "maxConsecutiveNightsNightWorker",
            );
        }
        this._queueShiftIterator((weekNo, dayNo, shift, _) => {
            if (this.identifier.isN(shift)) {
                // Fetch the date representing this day
                const fullDate = this.format.getDateObjectBasedOnMap(
                    weekNo,
                    dayNo,
                    "checkN",
                );
                // Format the current date's month as Month/Year
                const monthOfYear =
                    fullDate.toLocaleString(["nn-NO", "no-NO"], {
                        month: "short",
                    }) +
                    "/" +
                    fullDate.getFullYear();
                let currentCount = NMonthCounter.get(monthOfYear);
                if (currentCount === undefined) {
                    currentCount = 0;
                    NMonthCounter.set(monthOfYear, currentCount);
                }
                NMonthCounter.set(monthOfYear, ++currentCount);
                currentN++;
                nightShifts++;
                lastNWeek = weekNo;
                if (dayNo === 7) {
                    if (currentN > 1) {
                        nightShiftWeekends++;
                    }
                }
            } else {
                if (currentN > 0) {
                    if (currentN > maxConsecutiveNights) {
                        this.analysis.addWarning(
                            "NIGHTS_IN_A_ROW",
                            lastNWeek,
                            "for mange netter på rad: " + currentN,
                        );
                    }
                    if (fewestConsecutiveNights > currentN) {
                        fewestConsecutiveNights = currentN;
                    }
                    if (mostConsecutiveNights < currentN) {
                        mostConsecutiveNights = currentN;
                    }
                    // If we have an F* day the day that a person goes off from N, show a notice
                    if (
                        this.identifier.identify(shift) === "F" &&
                        shift !== "" &&
                        shift !== undefined
                    ) {
                        this.analysis.addNote(
                            "SHIFT_ON_DAY_AFTER_N",
                            weekNo,
                            shift + " dagen vedkommande går av nattevakt",
                        );
                    }
                }
                currentN = 0;
            }
            return true;
        });
        this._queueFinalizer(() => {
            // Build a string with a summary of the months with N
            const formattedMonthCounter = [];
            for (const [key, value] of NMonthCounter) {
                formattedMonthCounter.push(key + ":\u00A0" + value);
            }
            this.analysis.setStatistic("NIGHTS", nightShifts, {
                secondaryValue:
                    nightShifts > 0 ? formattedMonthCounter.join("\n") : null,
                metaObject: {
                    type: "MONTH_SPREAD",
                    months: NMonthCounter,
                },
            });
            this.analysis.setStatistic(
                "NIGHT_SHIFT_WEEKENDS",
                nightShiftWeekends,
            );
            if (nightShifts > 0) {
                this.analysis.setStatistic(
                    "MAX_NIGHTS_IN_A_ROW",
                    mostConsecutiveNights,
                );
                this.analysis.setStatistic(
                    "MIN_NIGHTS_IN_A_ROW",
                    fewestConsecutiveNights,
                );
            }
        });
    }

    checkPreN() {
        let lastShift = "";
        let shiftMinusTwo = "";
        let shiftMinusThree = "";
        this._queueShiftIterator((weekNo, dayNo, shift) => {
            if (this.identifier.isN(shift)) {
                if (
                    USE_LIMITED_RULESET === false &&
                    this.identifier.isD(lastShift)
                ) {
                    this.analysis.addWarning(
                        "D_BEFORE_N",
                        weekNo,
                        "problematisk døgnrytme, " +
                            this.format.verboseShiftName("D", lastShift) +
                            " før " +
                            this.format.verboseShiftName("N", shift),
                    );
                }
            } else if (this.identifier.isN(shiftMinusTwo)) {
                if (!this.identifier.isF(shift)) {
                    this.analysis.addWarning(
                        "MISSING_F_AFTER_N",
                        weekNo,
                        "manglar fri etter " +
                            this.format.verboseShiftName("N", shiftMinusTwo) +
                            " på " +
                            this.format.dayNamer(dayNo, weekNo),
                    );
                }
            } else if (
                this.identifier.isN(shiftMinusThree) &&
                this.identifier.isF(shiftMinusTwo) &&
                this.identifier.isD(shift) &&
                USE_LIMITED_RULESET === false
            ) {
                this.analysis.addWarning(
                    "D_AFTER_N",
                    weekNo,
                    "problematisk døgnrytme, går rett på " +
                        this.format.verboseShiftName("D", shift) +
                        " etter " +
                        this.format.verboseShiftName("N", shiftMinusThree),
                );
            }
            shiftMinusThree = shiftMinusTwo;
            shiftMinusTwo = lastShift;
            lastShift = shift;
            return true;
        });
    }

    checkF1() {
        this._queueWeekIterator((weekNo, week) => {
            // Stores the position of the F1
            let F1day;
            // Stores the position of the last day off closest to the end of the week
            let latestF;
            // Stores the position of the last non-F3 day off closest to the end of the week
            let latestNonF3F;
            // Stores the position of the last work day
            let latestNonF;
            // Stores the shift code of the last non-F day
            let latestNonFShift;
            // Stores the shift code of the last non-F3 day off
            let latestNonF3FShift;

            // The current day number
            let dayNo = 0;
            // Defines if we found days that contain actual shifts this week or not
            let hadNonFDays = false;

            // Which day to treat as "early" for F1
            // Default: earlier than thursday
            // HV: earlier than wednesday
            let earlyF1Day = 4;
            if (USE_LIMITED_RULESET === true) {
                earlyF1Day = 3;
            }

            for (const shift of week) {
                dayNo++;
                // Store the F1 day
                if (shift == "F1") {
                    F1day = dayNo;
                }
                // If this day is a day off, we also store the latestF, and, if
                // needed, update latestNonF3F.
                if (this.identifier.isF(shift)) {
                    latestF = dayNo;
                    if (shift !== "F3") {
                        latestNonF3F = dayNo;
                        latestNonF3FShift = shift;
                    }
                } else {
                    // This day had an actual shift, so we know that this week
                    // has shifts.
                    hadNonFDays = true;
                    latestNonF = dayNo;
                    latestNonFShift = shift;
                }
            }

            if (!F1day && hadNonFDays) {
                // Theres no F1 day
                this.analysis.addWarning(
                    "MISSING_F1",
                    weekNo,
                    "manglar F1-dag",
                );
            } else if (
                F1day !== undefined &&
                latestF !== undefined &&
                latestF < earlyF1Day
            ) {
                // The F1 day is earlier than earlyF1Day
                if (USE_LIMITED_RULESET === true) {
                    this.analysis.addNote(
                        "EARLY_F1",
                        weekNo,
                        "tidleg F1 dag (" +
                            this.format.dayNamer(F1day, null) +
                            ")",
                    );
                } else {
                    this.analysis.addWarning(
                        "EARLY_F1",
                        weekNo,
                        "tidleg F1 dag (" +
                            this.format.dayNamer(F1day, null) +
                            ")",
                    );
                }
            } else if (
                F1day !== undefined &&
                F1day < 4 &&
                latestNonF3F !== undefined &&
                latestNonF3F > F1day &&
                latestNonF3FShift !== undefined &&
                latestNonF3FShift != ""
            ) {
                // We have an F* day, that is not an F3 (F3=public holiday)
                // closer to the weekend than the F1 day.  This isn't as
                // severe, since in practice it usually ends up the same, so we
                // make it a notice instead of a warning.
                this.analysis.addNote(
                    "F1_NOT_CLOSEST_TO_WEEKEND",
                    weekNo,
                    "F1 (" +
                        this.format.dayNamer(F1day, null) +
                        ") bør byte plass med " +
                        latestNonF3FShift +
                        " (" +
                        this.format.dayNamer(latestNonF3F, null) +
                        ")",
                );
            } else if (
                latestNonF !== 7 &&
                F1day !== 7 &&
                F1day !== undefined &&
                latestNonFShift !== undefined &&
                !(latestNonF === 6 && this.identifier.isN(latestNonFShift))
            ) {
                this.analysis.addNote(
                    "MISPLACED_F1",
                    weekNo,
                    "F1 ikkje på sundag, sjølv om sundag ikkje er ein arbeidsdag",
                );
            }
            return true;
        });
    }

    checkWeekends() {
        const config = this.config;
        // TODO: Instead of blindly assuming "every third weekend" when
        // inferring the number of weekends, we should be checking that this
        // assumption is true.
        let lastWeekend = -1;
        let lastWeekendCalendarWeekNumber = -1;
        let lastWeekendShiftList = null;
        let totalWeekends = 0;
        let totalWeekendsFirstYear = 0;
        // Permit work every third weekend by default
        let permitWorkEveryNWeekend = 3;
        // Permit work every other weekend for night workers
        if (this._isNightWorker()) {
            permitWorkEveryNWeekend = 2;
        }
        // Stores the first weekend the user works in the current shift plan
        let firstWeekend: DateObjectLiteralNumeric | null = null;
        // Stores how many weekends in a row we've had
        let currentWeekendsInARow = [];
        // Stores which weeks the person is working a weekend
        const weeksWithWorkWeekends = [];
        const finalizeCurrentWeekendsInARow = () => {
            if (currentWeekendsInARow.length > 2) {
                this.analysis.addWarning(
                    "WEEKENDS_TOO_CLOSE",
                    lastWeekend,
                    `${currentWeekendsInARow.length} helger på rad`,
                    "Veke " + currentWeekendsInARow.join(", "),
                );
            } else if (currentWeekendsInARow.length === 2) {
                this.analysis.addNote(
                    "WEEKENDS_TOO_CLOSE",
                    lastWeekend,
                    `${currentWeekendsInARow.length} helger på rad`,
                    "Veke " + currentWeekendsInARow.join(", "),
                );
            }
        };
        this._queueWeekIterator((weekNo, week) => {
            if (lastWeekend + 1 == weekNo && !this._isNightWorker()) {
                let shiftNo = 0;
                let firstF;
                for (const shift of week) {
                    shiftNo++;
                    if (this.identifier.isF(shift)) {
                        firstF = shiftNo;
                        break;
                    }
                }
                if (firstF !== undefined && firstF > 2) {
                    if (USE_LIMITED_RULESET === true) {
                        this.analysis.addNote(
                            "LATE_F_AFTER_WEEKEND",
                            weekNo,
                            "sein fridag etter helg (" +
                                this.format.dayNamer(firstF, null) +
                                ")",
                        );
                    } else {
                        this.analysis.addWarning(
                            "LATE_F_AFTER_WEEKEND",
                            weekNo,
                            "sein fridag etter helg (" +
                                this.format.dayNamer(firstF, null) +
                                ")",
                        );
                    }
                }
            }
            if (this.identifier.isExplicitF(week[5]) && week[5] !== "F2") {
                this.analysis.addNote(
                    "STRANGE_F_PLACEMENT_ON_SATURDAY",
                    weekNo,
                    "rar plassering av fridagskode på ein laurdag: " + week[5],
                );
            }
            if (
                (this.identifier.isN(week[4]) ||
                    this.identifier.isA(week[4])) &&
                this.identifier.isF(week[5]) &&
                this.identifier.isF(week[6])
            ) {
                this.analysis.addWarning(
                    "AN_ON_NON_WEEKEND_FRIDAY",
                    weekNo,
                    week[4] +
                        " på ein fredag som ikkje ser ut til å vere ei arbeidshelg",
                );
            }
            if (
                this.identifier.isN(week[4]) ||
                !this.identifier.isF(week[5]) ||
                !this.identifier.isF(week[6])
            ) {
                // Store the current weekend
                let thisWeekend: DateObjectLiteralNumeric;
                // Retrieve the current weekend value
                if (!this.identifier.isF(week[5])) {
                    thisWeekend = this.format.getNumericDateFromMap(
                        weekNo,
                        5,
                        "checkWeekends",
                    );
                } else if (!this.identifier.isF(week[6])) {
                    thisWeekend = this.format.getNumericDateFromMap(
                        weekNo,
                        6,
                        "checkWeekends",
                    );
                } else {
                    thisWeekend = this.format.getNumericDateFromMap(
                        weekNo,
                        4,
                        "checkWeekends",
                    );
                }

                const resolveUndefined = (from) => {
                    return from === undefined || from === null || from === ""
                        ? "F"
                        : from;
                };
                // Get the list of shifts on this weekend
                const shiftList =
                    resolveUndefined(week[4]) +
                    "/" +
                    resolveUndefined(week[5]) +
                    "/" +
                    resolveUndefined(week[6]);
                currentWeekendsInARow.push(
                    `${thisWeekend.week} (${shiftList})`,
                );
                // Convert thisWeekend to being on the sunday of the weekend
                const monthZeroIndexed = thisWeekend.month - 1;
                let thisWeekendDate = new Date(
                    thisWeekend.year,
                    monthZeroIndexed,
                    thisWeekend.day,
                );
                // Make sure we're on a sunday
                while (thisWeekendDate.getDay() !== 0) {
                    thisWeekendDate = this._getDateNDaysLater(
                        thisWeekendDate,
                        1,
                    );
                }
                // Convert back from a Date object to the internal object representing a date
                thisWeekend = {
                    year: thisWeekendDate.getFullYear(),
                    month: thisWeekendDate.getMonth() + 1,
                    day: thisWeekendDate.getDate(),
                    week: thisWeekend.week,
                };
                weeksWithWorkWeekends.push(thisWeekend.week);
                // If firstWeekend is null then this is the first weekend of this shift plan
                if (firstWeekend == null) {
                    firstWeekend = thisWeekend;
                    totalWeekendsFirstYear++;
                } else {
                    // We store the number of weekends in the first year of the
                    // shift plan in totalWeekendsFirstYear. Increase it if
                    // this weekend is also the current year.
                    if (thisWeekend.year == firstWeekend.year) {
                        totalWeekendsFirstYear++;
                    }
                }
                totalWeekends++;
                if (
                    weekNo > 1 &&
                    weekNo - lastWeekend < permitWorkEveryNWeekend &&
                    currentWeekendsInARow.length === 1 &&
                    lastWeekendShiftList !== null
                ) {
                    this.analysis.addNote(
                        "WEEKENDS_TOO_CLOSE",
                        weekNo,
                        `helger følg ikkje ${permitWorkEveryNWeekend}.-kvar (sist i veke ${lastWeekendCalendarWeekNumber})`,
                        `Jobber ${shiftList} denne helga (${lastWeekendShiftList} i veke ${lastWeekendCalendarWeekNumber}).`,
                    );
                }
                lastWeekendCalendarWeekNumber = thisWeekend.week;
                lastWeekendShiftList = shiftList;
                lastWeekend = weekNo;
            } else {
                finalizeCurrentWeekendsInARow();
                currentWeekendsInARow = [];
            }
            return true;
        });
        this._queueFinalizer(() => {
            finalizeCurrentWeekendsInARow();
            if (firstWeekend === null) {
                // FIXME: This should be noted somehow
                /*
                this.feedback.addGeneralWarning(
                    "TurnusLint er forvirra: denne personen har ingen helgevakter?",
                );
                */
                return;
            }
            // Stores the number of inferred weekends that would occur before this shift plan
            let weekendsBeforePlan = 0;
            // Date needs a zero-indexed month (0-11), currently it is 1-indexed (1-12)
            const firstWeekendMonthZeroIndexed = firstWeekend.month - 1;
            let currentWeekend: Date = new Date(
                firstWeekend.year,
                firstWeekendMonthZeroIndexed,
                firstWeekend.day,
            );
            // Iterate over possible weekends
            while (currentWeekend.getFullYear() == firstWeekend.year) {
                // Get a date object representing "three weeks before"
                currentWeekend = this._getDateNDaysEarlier(currentWeekend, 21);
                // Make sure we're on a sunday
                while (currentWeekend.getDay() !== 0) {
                    currentWeekend = this._getDateNDaysLater(currentWeekend, 1);
                }
                // If currentWeekend is still the same year as firstWeekend, but is
                // on a different month, or day-of-month, then we bump
                // weekendsBeforePlan and totalWeekendsFirstYear. Otherwise we skip
                // this loop.
                if (
                    currentWeekend.getFullYear() == firstWeekend.year &&
                    (currentWeekend.getMonth() + 1 != firstWeekend.month ||
                        currentWeekend.getDate() != firstWeekend.day)
                ) {
                    weekendsBeforePlan++;
                    totalWeekendsFirstYear++;
                }
            }
            if (totalWeekends > 0) {
                this.analysis.setStatistic("WEEKENDS", totalWeekends, {
                    secondaryValue: weeksWithWorkWeekends.join(", "),
                });
            } else {
                this.analysis.setStatistic("WEEKENDS", totalWeekends);
            }
            // Only output this if we actually managed to infer something, and skip
            // night workers, as they may have schedules that differ from the
            // every-third-weekend kind
            if (
                weekendsBeforePlan > 0 &&
                this._isNightWorker() === false &&
                config.getBooleanOption(
                    "enableInferredYearlyWorkWeekendcount",
                ) === true
            ) {
                this.analysis.setStatistic(
                    "INFERRED_STAIRCASE_WEEKENDS",
                    totalWeekendsFirstYear,
                    {
                        secondaryValue: firstWeekend.year + "",
                        tietaryValue: weekendsBeforePlan,
                    },
                );
            }
        });
    }

    countWeekendTypes() {
        let shortWorkWeekends = 0;
        let longOffWeekends = 0;
        const longOffWeekendsList = [];
        const shortWorkWeekendsList = [];
        this._queueWeekIterator((weekNo, week) => {
            let nextWeek = this._person[weekNo + 1];
            if (nextWeek === undefined) {
                nextWeek = [];
            }
            if (
                this.identifier.isN(week[4]) ||
                !this.identifier.isF(week[5]) ||
                !this.identifier.isF(week[6])
            ) {
                if (
                    !this.identifier.isN(week[3]) &&
                    this.identifier.isF(week[4]) &&
                    this.identifier.isF(nextWeek[0])
                ) {
                    shortWorkWeekends++;
                    const fullDate = this.format.getDateFromMap(
                        weekNo,
                        6,
                        "countWeekendTypes",
                    );
                    shortWorkWeekendsList.push(fullDate.week);
                }
            } else if (
                // Ignore weeks that are all off (ie. vacation)
                this.identifier.isAllF(week) === false &&
                this.identifier.isAllF(nextWeek) === false &&
                // Ignore weeks that have a night shift the night to friday
                !this.identifier.isN(week[3]) &&
                // Require saturday and sunday to be off
                this.identifier.isF(week[5]) &&
                this.identifier.isF(week[6]) &&
                // Require either friday or monday to be off
                (this.identifier.isF(week[4]) ||
                    this.identifier.isF(nextWeek[0]))
            ) {
                // Fetch the date representing this week
                const fullDate = this.format.getDateFromMap(
                    weekNo,
                    6,
                    "countWeekendTypes",
                );
                longOffWeekends++;
                longOffWeekendsList.push(fullDate.week);
            }
            return true;
        });
        this._queueFinalizer(() => {
            let shortWeekendsTooltip;
            if (shortWorkWeekendsList.length > 0) {
                shortWeekendsTooltip =
                    "Veke " + shortWorkWeekendsList.join(", ");
            }
            this.analysis.setStatistic(
                "SHORT_WORK_WEEKENDS",
                shortWorkWeekends,
                {
                    secondaryValue: shortWeekendsTooltip,
                },
            );
            let longWeekendsTooltip;
            if (longOffWeekendsList.length > 0) {
                longWeekendsTooltip = "Veke " + longOffWeekendsList.join(", ");
            }
            this.analysis.setStatistic("LONG_WEEKENDS", longOffWeekends, {
                secondaryValue: longWeekendsTooltip,
            });
        });
    }

    checkConsecutive() {
        let consecutive = 0;
        let maxConsecutive = -1;
        let lastShiftWeek = 0;
        let maxConsecutiveAllowed = 5;
        const maxConsecutiveOfType = {
            A: 0,
            D: 0,
            N: 0, // This is unused, but kept for simplicity's sake
            F: 0, // This is unused, but kept for simplicity's sake
        };
        const maxConsecutiveOfTypeMeta = {
            A: {
                times: 0,
                when: [],
            },
            D: {
                times: 0,
                when: [],
            },
            N: {
                times: 0,
                when: [],
            }, // This is unused, but kept for simplicity's sake
            F: {
                times: 0,
                when: [],
            }, // This is unused, but kept for simplicity's sake
        };
        let currentConsecutiveType = 1;
        // Because we count a night shift both on the day it starts and the day it ends,
        // a night worker will be allowed "6 consecutive shifts", which equals 5 nights.
        if (this._isNightWorker()) {
            maxConsecutiveAllowed = 6;
        }
        let prevShift;
        this._queueShiftIterator((weekNo, dayNo, shift) => {
            if (
                this.identifier.isF(shift) &&
                (prevShift === undefined ||
                    USE_LIMITED_RULESET === true ||
                    !this.identifier.isN(prevShift))
            ) {
                if (consecutive > maxConsecutiveAllowed) {
                    const term =
                        USE_LIMITED_RULESET === true ? "vakter" : "vaktdøgn";
                    this.analysis.addWarning(
                        "DAYS_IN_A_ROW",
                        lastShiftWeek,
                        "for mange " + term + " på rad: " + consecutive,
                    );
                }
                consecutive = 0;
            } else {
                consecutive++;
                lastShiftWeek = weekNo;
            }
            if (maxConsecutive < consecutive) {
                maxConsecutive = consecutive;
            }
            // Store statistics on number of shifts of this type in a row
            const type = this.identifier.identify(shift);
            if (
                prevShift !== undefined &&
                type === this.identifier.identify(prevShift)
            ) {
                currentConsecutiveType++;
            } else {
                currentConsecutiveType = 1;
            }
            if (
                type !== "UNKNOWN" &&
                currentConsecutiveType > maxConsecutiveOfType[type]
            ) {
                maxConsecutiveOfType[type] = currentConsecutiveType;
                maxConsecutiveOfTypeMeta[type] = {
                    times: 1,
                    when: [weekNo],
                };
            } else if (
                type !== "UNKNOWN" &&
                currentConsecutiveType === maxConsecutiveOfType[type]
            ) {
                maxConsecutiveOfTypeMeta[type].times++;
                maxConsecutiveOfTypeMeta[type].when.push(weekNo);
            }
            // Store this shift as the previous one, for use in the next iteration
            prevShift = shift;

            return true;
        });
        this._queueFinalizer(() => {
            this.analysis.setStatistic("SHIFTS_IN_A_ROW", maxConsecutive);
            this.analysis.setStatistic("D_IN_A_ROW", maxConsecutiveOfType.D, {
                secondaryValue:
                    maxConsecutiveOfTypeMeta.D.times +
                    " " +
                    (maxConsecutiveOfTypeMeta.D.times > 1 ? "gangar" : "gang") +
                    " (veke " +
                    maxConsecutiveOfTypeMeta.D.when.join(", ") +
                    ")",
            });
            this.analysis.setStatistic("A_IN_A_ROW", maxConsecutiveOfType.A, {
                secondaryValue:
                    maxConsecutiveOfTypeMeta.A.times +
                    " " +
                    (maxConsecutiveOfTypeMeta.A.times > 1 ? "gangar" : "gang") +
                    " (veke " +
                    maxConsecutiveOfTypeMeta.A.when.join(", ") +
                    ")",
            });
        });
    }

    checkWeeksOff() {
        let weeks = 0;
        const weekList = [];
        this._queueWeekIterator((weekNo, week) => {
            let hadWork = false;
            for (const shift of week) {
                if (!this.identifier.isF(shift)) {
                    hadWork = true;
                    break;
                }
            }
            if (!hadWork) {
                weeks++;
                // Fetch the date representing this week
                const fullDate = this.format.getDateFromMap(
                    weekNo,
                    1,
                    "checkWeeksOff",
                );
                weekList.push(fullDate.week);
            }
            return true;
        });
        this._queueFinalizer(() => {
            this.analysis.setStatistic("WEEKS_OFF", weeks, {
                secondaryValue: weeks > 0 ? weekList.join(", ") : null,
            });
        });
    }

    checkRedDays() {
        const workingDays = [];
        const specialDays = [];
        let prevShift = "F";
        let seenNonF = false;
        let F4Counter = 0;
        let workingRedCounter = 0;
        let lastF4Week = -1;
        this._queueShiftIterator((weekNo, dayNo: number, shift, map) => {
            const today = this.holidays.getHolidayOnDate(
                map.year,
                map.month,
                map.day,
            );
            if (today !== null) {
                if (
                    !this.identifier.isF(shift) ||
                    this.identifier.isN(prevShift)
                ) {
                    let sortValue: ?number;
                    let shiftVerbose = shift;
                    let description =
                        " (" +
                        this.format.dayNamer(dayNo, null) +
                        " " +
                        this.format.getCalendarDate(
                            weekNo,
                            dayNo,
                            "checkRedDays",
                        ) +
                        " - ";
                    if (
                        this.identifier.isF(shift) &&
                        this.identifier.isN(prevShift)
                    ) {
                        description += "går av nattevakt denne dagen)";
                        shiftVerbose = "går av nattevakt";
                        sortValue = 1;
                    } else {
                        description += shift + ")";
                    }
                    if (today.dayType === "NONRED" || today.dow > 5) {
                        specialDays.push(today.name + description);
                    } else {
                        workingDays.push(today.name + description);
                        workingRedCounter++;
                    }
                    if (sortValue === undefined || sortValue === null) {
                        sortValue = 5;
                        if (this.identifier.isD(shift)) {
                            sortValue = 2;
                        } else if (this.identifier.isA(shift)) {
                            sortValue = 3;
                        } else if (this.identifier.isN(shift)) {
                            sortValue = 4;
                        }
                    }

                    this.analysis.setSpecialDay(
                        today.name,
                        shiftVerbose,
                        sortValue,
                        today.dayType,
                        map,
                        today.identifier,
                    );
                }
                if (today) {
                    if (
                        this.identifier.isF(shift) &&
                        shift != "F3" &&
                        !this.identifier.isN(prevShift) &&
                        today.dow < 6 &&
                        today.dayType === "RED"
                    ) {
                        let shiftName = shift;
                        if (
                            shiftName == "" ||
                            shiftName == undefined ||
                            shiftName == null
                        ) {
                            shiftName = "(ingenting)";
                        }
                        const message =
                            "feil fridagskode på " +
                            this.format.dayNamer(today.dow, null) +
                            " " +
                            map.day +
                            "/" +
                            map.month +
                            " (" +
                            today.name +
                            "). Har " +
                            shiftName +
                            ", skulle vere F3";
                        if (seenNonF) {
                            this.analysis.addWarning(
                                "WRONG_OR_MISSING_F3",
                                weekNo,
                                message,
                            );
                        }
                        // If we haven't seen a non-F code yet, then maybe this
                        // person doesn't have a valid schedule yet, so make
                        // the error a notice rather than warning.
                        else {
                            this.analysis.addNote(
                                "WRONG_OR_MISSING_F3",
                                weekNo,
                                message,
                            );
                        }
                    }
                }
            }
            prevShift = shift;
            if (!this.identifier.isF(shift)) {
                seenNonF = true;
            }
            if (shift === "F4") {
                F4Counter++;
                lastF4Week = weekNo;
            }
            return true;
        });
        this._queueFinalizer(() => {
            if (workingDays.length > 0) {
                this.analysis.setStatistic("RED_DAYS", workingDays.join(", "), {
                    secondaryValue: workingDays.length + "",
                });
            } else {
                this.analysis.setStatistic("RED_DAYS", "ingen");
            }
            if (specialDays.length > 0) {
                this.analysis.setStatistic(
                    "SPECIAL_DAYS",
                    specialDays.join(", "),
                    { secondaryValue: specialDays.length + "" },
                );
            } else {
                this.analysis.setStatistic("SPECIAL_DAYS", "ingen");
            }
            if (
                lastF4Week !== -1 &&
                F4Counter > 0 &&
                F4Counter !== workingRedCounter
            ) {
                if (F4Counter > workingRedCounter) {
                    this.analysis.addWarning(
                        "WRONG_F4_COUNT",
                        lastF4Week,
                        "For mange F4 dagar på turnusen (forventa " +
                            workingRedCounter +
                            ", faktisk " +
                            F4Counter +
                            ")",
                    );
                } else if (
                    F4Counter < workingRedCounter &&
                    USE_LIMITED_RULESET === false
                ) {
                    this.analysis.addWarning(
                        "WRONG_F4_COUNT",
                        lastF4Week,
                        "For få F4 dagar på turnusen (forventa " +
                            workingRedCounter +
                            ", faktisk " +
                            F4Counter +
                            ")",
                    );
                }
            }
        });
    }

    gatherShiftStats() {
        const individualShifts: ShiftCounter = {};
        const fShifts = {};
        let fTotal = 0;
        let prevShift;
        let prevShift2;
        const StandaloneF = {
            times: 0,
            when: [],
        };
        const ABeforeF = {
            times: 0,
            when: [],
        };
        const shiftTypes = {
            total: 0,
            A: 0,
            D: 0,
            N: 0,
            F: 0,
        };
        let lastWeekWithShifts = -1;
        let numberOfWeeksWithShifts = 0;
        const weekToShiftTypes = {};
        this._queueShiftIterator((weekNo, dayNo, shift) => {
            const fullDate = this.format.getDateFromMap(
                weekNo,
                1,
                "gatherShiftStats",
            );
            if (!this.identifier.isF(shift)) {
                // Get the shift type
                const shiftType = this.identifier.identify(shift);
                if (shiftType === "UNKNOWN") {
                    // FIXME: Should log error
                    return true;
                }
                // Initialize datastructures
                if (!individualShifts[shift]) {
                    individualShifts[shift] = 0;
                }
                if (weekToShiftTypes[weekNo] === undefined) {
                    weekToShiftTypes[weekNo] = {};
                }
                if (weekToShiftTypes[weekNo][shiftType] === undefined) {
                    weekToShiftTypes[weekNo][shiftType] = 0;
                }
                // Store stats
                individualShifts[shift]++;
                shiftTypes[shiftType]++;
                shiftTypes.total++;
                weekToShiftTypes[weekNo][shiftType]++;
                // Bump weeksWithShifts if needed
                if (lastWeekWithShifts !== weekNo) {
                    lastWeekWithShifts = weekNo;
                    numberOfWeeksWithShifts++;
                }

                if (
                    this.identifier.isF(prevShift) &&
                    !this.identifier.isF(prevShift2)
                ) {
                    // Gotcha for the dayNo tests: we're currently the day AFTER
                    // the day off, so if the day off was a friday, dayNo is
                    // now 6, not 5, thus we need to check for "day number
                    // off"+1
                    let looksLikeWeekend = false;
                    if (
                        (dayNo === 2 || dayNo === 3) &&
                        !this.identifier.isF(
                            this._getShiftForDay(weekNo - 1, 6),
                        )
                    ) {
                        looksLikeWeekend = true;
                    } else if (
                        (dayNo === 5 || dayNo === 6) &&
                        !this.identifier.isF(this._getShiftForDay(weekNo, 7))
                    ) {
                        looksLikeWeekend = true;
                    }
                    if (!looksLikeWeekend && USE_LIMITED_RULESET === false) {
                        StandaloneF.times++;
                        if (StandaloneF.when.indexOf(fullDate.week) === -1) {
                            StandaloneF.when.push(fullDate.week);
                        }
                    }
                }
            } else if (USE_LIMITED_RULESET === false) {
                if (shift != "" && shift != null) {
                    if (!fShifts[shift]) {
                        fShifts[shift] = 0;
                    }
                    fShifts[shift]++;
                    fTotal++;
                }
                if (
                    prevShift !== undefined &&
                    this.identifier.identify(prevShift) === "A"
                ) {
                    ABeforeF.times++;
                    ABeforeF.when.push(fullDate.week);
                }
            }
            prevShift2 = prevShift;
            prevShift = shift;
            return true;
        });
        this._queueFinalizer(() => {
            const percentages = {
                A: 0,
                D: 0,
                N: 0,
            };
            // Try to figure out "omregnet tid"
            let timeReduction: TimeReductionStates;
            let timeReductionReason: string;
            // Generate list of weeks with only a single type of shift
            let singleShiftWeeks = 0;
            const singleShiftWeeksList = [];
            for (const week of Object.keys(weekToShiftTypes)) {
                if (Object.keys(weekToShiftTypes[week]).length === 1) {
                    singleShiftWeeks++;
                    // Fetch the day representing this week
                    const fullDate = this.format.getDateFromMap(
                        week,
                        1,
                        "gatherShiftStats finalizer",
                    );
                    singleShiftWeeksList.push(fullDate.week);
                }
            }

            const typeStats = [];
            for (const type of ["A", "D", "N"]) {
                (percentages[type] =
                    (shiftTypes[type] / shiftTypes.total) * 100),
                    typeStats.push(
                        shiftTypes[type] +
                            " " +
                            type +
                            " (" +
                            this.format.roundToDecimals(percentages[type], 1) +
                            "%)",
                    );
                if (type !== "N") {
                    let statType;
                    if (type === "D") {
                        statType = "D_SHIFTS";
                    } else if (type === "A") {
                        statType = "A_SHIFTS";
                    } else {
                        // Impossible, but helps flow
                        throw "IMPOSSIBLE";
                    }
                    this.analysis.setStatistic(statType, shiftTypes[type]);
                }
            }
            this.analysis.setStatistic(
                "SHIFT_GENERIC_SPREAD",
                typeStats.join(", "),
            );

            if (USE_LIMITED_RULESET === false) {
                this.analysis.setStatistic("A_BEFORE_F_TIMES", ABeforeF.times, {
                    secondaryValue:
                        ABeforeF.times > 0
                            ? "(veke " + ABeforeF.when.join(", ") + ")"
                            : "",
                });
                this.analysis.setStatistic(
                    "STANDALONE_F_TIMES",
                    StandaloneF.times,
                    {
                        secondaryValue:
                            StandaloneF.times > 0
                                ? "(veke " + StandaloneF.when.join(", ") + ")"
                                : "",
                    },
                );
            }

            const individualStats = [];
            const sortedShifts = Object.keys(individualShifts).sort(
                (first, second) => {
                    return individualShifts[second] - individualShifts[first];
                },
            );
            for (const type of sortedShifts) {
                individualStats.push(
                    individualShifts[type] +
                        " " +
                        type +
                        " (" +
                        this.format.roundToDecimals(
                            (individualShifts[type] / shiftTypes.total) * 100,
                            1,
                        ) +
                        "%)",
                );
            }
            this.analysis.setStatistic(
                "SHIFT_CODE_SPREAD",
                individualStats.join(", "),
            );
            this.analysis.setShiftCounter(individualShifts);

            if (USE_LIMITED_RULESET === false) {
                const fStats = [];
                for (const type in fShifts) {
                    fStats.push(
                        fShifts[type] +
                            " " +
                            type +
                            " (" +
                            this.format.roundToDecimals(
                                (fShifts[type] / fTotal) * 100,
                                1,
                            ) +
                            "%)",
                    );
                }
                this.analysis.setStatistic(
                    "FREE_CODE_SPREAD",
                    fStats.join(", "),
                );
            }

            if (USE_LIMITED_RULESET === false) {
                this.analysis.setStatistic(
                    "SINGLE_SHIFT_CODE_WEEKS",
                    singleShiftWeeks,
                    {
                        secondaryValue:
                            singleShiftWeeks > 0
                                ? singleShiftWeeksList.join(", ")
                                : null,
                    },
                );
            }
            let timeReductionSortIndex: number = 0;

            // We attempt to make guesstimates for people whose schedule is
            // valid for less than a full year, so every comparison value
            // gets multiplied by the timeReductionChangeFactor before comparison.
            // This factor is 1.0 by default. If we have fewer than 45 valid
            // weeks then we modify the timeReductionChangeFactor by dividing
            // 45 by the number of valid weeks.
            let timeReductionChangeFactor = 1.0;
            if (lastWeekWithShifts < 50 && numberOfWeeksWithShifts > 30) {
                timeReductionChangeFactor = lastWeekWithShifts / 52;
            } else if (numberOfWeeksWithShifts <= 45) {
                timeReductionChangeFactor = numberOfWeeksWithShifts / 45;
            }

            // The number of nights required for a "Yes" to time reduction
            const NIGHTS_REQUIRED_FOR_YES = 34 * timeReductionChangeFactor;
            // The number of nights required for a "maybe" to time reduction
            // anything below this value is either a "no" or a "probably not".
            const NIGHTS_REQUIRED_FOR_MAYBE = 26 * timeReductionChangeFactor;
            // The number of weekends required for a "yes"
            const WEEKENDS_REQUIRED_FOR_YES = 16 * timeReductionChangeFactor;
            // The number of weekends required for a "maybe"
            const WEEKENDS_REQUIRED_FOR_MAYBE = 14 * timeReductionChangeFactor;

            // We use this to calculate a score
            const calculateScore = (
                base: number,
                nights: number,
                percentageNights: number,
                percentageA: number,
            ): number => {
                let score = 0;
                const scoreModifier = 1 + (1 - timeReductionChangeFactor);
                score += nights * scoreModifier;
                score += percentageNights;
                score += percentageA;
                return base + score;
            };

            // We use the WEEKENDS count that is done by .checkWeekends(), so
            // we must always be queued for execution *after* .checkWeekends().
            // Checks are finalized in the order they are called in analyze().
            // This fact is also noted there.
            const weekendsStat = this.analysis.statistic("WEEKENDS", "0");
            const weekends = parseInt(weekendsStat.value);
            if (
                shiftTypes["A"] === 0 ||
                shiftTypes["D"] === 0 ||
                shiftTypes["N"] === 0
            ) {
                timeReduction = "Nei";
                if (
                    shiftTypes.D > 0 &&
                    shiftTypes.A === 0 &&
                    shiftTypes.N === 0
                ) {
                    timeReductionReason = "Dagarbeidar";
                } else {
                    timeReductionReason = "Todelt turnus";
                }
            } else if (percentages["N"] >= 75) {
                timeReduction = "Nei";
                timeReductionReason = percentages["N"] + "% natt";
            } else if (weekends < WEEKENDS_REQUIRED_FOR_YES) {
                timeReductionReason = `Berre ${weekends} helger i planen`;
                if (
                    shiftTypes.total > 80 &&
                    weekends <= WEEKENDS_REQUIRED_FOR_MAYBE
                ) {
                    timeReduction = "Nei";
                } else if (
                    shiftTypes["N"] >= NIGHTS_REQUIRED_FOR_MAYBE &&
                    percentages["A"] >= 10
                ) {
                    timeReduction = "Kanskje";
                    timeReductionReason +=
                        ", men " + shiftTypes["N"] + " netter";
                    timeReductionSortIndex = calculateScore(
                        2000,
                        shiftTypes["N"],
                        percentages["N"],
                        percentages["A"],
                    );
                } else {
                    timeReduction = "Truleg ikkje";
                    timeReductionSortIndex = weekends + 10;
                    if (percentages["A"] <= 9) {
                        timeReductionReason +=
                            " og berre " +
                            percentages["A"].toFixed(1) +
                            "% (" +
                            shiftTypes["A"] +
                            ") A";
                        timeReductionSortIndex -= 6;
                    } else if (shiftTypes["N"] < NIGHTS_REQUIRED_FOR_MAYBE) {
                        timeReductionReason +=
                            " og berre " + shiftTypes["N"] + " netter";
                        timeReductionSortIndex -= 5;
                    }
                }
            } else if (
                shiftTypes["A"] > 0 &&
                shiftTypes["D"] > 0 &&
                shiftTypes["N"] > 0
            ) {
                if (
                    shiftTypes["N"] >= NIGHTS_REQUIRED_FOR_YES &&
                    percentages["A"] >= 10 &&
                    percentages["D"] >= 10
                ) {
                    timeReductionSortIndex = calculateScore(
                        3000,
                        shiftTypes["N"],
                        percentages["N"],
                        percentages["A"],
                    );
                    timeReduction = "Ja";
                    timeReductionReason =
                        "Tredelt turnus med " + shiftTypes["N"] + " netter";
                } else if (
                    shiftTypes["N"] >= NIGHTS_REQUIRED_FOR_MAYBE &&
                    percentages["A"] >= 10 &&
                    percentages["D"] >= 10
                ) {
                    timeReduction = "Kanskje";
                    timeReductionReason =
                        "Tredelt turnus med " + shiftTypes["N"] + " netter";
                    timeReductionSortIndex = calculateScore(
                        2000,
                        shiftTypes["N"],
                        percentages["N"],
                        percentages["A"],
                    );
                } else if (
                    percentages["N"] >= 12 &&
                    percentages["A"] >= 35 &&
                    percentages["D"] >= 10
                ) {
                    timeReduction = "Kanskje";
                    timeReductionReason =
                        "Tredelt turnus med " +
                        shiftTypes["N"] +
                        " (" +
                        percentages["N"].toFixed(1) +
                        "%) netter og " +
                        shiftTypes["A"] +
                        " (" +
                        percentages["A"].toFixed(1) +
                        "%) A ";
                    timeReductionSortIndex = calculateScore(
                        2000,
                        shiftTypes["N"],
                        percentages["N"],
                        percentages["A"],
                    );
                } else {
                    timeReduction = "Truleg ikkje";
                    timeReductionSortIndex = 1000;
                    if (percentages["A"] < 10 && shiftTypes["N"] >= 20) {
                        timeReductionReason =
                            "Tredelt turnus, men berre " +
                            shiftTypes["A"] +
                            " A-vakter (" +
                            percentages["A"].toFixed(1) +
                            "% av vaktene)";
                        timeReductionSortIndex += shiftTypes["A"];
                    } else if (shiftTypes["N"] < NIGHTS_REQUIRED_FOR_MAYBE) {
                        timeReductionReason =
                            "Tredelt turnus, men berre " +
                            shiftTypes["N"] +
                            " netter";
                        timeReductionSortIndex += shiftTypes["N"];
                    } else {
                        timeReductionReason =
                            "Tredelt turnus, men med lite ubekvemt";
                    }
                }
            } else {
                this._logger.warning(
                    "PersonChecker.gatherShiftStats-timeReduction",
                    "Reached end of if-chain checking for time reduction reasons. This shouldn't ever actually happen. Falling back to maybe.",
                );
                timeReduction = "Kanskje";
                timeReductionReason = "[TURNUSLINT FEIL]";
            }
            this.analysis.setStatistic("TIME_REDUCTION", timeReduction, {
                secondaryValue: timeReductionReason,
                tietaryValue: timeReductionSortIndex,
            });
        });
    }

    checkStandalone() {
        const shiftList = [];
        const shiftsInWeek = {};
        const nonFShiftInWeek = {};
        const LonelyShiftMonthCounter: MonthCounter = new Map();
        let lonelyShifts = 0;
        this._queueShiftIterator((weekNo, dayNo, shift) => {
            const minusOne = shiftList[shiftList.length - 1];
            const minusTwo = shiftList[shiftList.length - 2];
            const minusThree = shiftList[shiftList.length - 3];
            /*
             * Check for standalone shifts
             */
            if (minusOne && minusTwo && minusThree) {
                if (
                    this.identifier.isF(shift) &&
                    !this.identifier.isN(minusOne.shift) &&
                    !this.identifier.isF(minusOne.shift) &&
                    this.identifier.isF(minusTwo.shift) &&
                    !this.identifier.isN(minusThree.shift)
                ) {
                    this.analysis.addNote(
                        "LONELY_SHIFT",
                        weekNo,
                        "einsam vakt " +
                            this.format.getCalendarDate(
                                minusOne.weekNo,
                                minusOne.dayNo,
                                "checkStandalone",
                            ) +
                            ": " +
                            minusOne.shift,
                    );
                    lonelyShifts++;
                    // Fetch the date representing this day
                    const fullDate = this.format.getDateObjectBasedOnMap(
                        weekNo,
                        dayNo,
                        "checkStandalone",
                    );
                    // Format the current date's month as Month/Year
                    const monthOfYear =
                        fullDate.toLocaleString(["nn-NO", "no-NO"], {
                            month: "short",
                        }) +
                        "/" +
                        fullDate.getFullYear();
                    let currentCount = LonelyShiftMonthCounter.get(monthOfYear);
                    if (currentCount === undefined) {
                        currentCount = 0;
                        LonelyShiftMonthCounter.set(monthOfYear, currentCount);
                    }
                    LonelyShiftMonthCounter.set(monthOfYear, ++currentCount);
                }
            }
            /*
             * Check for weeks with only a single shift
             */
            if (!shiftsInWeek[weekNo]) {
                shiftsInWeek[weekNo] = 0;
            }
            // If the week starts with a night shift (that is, a night shift
            // that starts sunday and ends monday), treat that as a shift this
            // week, even though it started the previous week
            if (!this.identifier.isF(shift)) {
                shiftsInWeek[weekNo]++;
                nonFShiftInWeek[weekNo] = shift;
            }
            if (dayNo === 1) {
                if (
                    minusOne &&
                    minusOne.shift &&
                    this.identifier.isN(minusOne.shift)
                ) {
                    shiftsInWeek[weekNo]++;
                    nonFShiftInWeek[weekNo] = "N, sundag veka før";
                }
            } else if (dayNo === 7) {
                if (shiftsInWeek[weekNo] === 1) {
                    this.analysis.addNote(
                        "ONE_SHIFT_WEEK",
                        weekNo,
                        "veke med berre ei enkelt vakt (" +
                            nonFShiftInWeek[weekNo] +
                            ")",
                    );
                }
            }
            /*
             * Update the shift table
             */
            shiftList.push({ weekNo, dayNo, shift });

            return true;
        });
        this._queueFinalizer(() => {
            // Build a string with a summary of the months with N
            const formattedMonthCounter = [];
            for (const [key, value] of LonelyShiftMonthCounter) {
                formattedMonthCounter.push(key + ":\u00A0" + value);
            }
            this.analysis.setStatistic("LONELY_SHIFTS", lonelyShifts, {
                secondaryValue:
                    lonelyShifts > 0 ? formattedMonthCounter.join("\n") : null,
                metaObject: {
                    type: "MONTH_SPREAD",
                    months: LonelyShiftMonthCounter,
                },
            });
        });
    }
}

type FileCheckerEvent =
    | "CHECKER_RESET"
    | "NEW_RESULT_AVAILABLE"
    | "FILE_LOAD_START"
    | "FILE_LOAD_END"
    | "FILE_LOAD_PROGRESS"
    | "ANALYZE_START"
    | "ANALYZE_END";

type FileCheckerListener = (FileCheckerEvent, FileChecker, ?string) => void;
type GatParserList = Array<{|
    filename: string,
    parser: GatParser,
|}>;

type addFilesOptions = {|
    mode?: "exclusive" | "append",
|};

type FileCheckerProgressMode = "FULL" | "ANALYZE_ONLY";

class FileChecker {
    logger: TurnusLintLogger;
    config: TurnusLintConfig;
    _eventListeners: Array<FileCheckerListener> = [];
    parsers: GatParserList = [];
    entries: Array<ShiftAnalysisContainer> = [];
    holidays: Holidays;
    _progressMode: FileCheckerProgressMode = "FULL";

    constructor(logger: TurnusLintLogger, config: TurnusLintConfig) {
        this.logger = logger;
        this.config = config;
        this.holidays = new Holidays(this.logger);
    }

    addFiles(files: FileList, options: addFilesOptions = { mode: "append" }) {
        this._notifyListeners("FILE_LOAD_START");
        if (options.mode === "exclusive") {
            this.reset();
        }
        this.addFile(files[0], files, 0);
    }

    addFile(file: File, files: FileList, no: number = 0) {
        this._progressMode = "FULL";
        const reader = new FileReader();
        reader.addEventListener("loadend", () => {
            const result = reader.result;
            if (result === null || typeof result === "string") {
                throw "Got invalid file from FileReader";
            }
            let start;
            if (TURNUSLINT_DEBUG === true) {
                start = Date.now();
            }
            // reader.result contains the contents of blob as a typed array
            const gatP = new GatParser(this.logger);
            gatP.load(result, (percentage) =>
                this._emitProgressedEvent(percentage),
            )
                .then(async (parser) => {
                    await this._handleGatParserObj(parser, file.name);
                    if (TURNUSLINT_DEBUG === true) {
                        this.logger.debug(
                            "FileLoader",
                            "Spent " +
                                parseInt(Date.now() - start) / 1000 +
                                " seconds parsing",
                        );
                    }
                    this.parsers.push({
                        filename: file.name,
                        parser,
                    });
                    no++;
                    if (files.length > no) {
                        this.addFile(files[no], files, no);
                    } else {
                        this._notifyListeners("ANALYZE_END");
                    }
                })
                .catch((err) => {
                    this.logger.error("TurnusLint>handleFileFromList", err, {
                        extraInfo: err.stack,
                    });
                    this._notifyListeners("ANALYZE_END");
                });
        });
        if (files.length === 0) {
            this.logger.error(
                "FileChecker",
                "Fila vart ikkje lasta korrekt, prøv igjen.",
                { code: "FILECHECKER_LOAD_FAILED" },
            );
            this._notifyListeners("ANALYZE_END");
        } else {
            reader.readAsArrayBuffer(file);
        }
    }

    async reanalyze() {
        if (this.entries.length === 0) {
            return;
        }
        this._progressMode = "ANALYZE_ONLY";
        this.entries = [];
        this._notifyListeners("ANALYZE_START");
        for (const entry of this.parsers) {
            await this._handleGatParserObj(entry.parser, entry.filename);
        }
        this._notifyListeners("ANALYZE_END");
    }

    listen(cb: FileCheckerListener) {
        this._eventListeners.push(cb);
    }

    reset() {
        this.parsers = [];
        this.entries = [];
        this._notifyListeners("CHECKER_RESET");
    }

    _internalDidProgress(fraction: number) {
        let percentage: number;
        if (this._progressMode === "FULL") {
            percentage = fraction * 21 + 79;
        } else {
            percentage = fraction * 100;
        }
        this._emitProgressedEvent(percentage);
    }

    _emitProgressedEvent(percentage: number) {
        this._notifyListeners(
            "FILE_LOAD_PROGRESS",
            Math.round(percentage) + "%",
        );
    }

    async _asyncAnalyzePerson(data: GatData, name: string): Promise<boolean> {
        return new Promise((resolve, _) => {
            setTimeout(() => {
                const checker = new PersonChecker(
                    name,
                    data.shifts,
                    data.people[name],
                    data.weekToDate[name],
                    this.config,
                    this.logger,
                    data.notes[name],
                    data.nameToHash[name],
                    this.holidays,
                );
                if (checker.isValidPerson()) {
                    checker.analyze();
                    this.entries.push(checker.analysis);
                } else {
                    this.logger.warning(
                        "TurnusLint-class",
                        "Person har ingen vakter: " + name,
                    );
                }
                resolve(true);
            }, 0);
        });
    }

    async _handleGatParserObj(parser: GatParser, filename: string) {
        const data = parser.getData();
        const total = Object.keys(data.people).length;
        let current = 0;
        for (const name in data.people) {
            await this._asyncAnalyzePerson(data, name);
            this._internalDidProgress(++current / total);
        }
        if (this.entries.length === 0) {
            this.logger.critical(
                "FileChecker",
                "Fant ingen personar med gyldige turnusar i " + filename,
                { code: "FILECHECKER_NO_VALID_SCHEDULES" },
            );
        }
        this._notifyListeners("NEW_RESULT_AVAILABLE");
    }

    _notifyListeners(event: FileCheckerEvent, data: string | null = null) {
        if (TURNUSLINT_DEBUG) {
            this.logger.debug("FileChecker", "Emitting event " + event);
        }
        for (const listener of this._eventListeners) {
            listener(event, this, data);
        }
    }
}

export {
    PersonChecker,
    ShiftFeedbackFormatter,
    ShiftAnalysisContainer,
    FileChecker,
    dayNamer,
};

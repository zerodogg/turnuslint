/* global module process */
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const childProcess = require("child_process");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");

let GIT_REVISION, TURNUSLINT_DEBUG;
let TURNUSLINT_INCLUDE_GOATCOUNTER = false;
let TURNUSLINT_DEBUG_UNHANDLED_LINES = false;
let USE_LIMITED_RULESET = true;
if (process.env.USE_LIMITED_RULESET === "0") {
    USE_LIMITED_RULESET = false;
}
const BUILD_DATE = new Date().toISOString().replace(/T.+/, "");
if (process.env.NODE_ENV === "development") {
    GIT_REVISION = "(git)";
    TURNUSLINT_DEBUG = true;
} else {
    TURNUSLINT_DEBUG = false;
    GIT_REVISION = childProcess
        .execSync("git rev-parse --short=4 HEAD")
        .toString()
        .replace(/\n/g, "");
    if (process.env.BUILD_TARGET === "branch") {
        const CI_COMMIT_REF_NAME = process.env.CI_COMMIT_REF_NAME;
        TURNUSLINT_DEBUG = true;
        if (CI_COMMIT_REF_NAME !== null && CI_COMMIT_REF_NAME !== undefined) {
            GIT_REVISION += "-" + CI_COMMIT_REF_NAME;
        }
    }
}
if (process.env.TURNUSLINT_DEBUG_UNHANDLED_LINES === "1") {
    TURNUSLINT_DEBUG_UNHANDLED_LINES = true;
}
if (process.env.DEPLOYING_TO === "gitlab") {
    TURNUSLINT_INCLUDE_GOATCOUNTER = true;
}
const GIT_REVISION_FULL = childProcess
    .execSync("git rev-parse HEAD")
    .toString()
    .replace(/\n/g, "");

module.exports = {
    devtool: "source-map",
    output: {
        filename:
            process.env.NODE_ENV === "production"
                ? "[name].js?b=[chunkhash]"
                : "[name].[contenthash].js",
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: /node_modules\/(?!(unreachable))/,
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    "css-loader",
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: "css-loader",
                        // Disables url() handling. Just assume they are correct.
                        options: { url: false, sourceMap: true },
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sassOptions: {
                                sourceMap: true,
                                includePaths: ["static/"],
                            },
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            GIT_REVISION: JSON.stringify(GIT_REVISION),
            GIT_REVISION_FULL: JSON.stringify(GIT_REVISION_FULL),
            USE_LIMITED_RULESET: USE_LIMITED_RULESET,
            TURNUSLINT_DEBUG_UNHANDLED_LINES: TURNUSLINT_DEBUG_UNHANDLED_LINES,
            TURNUSLINT_DEBUG: TURNUSLINT_DEBUG,
            TURNUSLINT_INCLUDE_GOATCOUNTER: TURNUSLINT_INCLUDE_GOATCOUNTER,
            BUILD_DATE: JSON.stringify(BUILD_DATE),
        }),
        new HtmlWebpackPlugin({
            template: "src/index.html",
            minify: {
                removeComments: true,
            },
        }),
        new MiniCssExtractPlugin({}),
        new CopyWebpackPlugin({
            patterns: [
                { from: "static/" },
                { from: "nonfree/*.png" },
                { from: "COPYING" },
            ],
        }),
    ],
    optimization: {
        minimizer: [new TerserPlugin()],
        splitChunks: {
            chunks: "all",
        },
    },
};
